<?php

    Route::get('/','HomeController@home')->name('Trangchu');
    Route::get('GioiThieu','HomeController@Gioithieu')->name('Gioithieu');
    Route::get('LoaiPhong','HomeController@Loaiphong')->name('Loaiphong');
    Route::get('DichVu','HomeController@Dichvu')->name('Dichvu');
    Route::get('LienHe','HomeController@Lienhe')->name('Lienhe');
    Route::post('datphong', 'ReservationController@postDatPhong')->name('trangchu.postDatPhong');
    Route::post('dienthongtin', 'HomeController@dienthongtin')->name('trangchu.postlienhe');

Route::group(['prefix' => 'auth'], function () {

    Route::get('login', 'UserController@login')->name('admin.login');
    Route::post('login', 'UserController@postLogin')->name('admin.postLogin');
    Route::get('logout', 'UserController@logoutUser')->name('admin.Logout');
});


    Route::group(['prefix' => 'reservation'], function () {
        Route::get('danhsach', 'ReservationController@danhsachDatPhong')->name('admin.danhsachDatPhong');
        Route::get('xoa/{id}', 'ReservationController@xoaDatPhong')->name('admin.xoaDatPhong');
        Route::post('capnhattrangthai/{id}', 'ReservationController@trangthaidatphong')->name('admin.capnhapDatphong');
    });

    Route::get('/user', 'UserController@listUser')->name('admin.listUser');
    Route::post('capnhatuser/{id}', 'UserController@updateUser')->name('admin.updateUser');
    Route::get('changepassword/{id}', 'UserController@changepassword')->name('admin.changepassword');
    Route::post('changepassword/{id}', 'UserController@postpassword')->name('admin.postpassword');
    Route::get('register', 'UserController@themUser')->name('admin.themUser');
    Route::post('register', 'UserController@storeUser')->name('admin.storeUser');
    Route::get('xoa/{id}', 'UserController@xoaUser')->name('admin.xoaUser');

    Route::group(['prefix' => 'admin'], function () {

        Route::group(['prefix' => 'room'], function () {
            Route::get('danhsach', 'RoomController@danhSachPhong')->name('admin.danhsach');
            Route::get('sua/{id}', 'RoomController@SuaDanhSach')->name('admin.suaDanhSach');
            Route::post('sua/{id}', 'RoomController@updateDanhSachPhong');
            Route::get('xoa/{id}', 'RoomController@XoaRoom')->name('admin.xoa');
            Route::get('themPhong', 'RoomController@themPhong')->name('admin.themPhong');
            Route::post('themPhong', 'RoomController@XuLyThemPhong');
            Route::get('xoa/{id}', 'RoomController@XoaRoom')->name('admin.xoa');
            Route::post('capnhattrangthai/{id}', 'RoomController@trangthaiphong')->name('admin.capnhap');
        });
        Route::group(['prefix' => 'service'], function () {
            Route::get('danhsachdichvu', 'ServiceController@danhSachDichVu')->name('admin.danhsachDichVu');
            Route::get('Suadichvu/{id}', 'ServiceController@suaDichVu')->name('admin.SuaDichVu');
            Route::post('Suadichvu/{id}', 'ServiceController@capNhapDichVu');
            Route::get('themDichVu', 'ServiceController@themDichVu')->name('admin.themDichVu');
            Route::post('themDichVu', 'ServiceController@XuLyThemDichVu');
            Route::get('xoaDichVu/{id}', 'ServiceController@xoaDichVu')->name('admin.xoaDichVu');

        });
        Route::group(['prefix' => 'styleRoom'], function () {
            Route::get('danhsachloaiphong', 'StyleRoomController@danhSachLoaiPhong')->name('admin.danhsachloaiphong');
            Route::get('SuaLoaiPhong/{id}', 'StyleRoomController@SuaLoaiPhong')->name('admin.SuaLoaiPhong');
            Route::post('SuaLoaiPhong/{id}', 'StyleRoomController@capnhapLoaiPhong');
            Route::get('themloaiphong', 'StyleRoomController@themLoaiPhong')->name('admin.themLoaiPhong');
            Route::post('themloaiphong', 'StyleRoomController@XuLyThemLoaiPhong');
            Route::get('xoaLoaiPhong/{id}', 'StyleRoomController@xoaLoaiPhong')->name('admin.xoaLoaiPhong');
        });
        Route::group(['prefix' => 'information'], function () {
            Route::get('danhsach', 'ContactController@lisInfor')->name('admin.listInfor');
            Route::get('them', 'ContactController@themThongTin')->name('admin.themThongTin');
            Route::post('them', 'ContactController@storeDanhSachPhong')->name('admin.storeThongTin');
        });
        Route::group(['prefix' => 'feedback'], function () {
            Route::get('lienhekhachhang', 'HomeController@danhSachdienthongtin')->name('admin.feedback');
            Route::get('lienhekhachhang/{id}', 'HomeController@xoalienhe')->name('admin.xoalienhe');
        });

    });
