<?php

use Illuminate\Database\Seeder;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        {
        	DB::table('Users')->insert(
	        	[
                    'name' => 'Long',
                    'DOB' => '12/12/2157',
                    'phone' => '0123456789',
	            	'email' => 'tinhanh473@gmail.com',
                    'password' => bcrypt('123456'),
	            	'created_at' => new DateTime(),
	        	]
        	);
        }
    }
}
