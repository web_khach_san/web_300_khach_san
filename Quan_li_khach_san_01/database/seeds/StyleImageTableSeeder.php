<?php

use Illuminate\Database\Seeder;

class StyleImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('style_images')->insert
        ([
            'name' => 'Mát xa sunnna', 
            'description' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín',
            'image' => 'https://bitly.vn/5j9k',
        ]);
    }
}
