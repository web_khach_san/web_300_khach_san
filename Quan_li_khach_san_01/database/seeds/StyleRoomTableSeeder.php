<?php

use Illuminate\Database\Seeder;

class StyleRoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('style_rooms')->insert
        ([
        ['MaLoaiPhong' => 'VIP',
            'TenLoaiPhong' => 'DELUXE KING RIVER',
            'Anh' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQR99ndHs3ZeBt2tbIxN24alpdb7ob1k2wKHDPVKzvsCFF9oDOK',
            'Gia' => '120',
            'Mota' => 'Phòng nằm gần sông nên rất mát mẻ',

        ],
        [
            'MaLoaiPhong' => 'No VIP',
            'TenLoaiPhong' => 'ROMATIC KING RIVER',
            'Anh' => 'http://hue.muongthanh.com/FileUpload/Images/thumb/_1380636.jpg',
            'Gia' => '250',
            'Mota' => 'Phòng nằm trên cao nên không có muỗi',
        ],
        [
            'MaLoaiPhong' => 'No VIP',
            'TenLoaiPhong' => 'RELAX KING RIVER',
            'Anh' => 'https://bitly.vn/5peh',
            'Gia' => '567',
            'Mota' => 'Phòng nằm trên cao nên không có muỗi',
        ],
    ]);
    }
}
