<?php

use Illuminate\Database\Seeder;

class TourTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tours')->insert
        ([
        [
            'nameTour' => 'TOUR DU LỊCH ĐẦM PHÁ TAM GIANG', 
            'Anh' => 'https://image.thanhnien.vn/660/uploaded/quochung.qc/2017_10_26/mh1/1_bvsm.jpg', 
            'content' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín'
        ],
        [
            'nameTour' => 'TOUR DU LỊCH RESOFT LAGUNA', 
            'Anh' => 'http://giadinh.mediacdn.vn/zoom/655_361/vjByQk1Ngis0lccccccccccccJoDdr/Image/2014/08/anh-1-a8931.jpg', 
            'content' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín'
        ],
        [
            'nameTour' => 'TOUR DU LỊCH ĐẢO NGỌC - LĂNG CÔ', 
            'Anh' => 'https://znews-photo.zadn.vn/w660/Uploaded/wyhktpu/2017_09_11/image009.jpg', 
            'content' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín'
        ]  
        ]);
        }
 }
