<?php

use Illuminate\Database\Seeder;

class InfomationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { DB::table('information')->insert
        ([
            'name' => 'Khách Sạn Mường Thanh Chi Nhánh Huế',
            'address' => '01 Bến Nghế, p.Phú Hội, tp Huế',
            'phone'=>"0902050607",
            'fax'=>"0320.0902050",
            'website'=>"https://www.huonggianghotel.com.vn",
            'email'=>"https://www.huonggianghotel.com.vn",
            'content'=>"TỔNG QUAN VỀ KHÁCH SẠN
            Nằm trên con đường Lê Lợi xanh mát của trung tâm thành phố và ngay bên bờ sông Hương thơ mộng, Khách sạn
            Mường Thanh Holiday Huế mang lại bức tranh toàn cảnh về thành phố Huế, sông Hương, Cầu Tràng Tiền và Kinh thành Huế cổ xưa.
            Từ đây, quý khách có thể tản bộ đi thăm chợ đêm hay đến các địa điểm du lịch khác của Huế. Với sự kết hợp kiến trúc và văn
             hóa giữa hiện đại xen lẫn cổ điển, Khách sạn Mường Thanh Holiday Huế là một gam màu nâu trầm ấm và những nét hoa văn của
             Huế xưa gợi lên một không gian thanh bình để nghỉ ngơi, thư giãn.
            Khách sạn Mường Thanh Holiday Huế chỉ cách sân bay Phú Bài và các điểm tham quan khác trong thành phố 20 phút đi xe.
             Ngay bến phà Huế, nơi khách có thể khám phá thành phố một cách độc đáo. Với 108 phòng, mỗi phòng đều được trang bị
             tiện nghi hiện đại như bar mini, truyền hình vệ tinh và két sắt. Đối với khách kinh doanh, có hai phòng họp có thể
             chứa đến 500 người. Địa điểm ăn uống phục vụ từ ẩm thực địa phương cho đến các món ăn Ý hay theo chuyên đề tại nhà
             s hàng Calamus.",
            ]);
    }
}
