<?php

use Illuminate\Database\Seeder;

class EvenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news__evens')->insert
        ([
            'title' => 'Hương Giang mộng mơ', 
            'content' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín',
        ]);
    }
}
