<?php

use Illuminate\Database\Seeder;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('rooms')->insert
        ([
           ['Sophong' => '305', 
            'Mota' => 'Khách sạn Hương Giang có 108 phòng nghỉ với không gian sống sang trọng, nội thất ấm cúng và tiện nghi đẳng cấp thế giới.',
            'Anh' => 'h1.jpg',
            'id_loaiPhong' => '1',
            'Trangthai'=>false,
            ],

           ['Sophong' => '306', 
            'Mota' => 'Khách sạn Hương Giang có 108 phòng nghỉ với không gian sống sang trọng, nội thất ấm cúng và tiện nghi đẳng cấp thế giới.',
            'Anh' => 'h2.jpg',
            'id_loaiPhong' => '2',
            'Trangthai'=>false],

            ['Sophong' => '307', 
            'Mota' => 'Khách sạn Hương Giang có 108 phòng nghỉ với không gian sống sang trọng, nội thất ấm cúng và tiện nghi đẳng cấp thế giới.',
            'Anh' => 'h3.jpg',
            'id_loaiPhong' => '2',
            'Trangthai'=>false],

            ['Sophong' => '308', 
            'Mota' => 'Khách sạn Hương Giang có 108 phòng nghỉ với không gian sống sang trọng, nội thất ấm cúng và tiện nghi đẳng cấp thế giới.',
            'Anh' => 'h5.jpg',
            'id_loaiPhong' => '2',
            'Trangthai'=>false],
            
            ['Sophong' => '309', 
            'Mota' => 'Khách sạn Hương Giang có 108 phòng nghỉ với không gian sống sang trọng, nội thất ấm cúng và tiện nghi đẳng cấp thế giới.',
            'Anh' => 'h6.jpg',
            'id_loaiPhong' => '1',
            'Trangthai'=>false],
        ]);
    }

}
