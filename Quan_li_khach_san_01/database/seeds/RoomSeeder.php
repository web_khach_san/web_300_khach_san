<?php

use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert
        ([
            [ 'Sophong' => '1T300','Mota'=>'Phòng nằm gần sông, có view đẹp','Anh'=>"https://pix8.agoda.net/hotelImages/266919/-1/6be83bb3f633dbb0a3c999c3b5201ac5.jpg",'id_loaiPhong'=>2],
            
        ]);
    }
}
