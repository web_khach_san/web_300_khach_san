<?php

use Illuminate\Database\Seeder;

class AdvertisementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisements')->insert
        ([
            'name' => 'Mát xa sunnna', 
            'content' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín',
            'image' => 'https://bitly.vn/5j8-',
        ]);
    }
}
