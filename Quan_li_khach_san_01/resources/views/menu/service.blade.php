@extends('index')
@section('title')
Dịch vụ
@endsection
@section('banner')
<section class="contact-banner-area" id="gallery">
	<div class="container h-100">
		<div class="contact-banner">
			<div class="text-center">
				<h1>Gallery</h1>
				<nav aria-label="breadcrumb" class="banner-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Gallery</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
@endsection
@section('content')
<section class="section-margin">
	<div class="container">
		<div class="section-intro text-center pb-80px">
			<div class="section-intro__style">
				<img src="plugin/img/home/bed-icon.png" alt="">
			</div>
			<h2>Our Gallery</h2>
		</div>

		<div class="row">
			<div class="col-md-5">
				<div class="row">
					<div class="col-12 mb-4">
						<a href="plugin/img/gallery/g1.png" class="img-gal card-img">
							<div class="single-imgs relative">
								<div class="overlay overlay-bg"></div>
								<div class="relative">
									<img class="card-img rounded-0" src="plugin/img/gallery/g1.png" alt="">
								</div>
							</div>
						</a>
					</div>
					<div class="col-12 mb-4">
						<a href="plugin/img/gallery/g2.png" class="img-gal card-img">
							<div class="single-imgs relative">
								<div class="overlay overlay-bg"></div>
								<div class="relative">
									<img class="card-img rounded-0" src="plugin/img/gallery/g2.png" alt="">
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="col-md-7">
				<div class="row">
					<div class="col-sm-7 mb-4">
						<a href="plugin/img/gallery/g3.png" class="img-gal card-img">
							<div class="single-imgs relative">
								<div class="overlay overlay-bg"></div>
								<div class="relative">
									<img class="card-img rounded-0" src="plugin/img/gallery/g3.png" alt="">
								</div>
							</div>
						</a>
					</div>
					<div class="col-sm-5 mb-4">
						<a href="plugin/img/gallery/g4.png" class="img-gal card-img">
							<div class="single-imgs relative">
								<div class="overlay overlay-bg"></div>
								<div class="relative">
									<img class="card-img rounded-0" src="plugin/img/gallery/g4.png" alt="">
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mb-4">
						<a href="plugin/img/gallery/g5.png" class="img-gal card-img">
							<div class="single-imgs relative">
								<div class="overlay overlay-bg"></div>
								<div class="relative">
									<img class="card-img rounded-0" src="plugin/img/gallery/g5.png" alt="">
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-7 mb-4">
						<a href="plugin/img/gallery/g6.png" class="img-gal card-img">
							<div class="single-imgs relative">
								<div class="overlay overlay-bg"></div>
								<div class="relative">
									<img class="card-img rounded-0" src="plugin/img/gallery/g6.png" alt="">
								</div>
							</div>
						</a>
					</div>
					<div class="col-sm-5 mb-4">
						<a href="plugin/img/gallery/g7.png" class="img-gal card-img">
							<div class="single-imgs relative">
								<div class="overlay overlay-bg"></div>
								<div class="relative">
									<img class="card-img rounded-0" src="plugin/img/gallery/g7.png" alt="">
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection