@extends('index')
@section('title')
Liên hệ
@endsection
@section('banner')
<section class="contact-banner-area" id="contact">
		<div class="container h-100">
			<div class="contact-banner">
				<div class="text-center">
					<h1>Contact Us</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
            </ol>
          </nav>
				</div>
			</div>
    </div>
	</section>
@endsection
@section('content')
<section class="section-margin">
  <div class="container">
    <!-- google map start -->
    <div class="d-none d-sm-block mb-5 pb-4">
      <div id="map" style="height: 420px;">
        <iframe class="" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!
            1d1913.1557637327605!2d107.60379629814501!3d16.459756012535795
            !2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!
            1s0x3141a1137e773335%3A0x72fee7b098a5eab!2zMjYgTmd1eeG7hW4gxJD
            hu6ljIFThu4tuaCwgQW4gxJDDtG5nLCBUaMOgbmggcGjhu5EgSHXhur8sIFRo4burYSBUaGnDqm
            4gSHXhur8sIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1561518817004!5m2!1svi!2s" width="100%" height="420px"
          frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>

    </div>
    <div class="row">
      <div class="col-md-4 col-lg-3 mb-4 mb-md-0">
        <div class="media contact-info">
          <span class="contact-info__icon"><i class="ti-home"></i></span>
          <div class="media-body">
            <h3>California United States</h3>
            <p>Santa monica bullevard</p>
          </div>
        </div>
        <div class="media contact-info">
          <span class="contact-info__icon"><i class="ti-headphone"></i></span>
          <div class="media-body">
            <h3><a href="tel:454545654">00 (440) 9865 562</a></h3>
            <p>Mon to Fri 9am to 6pm</p>
          </div>
        </div>
        <div class="media contact-info">
          <span class="contact-info__icon"><i class="ti-email"></i></span>
          <div class="media-body">
            <h3><a href="mailto:support@colorlib.com">support@colorlib.com</a></h3>
            <p>Send us your query anytime!</p>
          </div>
        </div>
      </div>
      <div class="col-md-8 col-lg-9">
        <form class="row contact_form" action="{{route('trangchu.postlienhe')}}" method="post" accept-charset="utf-8"
          id="contactForm" novalidate="novalidate">
          @csrf
         
          @if (Session::has('success'))
          <p class="text-success" id="hide">
            <i class="fa fa-check" aria-hidden="true"></i>
            {{ Session::get('success') }}
          </p>
          @endif
          <script>
            setTimeout(function(){
                            $('#hide').fadeOut('fast');
                         },5000);
          </script>
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" class="form-control" id="subject" name="subject" placeholder="Chủ đề của bạn (*)">
            </div>
            <div class="form-group">
              <input type="email" class="form-control" id="name" name="name" placeholder="Tên của bạn (*)">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="email" name="email" placeholder="Địa chỉ Email (*)">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <textarea class="form-control different-control" name="content" id="content" rows="5"
                placeholder="Nội dung liên hệ (*)"></textarea>
            </div>
          </div>
          <div class="col-md-12 text-right">
            <button type="submit" value="submit" class="button-contact"><span>Send Message</span></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection