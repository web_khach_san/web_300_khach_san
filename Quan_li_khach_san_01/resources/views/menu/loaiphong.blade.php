@extends('index')
@section('title')
Loại phòng
@endsection
@section('banner')
<section class="contact-banner-area" id="contact">
		<div class="container h-100">
			<div class="contact-banner">
				<div class="text-center">
					<h1>Accomodation</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Accomodation</li>
            </ol>
          </nav>
				</div>
			</div>
    </div>
	</section>
@endsection
@section('content')
<section class="section-margin section-margin--small">
    <div class="container">
      <div class="section-intro text-center pb-80px">
        <div class="section-intro__style">
          <img src="img/home/bed-icon.png" alt="">
        </div>
        <h2>Các loại phòng</h2>
      </div>

      <div class="row pb-4">
        @foreach($loaiphong as $lp)
        <div class="col-md-6 col-xl-4 mb-5">
          <div class="card card-explore">
            <div class="card-explore__img">
              <img class="card-img" src="{{ asset('storage/'.$lp->Anh) }}" alt="">
            </div>
            <div class="card-body">
              <h3 class="card-explore__price">${{ $lp->Gia }} <sub>/ Per Night</sub></h3>
              <h4 class="card-explore__title"><a href="#">{{ $lp->TenLoaiPhong }}</a></h4>
              <p>{{ $lp->Mota }}</p>
              <a class="card-explore__link" href="#">Book Now <i class="ti-arrow-right"></i></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  @endsection