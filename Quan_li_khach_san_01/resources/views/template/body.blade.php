@extends('index')
@section('title')
Trang chủ
@endsection
@section('banner')
<section class="home-banner-area" id="home">
        <div class="container h-100">
          <div class="home-banner">
            <div class="text-center">
              <h4>See What a Difference a stay makes</h4>
              <h1>Luxury <em>is</em> personal</h1>
              <a class="button home-banner-btn" href="#book-tour">Book Now</a>
            </div>
          </div>
        </div>
      </section>
@endsection
@section('content')
    @include('template.body.introduction')
    @include('template.body.room')
    @include('template.body.service')
    @include('template.body.event')
@endsection