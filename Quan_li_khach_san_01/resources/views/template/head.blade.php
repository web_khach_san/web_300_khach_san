<!-- Basic Page Needs -->
<meta charset="utf-8">
   
   <meta name="robots" content="index, follow">
   <meta name="keywords">
   <meta name="description" content="Fibo Holiday Hotel">
   <link rel="icon" href="/favicon.ico">
   <!-- Mobile Specific Metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   <!-- Font-->

   <meta charset="UTF-8">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

   <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet" type="text/css">
   <link href="../plugin/Plugins/slider-nivo/nivo-slider.css" rel="stylesheet" />
   <link href="../plugin/Plugins/assets/css/lib/bootstrap.min.css" rel="stylesheet" />
   <link href="../plugin/Plugins/assets/css/lib/owl.carousel.css" rel="stylesheet" />
   <link href="../plugin/Plugins/assets/css/lib/jquery-ui.min.css" rel="stylesheet" />
   <link href="../plugin/Plugins/assets/css/lib/bootstrap-select.min.css" rel="stylesheet" />

   <link href="../plugin/Plugins/prettyphoto-master/css/prettyPhoto.css" rel="stylesheet" />

   <!-- MAIN STYLE -->

   <link href="../plugin/Plugins/assets/css/style.css" rel="stylesheet" />
   <link href="../plugin/Content/StyleSheet.css" rel="stylesheet" />

   <script src="../plugin/Plugins/assets/js/lib/jquery-1.11.0.min.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/jquery-ui.min.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/bootstrap.min.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/bootstrap-select.js"></script>



   <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" type="text/css" media="all">

   <!-- footer -->
   <script src="../plugin/Plugins/assets/js/lib/isotope.pkgd.min.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/jquery.themepunch.revolution.min.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/jquery.themepunch.tools.min.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/owl.carousel.js"></script>

   <script src="../plugin/Plugins/assets/js/lib/jquery.appear.min.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/jquery.countTo.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/jquery.countdown.min.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/jquery.parallax-1.1.3.js"></script>
   <script src="../plugin/Plugins/assets/js/lib/jquery.magnific-popup.min.js"></script>
   <!--<script type="text/javascript" src="js/lib/SmoothScroll.js"></script>-->
   <script src="../plugin/Plugins/prettyphoto-master/js/jquery.prettyPhoto.js"></script>
   <script src="../plugin/Plugins/tab-zozo/js/zozo.tabs.min.js"></script>
   <script src="../plugin/Plugins/slider-nivo/jquery.nivo.slider.js"></script>
   <script type="text/javascript">
       $(window).load(function() {
           $('#slider').nivoSlider({
               effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
               slices: 15, // For slice animations
               boxCols: 8, // For box animations
               boxRows: 4, // For box animations
               animSpeed: 500, // Slide transition speed
               pauseTime: 3000, // How long each slide will show
               startSlide: 0, // Set starting Slide (0 index)
               directionNav: true, // Next & Prev navigation
               controlNav: false, // 1,2,3... navigation
               controlNavThumbs: false, // Use thumbnails for Control Nav
               pauseOnHover: true, // Stop animation while hovering
               manualAdvance: false, // Force manual transitions
               prevText: '<i class="fas fa-chevron-left"></i>', // Prev directionNav text
               nextText: '<i class="fas fa-chevron-right"></i>', // Next directionNav text
               randomStart: false, // Start on a random slide
           });
       });
       $(document).ready(function() {
           jQuery("a[rel^='prettyPhoto']").prettyPhoto({
               animationSpeed: 'slow',
               theme: 'pp_default',
               gallery_markup: '',
               slideshow: 2000,
               social_tools: ''
           });
           

       });
       
   </script>
   <script src="../plugin/Plugins/assets/js/scripts.js"></script>

   <script src="../plugin/Content/Js/layout_page.js"></script>