<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" href="plugin/img/favicon.png" type="image/png">
<link rel="stylesheet" href="plugin/vendors/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="plugin/vendors/fontawesome/css/all.min.css">
<link rel="stylesheet" href="plugin/vendors/themify-icons/themify-icons.css">
<link rel="stylesheet" href="plugin/vendors/linericon/style.css">
<link rel="stylesheet" href="plugin/vendors/magnefic-popup/magnific-popup.css">
<link rel="stylesheet" href="plugin/vendors/owl-carousel/owl.theme.default.min.css">
<link rel="stylesheet" href="plugin/vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="plugin/vendors/nice-select/nice-select.css">
<link rel="stylesheet" href="plugin/css/style.css">