
<section class="welcome">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-5 mb-4 mb-lg-0">
          <div class="row no-gutters welcome-images">
            <div class="col-sm-7">
              <div class="card">
                <img class="" src="plugin/img/home/welcomeBanner1.png" alt="Card image cap">
              </div>
            </div>
            <div class="col-sm-5">
              <div class="card">
                <img class="" src="plugin/img/home/welcomeBanner2.png" alt="Card image cap">
              </div>
            </div>
            <div class="col-lg-12">
              <div class="card">
                <img class="" src="plugin/img/home/welcomeBanner3.png" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
        @foreach($gioithieu as $gt)
        <div class="col-lg-7">
          <div class="welcome-content">
            <h2 class="mb-4"><span class="d-block">Welcome</span>{{ $gt->name }}</h2>
            <p>Beginning blessed second a creepeth. Darkness wherein fish years good air whose after seed appear midst evenin, appear void give third bearing divide one so blessed moved firmament gathered </p>
            <p>Beginning blessed second a creepeth. Darkness wherein fish years good air whose after seed appear midst evenin, appear void give third bearing divide one so blessed</p>
            <a class="button button--active home-banner-btn mt-4" href="#">Learn More</a>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
