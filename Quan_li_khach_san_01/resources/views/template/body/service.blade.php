<section class="section-margin">
    <div class="container">
      <div class="section-intro text-center pb-20px">
        <div class="section-intro__style">
          <img src="plugin/img/home/bed-icon.png" alt="">
        </div>
        <h2>Our Guest Love Us</h2>
      </div>
      <div class="owl-carousel owl-theme testi-carousel">
        @foreach($dichvu as $dv)
        <div class="testi-carousel__item">
          <div class="media">
            <div class="testi-carousel__img">
              <img src="plugin/img/home/testimonial1.png" alt="">
            </div>
            <div class="media-body">
              <p>{{ $dv->content }}</p>
              <div class="testi-carousel__intro">
                <h3>{{ $dv->nameService }}</h3>
                <p>CEO & Founder</p>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        
    </div>
  </section>