<section class="section-margin">
    <div class="container">
      <div class="section-intro text-center pb-80px">
        <div class="section-intro__style">
          <img src="plugin/img/home/bed-icon.png" alt="">
        </div>
        <h2>News & Events</h2>
      </div>

      <div class="row">
        @foreach($events as $event)
        <div class="col-md-6 col-lg-4 mb-4 mb-md-0">
          <div class="card card-news">
            <div class="card-news__img">
              <img class="card-img" src="plugin/img/home/explore1.png" alt="">
            </div>
            <div class="card-body">
              <h4 class="card-news__title"><a href="#">{{ $event->title }}</a></h4>
              <ul class="card-news__info">
                <li><a href="#"><span class="news-icon"><i class="ti-notepad"></i></span> {{ $event->created_at }}</a></li>
                <li><a href="#"><span class="news-icon"><i class="ti-comment"></i></span> 03 Comments</a></li>
              </ul>
              <p>{{ $event->content }}</p>
              <a class="card-news__link" href="#">Read More <i class="ti-arrow-right"></i></a>
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
    </div>
  </section>