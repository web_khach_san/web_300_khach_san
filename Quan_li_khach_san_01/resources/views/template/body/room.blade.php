
<section class="section-margin">
  <div class="container">
    <div class="section-intro text-center pb-80px">
      <div class="section-intro__style">
        <img src="plugin/img/home/bed-icon.png" alt="">
      </div>
      <h2>Explore Our Rooms</h2>
    </div>

    <div class="row">
        @foreach($loaiphong as $lp)
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="card card-explore">
          <div class="card-explore__img">
            <img class="card-img" src="{{ asset('storage/'.$lp->Anh) }}" alt="">
          </div>
          <div class="card-body">
            <h3 class="card-explore__price">{{ $lp->Gia }} <sub>/ Per Night</sub></h3>
            <h4 class="card-explore__title"><a href="#">{{ $lp->TenLoaiPhong }}</a></h4>
            <p>{{ $lp->Mota }}</p>
            <a class="card-explore__link" href="#">Book Now <i class="ti-arrow-right"></i></a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
