<div class="main_menu">
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <!-- <a class="navbar-brand logo_h" href="index.html"><img src="plugin/img/logo.png" alt=""></a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
          <ul class="nav navbar-nav menu_nav">
            <li class="nav-item active"><a class="nav-link" href="{{ route('Trangchu') }}">Trang Chủ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('Gioithieu') }}">Giới Thiệu</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('Loaiphong') }}">Loại Phòng</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('Dichvu') }}">Dịch Vụ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('Lienhe') }}">Liên Hệ</a></li>
            <li class="nav-item"><a class="nav-link" href="contact.html">Đặt Phòng</a></li>
          </ul>
        </div>

        <ul class="social-icons ml-auto">
          <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
          <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
          <li><a href="#"><i class="fab fa-twitter"></i></a></li>
          <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
          <li><a href="#"><i class="fas fa-rss"></i></a></li>
        </ul>
      </div>
    </nav>
    
    {{--  <div class="search_input" id="search_input_box">
      <div class="container">
        <form class="d-flex justify-content-between">
          <input type="text" class="form-control" id="search_input" placeholder="Search Here">
          <button type="submit" class="btn"></button>
          <span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
        </form>
      </div>
    </div>  --}}
  </div>