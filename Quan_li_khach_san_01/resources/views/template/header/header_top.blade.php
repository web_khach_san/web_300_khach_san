<div class="header-top">
    <div class="container">
      <div class="d-flex align-items-center">
        <div id="logo">
          <a href="index.html"><img src="plugin/img/Logo.png" alt="" title="" /></a>
        </div>
        <div class="ml-auto d-none d-md-block d-md-flex">
          <div class="media header-top-info">
            <span class="header-top-info__icon"><i class="fas fa-phone-volume"></i></span>
            <div class="media-body">
              <p>Have any question?</p>
              <p>Free: <a href="tel:+12 365 5233">+12 365 5233</a></p>
            </div>
          </div>
          <div class="media header-top-info">
            <span class="header-top-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <p>Have any question?</p>
              <p>Free: <a href="tel:+12 365 5233">+12 365 5233</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>