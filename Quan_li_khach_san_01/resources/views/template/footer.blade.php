<footer id="footer">
        <div class="footer_center">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 col-lg-5">
                        <div class="widget widget_logo">
                            <div class="widget-logo">
                                <div class="img">
                                    <a href="/">
                                        <img src="/Content/img/2522017213927logo-footer.png">
                                    </a>
                                </div>
                                @foreach($gioithieu as $gt)
                                <div class="text">
                                    <p style="text-transform: uppercase"><i class="fa fa-building-o"></i>{{ $gt->name }}</p>
                                    <p><i class="fa fa-location-arrow"></i>{{ $gt->address }}</p>
                                    <p><i class="fa fa-phone"></i>{{ $gt->phone }}</p>
                                    <p><i class="fa fa-envelope"></i><a href="{{ $gt->email }}">{{ $gt->email }}</a></p>
                                    <p><i class="fa fa-link"></i><a href="{{ $gt->website }}">{{ $gt->website }}</a></p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <hr style="border-top: rgba(255,255,255,0.3) 1px solid; margin-top: 20px; margin-bottom: 0px;">
                        <div class="widget widget_tripadvisor">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="tripadvisor">
                                        <a>
                                            <img src="/Content/img/trip1.png" style="width: 120px;">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="widget-title">Connect Us On</h4>
                                    <div class="footer_on" style="padding-top: 5px;">
                                        <div class="social">
                                            <div class="social-content">
                                                <a href="https://www.pinterest.com/hanoigolden/hanoi-golden-holiday-hotel/"><i class="fab fa-pinterest"></i></a>
                                                <a href="https://www.facebook.com/webhotel.vn/"><i class="fab fa-facebook"></i></a>
                                                <a><i class="fab fa-twitter"></i></a>
                                                <a href="https://www.youtube.com/channel/UC2tvC6uHNydLWogkn20jzEw"><i class="fab fa-google-plus"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="col-xs-6 col-lg-3">
                            <div class="widget">
                                <h4 class="widget-title" id="1215">VỀ CHÚNG TÔI</h4>
                                <ul>
                                        <li><a href="/lien-he">Liên hệ</a></li>
                                        <li><a href="/dia-chi-ban-do">Địa chỉ &amp; bản đồ</a></li>
                                        <li><a href="/tin-tuc1">Tin tức</a></li>
                                </ul>
                            </div>
                        </div>
                        
                    <div class="col-xs-6 col-lg-4">
                        <div class="widget widget_tripadvisor">
                            <h4 class="widget-title">Our Hotels</h4>
                            <ul>
                                    <li><a href="/tin-tuc1/89/lang-chu-tich-ho-chi-minh">Lăng Chủ Tịch Hồ Chí Minh </a></li>
                                    <li><a href="/tin-tuc1/90/mon-an-dac-san-chau-a">Món ăn đặc sản châu Á </a></li>
                                    <li><a href="/tin-tuc1/92/thanh-co-ha-noi">Thành Cổ Hà Nội</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </footer>