@extends('admin.layout.layout')

@section('content')
<div class="container-fluid">

    <div class="card">

        <div class="card-header">
            <h4 class="card-title">Trang thống kê</h4>
            <div class="col"><hr></div>
        </div>

    <div class="card-body">

        <form action="" method="POST">
            @csrf
            <div class="filter-bar">
                <div class="row">
                    <div class=" row col-sm">
                        <div class="col-sm-3"><p>từ ngày:</p></div>
                        <div class="col-sm-9"><input type="date" class="from-date"></div>
                    </div>
                    <div class=" row col-sm">
                        <div class="col-sm-3"><p>đến ngày:</p></div>
                        <div class="col-sm-9"><input type="date" class="to-date"></div>
                    </div>
                </div>
            </div>
        </form>

        <div class="row">
                <div class="col-sm">
                    <div class="card blue" style="width: 18rem;">
                        <div class="card-header">
                            <h5 class="card-title">Tổng Lượng đặt phòng</h5>
                            <div class="col"><hr></div>
                        </div>
                        <div class="card-body">
                            <p class="counter-count">954</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="card green" style="width: 18rem;">
                        <div class="card-header">
                            <h5 class="card-title">Lượt Click quảng cáo</h5>
                            <div class="col"><hr></div>
                        </div>
                        <div class="card-body">
                            <p class="counter-count">954</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="card red" style="width: 18rem;">
                        <div class="card-header">
                            <h5 class="card-title">lượng Like </h5>
                            <div class="col"><hr></div>
                        </div>
                        <div class="card-body">
                            <p class="counter-count">954</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
