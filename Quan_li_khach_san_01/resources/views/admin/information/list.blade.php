@extends('admin.layout.layout')
@section('content')     


<div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Thông tin khách sạn</h4>
              
              <a href="{{route('admin.themThongTin')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Thêm thông tin
              </a>
              </div>

              @if(session('message'))
              <div class="alert alert-success">
                  <strong>{{session('message')}}</strong>
              </div>
          @endif
          <style>
            .card-body label{
              font-size: 17px;
              font-weight: bold;
            }
          </style>
              <div class="card-body" >
                {{--  <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary text-center">
                            <th>Name</th>
                            <th>Address</th>
                            <th>
                                Phone
                            </th>
                            <th>Fax</th>
                            <th>Email</th>
                            <th>Website</th>                       
                            <th colspan="2">Hành Động</th>
                    </thead>
                    <tbody>
                      
                           
                            <tr>
                                <td>{{ $tt->name }}</td>
                                <td>
                                    {{ $tt->address }}
                                </td>
                          
                                <td>
                                        {{$tt->phone}}                              
                                   
                                </td>
                                <td>
                                        {{$tt->content}}                              
                                   
                                </td>
                                <td>
                                        {{$tt->website}}                              
                                   
                                </td>
                                    
                                    
                                    <td>     
                                           
                                          <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href=""><i class="fa fa-trash"></i></a>
                                        </td>                                    
                            </tr>
                            
                    </tbody>
                  </table>
                </div>  --}}
                @foreach($thongtin as $tt)
                <div class="form-group">
                  <label for="">Tên khách sạn : </label>
                  <span>{{ $tt->name }}</span>
                </div>
                <div class="form-group">
                  <label for="">Email : </label>
                  <span>{{ $tt->email }}</span>
                </div>
                <div class="form-group">
                  <label for="">Địa chỉ : </label>
                  <span>{{ $tt->address }}</span>
                </div>
                <div class="form-group">
                  <label for="">Điện thoại : </label>
                  <span>{{ $tt->phone }}</span>
                </div>
                <div class="form-group">
                  <label for="">Fax: </label>
                  <span>{{ $tt->fax }}</span>
                </div>
                <div class="form-group">
                  <label for="">Website : </label>
                  <span>{{ $tt->website }}</span>
                </div>
                <div class="form-group">
                  <label for="">Nội dung : </label>
                  <span>{{ $tt->content }}</span>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
</div>

{!! $thongtin->render() !!}

@endsection
