@extends('admin.layout.layout')
@section('content')     
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Thêm</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.storeThongTin')}}" method="post">
                    @csrf
                        <div class="form-group">
                            <label for="my-input">name</label>
                            <input id="my-input" class="form-control" type="text" name="name">
                            @if($errors->has('name'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('name')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">email</label>
                            <input id="my-input" class="form-control" type="text" name="email">
                            @if($errors->has('email'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('email')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">address</label>
                            <input id="my-input" class="form-control" type="text" name="address">
                            @if($errors->has('address'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('address')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">phone</label>
                            <input id="my-input" class="form-control" type="text" name="phone">
                            @if($errors->has('phone'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('phone')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">fax</label>
                            <input id="my-input" class="form-control" type="text" name="fax">
                            @if($errors->has('fax'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('fax')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">website</label>
                            <input id="my-input" class="form-control" type="text" name="website">
                            @if($errors->has('website'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('website')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">content</label>
                            <textarea id="summernote" name="content" class="form-control ckeditor"></textarea>
                            @if($errors->has('content'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('content')}}</strong>
                            </div>
                            @endif
                            <input type="submit" class="btn btn-success" value="Thêm thông tin khách sạn">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>

</script>
</div>

@endsection
