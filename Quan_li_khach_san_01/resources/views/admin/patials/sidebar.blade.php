<div class="sidebar styleside" data-color="white" data-active-color="danger">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
  {{-- @if(Auth::check())
    <div class="logo">
      <a href="http://www.creative-tim.com" class="simple-text logo-mini">
        <div class="logo-image-small">
          <img src="../assets/img/logo-small.png">
        </div>
      </a>
      <a href="http://www.creative-tim.com" class="simple-text logo-normal">
        {{ Auth::user()->name }}
        <!-- <div class="logo-image-big">
          <img src="../assets/img/logo-big.png">
        </div> -->
      </a>
    </div> --}}
     {{-- @endif --}}
      <div class="sidebar-wrapper">
      <ul class="nav">
        <li>
        <a href="{{ route('admin.listInfor') }}">
              <i class="nc-icon nc-single-02"></i>
              <p>Thông tin khách sạn</p>
            </a>
          </li>
          {{-- <li>
        <a href="{{ route('admin.listUser') }}">
              <i class="nc-icon nc-tile-56"></i>
              <p>Danh sách người dùng</p>
            </a>
          </li> --}}
          <li>
          <a href="{{ route('admin.danhsachDatPhong') }}">
            <i class="fa fa-align-right"></i>
            <p>Danh sách đặt phòng</p>
          </a>
        </li>
        <li>

        <a href="{{route('admin.danhsach')}}">
            <i class="fa fa-bars"></i>
            <p>Danh sách phòng</p>
          </a>
        </li>
        <li>
            <a href="{{route('admin.danhsachloaiphong')}}">
                <i class="fa fa-wpforms"></i>
                <p>Quản lý loại phòng</p>
            </a>
        </li>

        <li>
            <a href="{{route('admin.danhsachDichVu')}}">
                <i class="fa fa-envira"></i>
                <p>Quản lý dịch vụ</p>
            </a>
        </li>

        <li>
            <a href="{{ route('admin.thongkeAll') }}">
                <i class="nc-icon nc-spaceship"></i>
                <p>Trang thống kê</p>
            </a>
        </li>

        <li class="active-pro">
          <a href="#">
            <i class="nc-icon nc-spaceship"></i>
            <p>Quay lại trang chủ</p>
          </a>
        </li>
      </ul>
    </div>



  </div>
