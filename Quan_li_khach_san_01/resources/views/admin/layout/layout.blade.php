<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <base href="{{ asset('') }}"></base>
        <title>
         Web khách sạn
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="../assets/demo/demo.css" rel="stylesheet" />
        <link rel="stylesheet" href="admin_asset/dist/css/style.css">
        <script type="text/javascript" language="javascript" src="admin_asset/ckeditor/ckeditor.js" ></script>

      </head>

<body>

   <div class="wrapper ">
      @include('admin.patials.sidebar')
      <div class="main-panel">

        @include('admin.patials.navbar')

        <div class="content">
            @yield('content')
        </div>

      @include('admin.patials.footer')
      </div>
    </div>
   <script src="../assets/js/core/jquery.min.js"></script>
   <script src="../assets/js/core/popper.min.js"></script>
   <script src="../assets/js/core/bootstrap.min.js"></script>
   <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
   <!--  Google Maps Plugin    -->
   <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
   <script src="../assets/js/plugins/chartjs.min.js"></script>
   <script src="../assets/js/plugins/bootstrap-notify.js"></script>
   <script src="../assets/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
   <script src="../assets/demo/demo.js"></script>
   <script>
     $(document).ready(function() {
       demo.initChartsPages();
     });
   </script>

   <script src="admin_asset/bower_components/metisMenu/dist/metisMenu.min.js"></script>

   <script src="admin_asset/dist/js/sb-admin-2.js"></script>

@yield('script')
</body>
</html>
