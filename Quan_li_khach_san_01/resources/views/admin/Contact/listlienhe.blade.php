@extends('admin.layout.layout')
@section('content')     


<div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Danh sách liên hệ</h4>
              <p>Tìm thấy: {{count( $lienhe)}}thông tin </p>
              <a href="#" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Thêm thông tin liên hệ
              </a>
              </div>

              @if(session('message'))
              <div class="alert alert-success">
                  <strong>{{session('message')}}</strong>
              </div>
          @endif
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                            <th>Stt</th>
                            <th>Chủ đề liên hệ</th>
                            <th>Tên người liên hệ</th>
                            <th>Email</th>
                            <th>Nội dung</th>
                    </thead>
                    <tbody>
                      
                            @foreach($lienhe as $lh)
                            <tr>
                                <td>{{ $lh->id }}</td>
                                <td>{{ $lh->subject }}</td>
                                <td>{{ $lh->name }}</td>
                                <td>{{ $lh->email }}</td>
                                <td>{{ $lh->content }}</td>
                                
                                <td> 
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{route('admin.xoalienhe',$lh->id)}}"><i class="fa fa-trash"></i></a>
                                </td>

                                
                                @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>

{!! $lienhe->render() !!}

@endsection
