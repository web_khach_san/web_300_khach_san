@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">
    
        <div class="col-lg-12">
                <h3>
                    Thêm phòng <strong></strong> 
                </h3>
        </div>
                         @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    <strong>{{$err}}</strong><br>
                                @endforeach
                            </div>
                        @endif
                        
                        @if(session('error'))
                            <div class="alert alert-danger">
                                <strong>{{session('error')}}</strong>
                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                <strong>{{session('message')}}</strong>
                            </div>
            @endif
        <form action="{{route('admin.themPhong')}}" method="Post" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group">
                  <label >Số phòng</label>
                <input type="text" class="form-control" name="Sophong" placeholder="Nhập số phòng">
                </div>
                <div class="form-group">
                  <label >Giới thiệu về phòng</label>
                  <textarea type="text" class="form-control" name="Mota"  row="10" placeholder="Giới thiệu về phòng"></textarea>
                </div>
                <div class="form-group">
                        <label>Ảnh</label>
               <input type="file" class="form-control" name="imageroom">  
                </div>
                <div class="form-group">
                        <label for="exampleInputPassword1">Loại phòng</label>                
                       <select class="form-control" name="loaiPhong">
                            @foreach($styleRoom as $style)
                            <option 
                            value="{{ $style->id }}">{{ $style->TenLoaiPhong }}</option>
                        @endforeach               
                           </option>
                       </select>
                </div>
                <button type="submit" class="btn btn-primary">Thêm dịch vụ</button>
              </form>
            </div>
        
@endsection