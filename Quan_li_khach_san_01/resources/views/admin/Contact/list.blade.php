@extends('admin.layout.layout')
@section('content')     


<div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Danh sách phòng</h4>
              <p>Tìm thấy: {{count($rooms)}} phòng</p>
              <a href="{{route('admin.themPhong')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Thêm phòng
              </a>
              </div>

              @if(session('message'))
              <div class="alert alert-success">
                  <strong>{{session('message')}}</strong>
              </div>
          @endif
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                            <th>Stt</th>
                            <th>Số phòng</th>
                            <th>Mô tả</th>
                            <th>
                                Loại Phòng
                            </th>
                            <th>Ảnh</th>
                            <th>Trạng thái</th>                       
                            <th>Hành Động</th>
                    </thead>
                    <tbody>
                      
                            @foreach($rooms as $room)
                            <tr>
                                <td>{{ $room->id }}</td>
                                <td>
                                    {{ $room->Sophong }}
                                </td>
                                <td>
                                    {{ $room->Mota}}
                                </td>                           
                                <td>
                                        {{$room->styleRooms->TenLoaiPhong}}                              
                                   
                                </td>
                                <td>
                                        @if($room->Anh)
                                        <img src="upload/imagesRoom/{{ $room->Anh }}" alt="" style="width: 300px; height: 200px">
                                        @else
                                            {{'Chưa có ảnh'}}
                                        @endif
                                    </td>
                                    <td>
                                      <form method='POST' action={{ route('admin.capnhap', $room->id) }}>
                                            @csrf
                                      @if ($room->Trangthai == false)
                                      
                                           <input type='submit' value="Phòng trống">
                                      
                                      @else
                                    
                                      
                                          <input type='submit' value="Phòng có khách"></form> 
                                        
                                        </form>
                                      @endif
                                    </td>
                                    <td>
                                      <td>
                                      
                                       </td>
                                    </td>
                                    <td>
                                          
                                            <a href="{{route('admin.suaDanhSach',$room->id)}}" class="btn btn-info btn-sm">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Sửa 
                                            </a>
                                          
                                            <button data-id="{{$room->id}}" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete">
                                              <i class="fa fa-trash" aria-hidden="true"></i> Xoá
                                          </button>
                                        </td>                                    
                            </tr>
                            @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Xóa phòng</h4>
      </div>
        <div class="modal-body">
            <form id="form-delete">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="del-id">
                <p>Bạn có chắc muốn xóa phòng<strong id="del-id"></strong> này?</p>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="{{route('admin.xoa',$room->id)}}" class="btn btn-danger" id="delete">Xóa</a>
            </div>
            </form> 
        </div>
    </div>
  </div>
</div>
{!! $rooms->render() !!}

@endsection
