@extends('admin.layout.layout')

@section('content')
<div class="container-fluid">
    
<div class="col-lg-12">
        <h3>
            Sửa thông tin phòng số {{$rooms->Sophong}}
        </h3>
    </div>
    <div>
      </div>
      @if(count($errors) > 0)
         <div class="alert alert-danger">
             @foreach($errors->all() as $err)
                 <strong>{{$err}}</strong><br>
             @endforeach
         </div>
     @endif
     
     @if(session('error'))
         <div class="alert alert-danger">
             <strong>{{session('error')}}</strong>
         </div>
     @endif

     @if(session('message'))
         <div class="alert alert-success">
             <strong>{{session('message')}}</strong>
         </div>
@endif
    </div>
<form action="{{route('admin.suaDanhSach',$rooms->id)}}" method="Post" enctype="multipart/form-data">
    
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="form-group">
          <label >Số phòng</label>
        <input type="text" class="form-control" name="Sophong" value="{{$rooms->Sophong}}">
        </div>
        <div class="form-group">
          <label >Giới thiệu về phòng</label>
          <textarea type="text" class="form-control" name="Mota"  row="10" >{{$rooms->Mota}}</textarea>
        </div>
        <div class="form-group">
                <label>Ảnh</label>
                <p>
                <img width="400px" src="upload/imagesRoom/{{ $rooms->Anh }}">
                <input type="file" class="form-control" name="Images">
                </p>                
                
            
        </div>
        <div class="form-group">
                <label for="exampleInputPassword1">Loại phòng</label>                
               <select class="form-control" name="loaiPhong">
                    @foreach($styleRoom as $style)
                    <option 
                    @if($rooms->id_loaiPhong == $style->id)
                        {{ 'selected' }}
                    @endif
                    value="{{ $style->id }}">{{ $style->TenLoaiPhong }}</option>
                @endforeach               

                   </option>
               </select>
        </div>
        <button type="submit" class="btn btn-primary">Sửa</button>
      </form>
    </div>

@endsection