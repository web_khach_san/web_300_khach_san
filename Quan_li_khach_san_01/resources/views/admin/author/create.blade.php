@extends('admin.layout.layout')
@section('content')     
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Thêm</h4>
                   
						@if(Session::has('thanhcong'))
						<div class="alert alert-success">
							{{Session::get('thanhcong')}}
						</div>
						@endif
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.storeUser') }}" method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="name">Họ và tên*</label>
                            <input  class="form-control" type="text" name="name" required>
                            @if($errors->has('name'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('name')}}</strong>
                            </div>
                            @endif
                            <label for="DOB">Ngày sinh</label>
                            <input class="form-control" type="date" name="DOB" required>
                            @if($errors->has('DOB'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('DOB')}}</strong>
                            </div>
                            @endif
                            <label for="phone">Số điện thoại</label>
                            <input class="form-control" type="text" name="phone" required>
                            @if($errors->has('phone'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('phone')}}</strong>
                            </div>
                            @endif
                            <label for="email">email</label>
                            <input class="form-control" type="text" name="email" required>
                            @if($errors->has('email'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('email')}}</strong>
                            </div>
                            @endif
                            <label for="password">password</label>
                            <input class="form-control" type="password" name="password" required>
                            @if($errors->has('password'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('password')}}</strong>
                            </div>
                            @endif
                            <button type="submit" class="btn btn-success">Thêm người dùng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
</script>
</div>

@endsection
