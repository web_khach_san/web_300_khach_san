@extends('admin.layout.layout') 
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Đổi mật khẩu</h4>
                </div>
                @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                    <strong>{{$err}}</strong><br> @endforeach
                </div>
                @endif @if(session('error'))
                <div class="alert alert-danger">
                    <strong>{{session('error')}}</strong>
                </div>
                @endif @if(session('message'))
                <div class="alert alert-success" id="hide">
                    <strong>{{session('message')}}</strong>
                </div>
                @endif @if(session('danger'))
                <div class="alert alert-danger" id="hide">
                    <strong>{{session('danger')}}</strong>
                </div>
                @endif
                <script>
                    setTimeout(function(){
                            $('#hide').fadeOut('fast');
                         },5000);
                </script>
                <div class="card-body">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.postpassword',$user->id) }}" method="post">
                            @csrf
                            <div class="form-group" style="position:relative">
                                <label for="">Mật khẩu cũ</label>
                                <input type="password" name="password_old" class="form-control" require>
                                <a style="position:absolute; top:54%;right:10px;color:#000" class="eye" ref="javascript::void(0)"><i class="fa fa-eye"></i></a>
                                <a style="position:absolute; top:54%;right:10px;color:#000;display:none" class="eye-slash" ref="javascript::void(0)"><i class="fa fa-eye"></i></a>
                            </div>
                            <div class="form-group" style="position:relative">
                                <label for="">Mật khẩu mới</label>
                                <input type="password" name="password" class="form-control">
                                <a style="position:absolute; top:54%;right:10px;color:#000" class="eye" ref="javascript::void(0)"><i class="fa fa-eye"></i></a>
                                <a style="position:absolute; top:54%;right:10px;color:#000;display:none" class="eye-slash" ref="javascript::void(0)"><i class="fa fa-eye"></i></a>                                </div>
                            <div class="form-group" style="position:relative">
                                <label for="">Nhập lại mật khẩu mới</label>
                                <input type="password" name="re_password" class="form-control">
                                <a style="position:absolute; top:54%;right:10px;color:#000" class="eye" ref="javascript::void(0)"><i class="fa fa-eye"></i></a>
                                <a style="position:absolute; top:54%;right:10px;color:#000;display:none" class="eye-slash" ref="javascript::void(0)"><i class="fa fa-eye"></i></a>                                </div>
                            <button type="submit" class="btn btn-primary" value=""><i class="fa fa-save"></i> Cập nhật</button>
                        </form>
                    </div>
                    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
                    <script>
                        $(function(){
                                $(".form-group .eye").click(function(){
                                    let $this = $(this);
                                    if($this.hasClass('active')){
                                        $this.parents('.form-group').find('input').attr('type','password');
                                        $this.removeClass('active');
                                    }else{
                                        $this.parents('.form-group').find('input').attr('type','text');
                                        $this.removeClass('active');
                                    }
                                    $('.eye').hide();
                                    $('.eye-slash').show();
                                });
                                $(".form-group .eye-slash").click(function(){
                                    let $this = $(this);
                                    if($this.hasClass('active')){
                                        $this.parents('.form-group').find('input').attr('type','text');
                                        $this.removeClass('active');
                                    }else{
                                        $this.parents('.form-group').find('input').attr('type','password');
                                        $this.removeClass('active');
                                    }
                                    $('.eye').show();
                                    $('.eye-slash').hide();
                                });
                            });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection