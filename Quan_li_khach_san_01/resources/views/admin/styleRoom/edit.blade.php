@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">
    
        <div class="col-lg-12">
                <h3>
                Sửa thông tin về loại phòng <strong>{{$styleRooms->TenloaiPhong}}</strong> 
                </h3>
        </div>
                         @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    <strong>{{$err}}</strong><br>
                                @endforeach
                            </div>
                        @endif
                        
                        @if(session('error'))
                            <div class="alert alert-danger">
                                <strong>{{session('error')}}</strong>
                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                <strong>{{session('message')}}</strong>
                            </div>
            @endif
    <form action="{{route('admin.SuaLoaiPhong',$styleRooms->id)}}" method="Post" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group">
                  <label >Mã số loại phòng</label>
                <input type="text" class="form-control" name="maSo" value="{{$styleRooms->MaLoaiPhong}}">
                </div>
                <div class="form-group">
                  <label >Tên loại phòng</label>
                  <textarea type="text" class="form-control" name="nameStyle"  row="10">{{$styleRooms->TenLoaiPhong}}</textarea>
                </div>
                <div class="form-group">
                        <label >Mô tả về loại phòng</label>
                        <textarea type="text" class="form-control ckeditor" name="Mota"  row="10" >{{$styleRooms->Mota}}</textarea>
                      </div>
                <div class="form-group">
                        <label>Ảnh</label>
                        <p>
                                
                                <img width="400px" src="{{asset('storage/'.$styleRooms->Anh )}}">
                <input type="file" class="form-control" name="Images">
                <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" /> 
                        </p>
                </div>
                <div class="form-group">
                        <label >Giá loại phòng (đơn vị: VNĐ)</label>
                      <input type="number" class="form-control" name="Gia" value='{{$styleRooms->Gia}}'>
                      </div>
                <button type="submit" class="btn btn-primary">Cập nhập loại phòng</button>
              </form>
            </div>
        
@endsection