@extends('index')
@section('content')
<div class="container">
    <div class="col-md-9 col-md-push-3">
        <div class="home-about">
            <div class="text">
                <h1 class="heading" style="text-transform: uppercase">
                    Giới Thiệu
                </h1>
                @foreach($gioithieu as $gt)
                <div class="content-full">
                    <p style="margin-left:auto; margin-right:auto; text-align:justify">
                        {{$gt->content}}
                    </p>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @include('trangchu.aboutus')
    @endsection