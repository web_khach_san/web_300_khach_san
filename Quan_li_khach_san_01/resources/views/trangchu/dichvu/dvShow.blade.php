@extends('index')
@section('content')
<section class="section-room bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <div class="clearfix">
                    <small class="title-sub">Dịch vụ</small>

                </div>

                <div class="room-wrap-1">
                    <div class="row">
                        @foreach($dichvu as $dv)
                        <div class="col-md-4 col-sm-6 col-xs-12 mg-bottom-30">
                                
                            <div class="room_item-5" style="background-image: url({{ asset('storage/'.$dv->images ) }});">
                                <div class="img">

                                    <a href="/TOURS INFO/44/​phu-quoc-tour-south"><img src="{{ asset('storage/'.$dv->images ) }}"
                                            alt="​Phu Quoc tour south" ></a>

                                </div>
                                <div class="room_item-forward">
                                    <h2><a href="/TOURS INFO/44/​phu-quoc-tour-south">{{$dv->nameService}}</a></h2>
                                </div>
                                <div class="room_item-back">
                                    <h3>​{{$dv->nameService}}</h3>
                                    <a>{{$dv->content}}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @include('trangchu.aboutus')
</section>

@endsection



{{-- 



@extends('index')
@section('content')
<section class="section-room bg-white">
    <div class="container">
        <h1 id="H1Title" class="main-header text-center">Dịch Vụ</h1>
        <div class="">
            <div>
                <div class="room-wrap-2">
                    @foreach($dichvu as $dv)
                    <div class="room_item-2 img-right">
                        <div class="img">
                            <a href="/ROOMS/15/deluxe-room">
                                <img src="{{$dv->images}}" alt="Deluxe room">
</a>
</div>
<div class="text">
    <h2>
        <a href="#">{{$dv->nameService}}</a>
    </h2>
    <div>
        <div style="text-align:justify">{{$dv->content}}</div>
    </div>
    <a href="{{route('dichvu.chitiet',$dv->id)}}" class="awe-btn awe-btn-default">VIEW DETAILS</a>
</div>

</div>
@endforeach
</div>
</div>
</div>
</div>
</section>
@endsection --}}