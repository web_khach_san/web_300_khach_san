<section class="section-team">
            <div class="container">
                <div class="team">
                    <h2 class="heading text-center">Meet Our Team</h2>

                    <div class="team_content">

                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <div class="team_item text-center">

                                    <div class="img">
                                        <a href="javascript:;">
                                            <img src="./plugin/files/images/slide/1.jpg" />
                                        </a>
                                    </div>

                                    <div class="text mg-top-20">
                                        <h2 style="font-size: 14px;">NH&#211;M QUẢN L&#221;</h2>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <div class="team_item text-center">

                                    <div class="img">
                                        <a href="javascript:;">
                                            <img src="./plugin/files/images/slide/1.jpg" />
                                        </a>
                                    </div>

                                    <div class="text mg-top-20">
                                        <h2 style="font-size: 14px;">SALES &amp; RECEPTION</h2>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <div class="team_item text-center">

                                    <div class="img">
                                        <a href="javascript:;">
                                            <img src="./plugin/files/images/slide/1.jpg" />
                                        </a>
                                    </div>

                                    <div class="text mg-top-20">
                                        <h2 style="font-size: 14px;">FIBO HOLIDAY TEAM</h2>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <div class="team_item text-center">

                                    <div class="img">
                                        <a href="javascript:;">
                                            <img src="./plugin/files/images/slide/1.jpg" />
                                        </a>
                                    </div>

                                    <div class="text mg-top-20">
                                        <h2 style="font-size: 14px;">BẢO VỆ</h2>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <div class="team_item text-center">

                                    <div class="img">
                                        <a href="javascript:;">
                                            <img src="./plugin/files/images/slide/1.jpg" />
                                        </a>
                                    </div>

                                    <div class="text mg-top-20">
                                        <h2 style="font-size: 14px;">HOUSEKEEPING</h2>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <div class="team_item text-center">

                                    <div class="img">
                                        <a href="javascript:;">
                                            <img src="./plugin/files/images/slide/1.jpg" />
                                        </a>
                                    </div>

                                    <div class="text mg-top-20">
                                        <h2 style="font-size: 14px;">FOOD &amp; BEVERAGE</h2>

                                    </div>

                                </div>
                            </div>


                        </div>

                    </div>
                </div>

            </div>
        </section>