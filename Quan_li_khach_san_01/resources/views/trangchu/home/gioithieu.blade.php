<section class="section-home-about bg-white">
            <div class="container">
                <div class="home-about">
                    <div class="row">
                        @foreach($gioithieu as $gt)
                        <div class="col-md-6 mg-bottom-30">
                            <div class="text">
                                <h2 class="heading mg-bottom-20" style="text-transform: uppercase">
                                    Ch&#224;o mừng đến với {{$gt->name}}

                                </h2>
                                <div>
                                    <div style="text-align:justify">
                                    <p style="text-align:justify">{{$gt->content}}</p>

                                    </div>

                                </div>
                                <a href="/Gioi-thieu/61/chao-mung-den-voi-fibo-holiday" class="awe-btn awe-btn-default mg-top-10">READ MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6 mg-bottom-30">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <a href="/Gioi-thieu/61/chao-mung-den-voi-fibo-holiday">
                                            <img alt="Ch&#224;o mừng đến với Fibo Holiday" src="./plugin/files/images/slide/1.jpg" style="width: 100%;">
                                        </a>
                                    </p>
                                    <iframe src="https://www.facebook.com/plugins/like.php?href=http%3A%2F%2F127.0.0.1%3A8000%2F&width=140&layout=button&a
                                   ction=like&size=large&show_faces=true&share=true&height=65&appId" width="140" height="65" style="border:none;overflow:
                                   hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </section>