<section class="section-guestbook-event bg-white">
            <div class="container">
                <div class="guestbook-event">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="heading">SPECIAL PACKAGES</h2>
                            <div class="event-slide owl-single owl-carousel owl-theme" id="owl">
                                @foreach($tour as $nametour)
                                <div class="item">
                                    <div class="event-item">
                                        <div class="event-item">
                                            <div class="img">
                                                <a class="hover-zoom" href="/TOURS/48/​tour-du-lich-nam-phu-quoc" title="​Tour du lịch nam Ph&#250; Quốc">
                                               
                                                    <img src="./plugin/files/images/slide/1.jpg" />
                                                </a>
                                            </div>
                                            <div class="text leght-text">
                                                <div class="text-cn">
                                                    <h2>​​{{$nametour->nameTour}}</h2>
                                                <span>{{$nametour->content}}</span>

                                                    <a href="/TOURS/48/​tour-du-lich-nam-phu-quoc" class="awe-btn awe-btn-12">VIEW DETAIL <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h2 class="heading">OUR SERVICES</h2>
                            <div class="event-slide">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="accomd-modations-room" style="margin-bottom: 30px; ">
                                            <div class="img">
                                                <a href="/nha-hang/9/nha-hang-fibo">
                                                    <img src="./plugin/files/images/slide/1.jpg" />
                                                </a>
                                            </div>
                                            <div class="text">
                                                <h2><a href="/nha-hang/9/nha-hang-fibo">Nh&#224; h&#224;ng FIBO</a></h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="accomd-modations-room" style="margin-bottom: 30px;">
                                            <div class="img">
                                                <a href="/su-kien/10/su-kien">
                                                    <img src="./plugin/files/images/slide/1.jpg" />
                                                </a>
                                            </div>
                                            <div class="text">
                                                <h2><a href="/su-kien/10/su-kien">Sự kiện</a></h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="accomd-modations-room" style="margin-bottom: 30px;">
                                            <div class="img">
                                                <a href="/spa1/16/spa">
                                                    <img src="./plugin/files/images/slide/1.jpg" />
                                                </a>
                                            </div>
                                            <div class="text">
                                                <h2><a href="/spa1/16/spa">Spa</a></h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="accomd-modations-room" style="margin-bottom: 30px;">
                                            <div class="img">
                                                <a href="/be-boi/17/be-boi">
                                                    <img src="./plugin/files/images/slide/1.jpg" />
                                                </a>
                                            </div>
                                            <div class="text">
                                                <h2><a href="/be-boi/17/be-boi">Bể bơi</a></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>

            </div>
        </section>