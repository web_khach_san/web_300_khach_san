<section id="contact">
  <div class="container">
    <div class="well well-sm">
      <h3><strong>LIÊN HỆ VỚI CHÚNG TÔI</strong></h3>
    </div>

    <div class="row" style="height:420px;">
      <div class="col-md-6 col-sm-3">
        <iframe class="" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!
                                              1d1913.1557637327605!2d107.60379629814501!3d16.459756012535795
                                              !2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!
                                              1s0x3141a1137e773335%3A0x72fee7b098a5eab!2zMjYgTmd1eeG7hW4gxJD
                                              hu6ljIFThu4tuaCwgQW4gxJDDtG5nLCBUaMOgbmggcGjhu5EgSHXhur8sIFRo4burYSBUaGnDqm
                                              4gSHXhur8sIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1561518817004!5m2!1svi!2s"
          width="100%" height="420px" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>

      <div class="col-md-6 col-xs-12">
        <h4><strong>Điền Thông Tin</strong></h4>
        <div class="col-12">
          @if (Session::has('success'))
          <p class="text-success" id="hide">
            <i class="fa fa-check" aria-hidden="true"></i>
            {{ Session::get('success') }}
          </p>
          @endif
          <script>
            setTimeout(function(){
                            $('#hide').fadeOut('fast');
                         },5000);
          </script>
        </div>
        <form action="{{route('trangchu.postlienhe')}}" method="post" accept-charset="utf-8" id="contact_form">
          @csrf
          <div class="form-group">
            <input type="text" name="subject" class="form-control custom-input" required="required"
              placeholder="Chủ đề của bạn (*)">
          </div>
          <div class="form-group">
            <input type="text" name="name" class="form-control custom-input" required="required"
              placeholder="Tên của bạn (*)">
          </div>
          {{-- <div class="form-group">
                                                    <input type="number" name="number" class="form-control custom-input" required="required" placeholder="Số điện thoại của bạn (*)">
                                                </div> --}}
          <div class="form-group">
            <input type="email" name="email" class="form-control custom-input" required="required"
              placeholder="Địa chỉ Email (*)">
          </div>
          <div class="form-group">
            <textarea class="form-control" name="content" placeholder="Nội dung liên hệ (*)" rows="9"
              required="required"></textarea>
          </div>
          <div class="form-group">
            <button class="btn btn-block btn-default" type="submit"><i class="fas fa-paper-plane"></i> Gửi tin
              nhắn</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Histats.com  (div with counter) -->
  <div id="histats_counter"></div>
  <!-- Histats.com  START  (aync)-->
  <script type="text/javascript">
    var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,4278424,4,339,112,48,00011111']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();
  </script>
  <noscript><a href="/" target="_blank"><img src="//sstatic1.histats.com/0.gif?4278424&101" alt=""
        border="0"></a></noscript>
  <!-- Histats.com  END  -->
</section>