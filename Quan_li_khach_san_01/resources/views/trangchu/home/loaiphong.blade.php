<section class="section-accommo_1">
            <div class="container">
                <div class="accomd-modations_1">
                    <h2 class="heading">ROOMS & SUITES</h2>
                    <div class="accomd-modations-content_1">
                        <div class="accomd-modations-slide_1 owl-carousel owl-theme" id="owl1" style="display: block; opacity: 1;">

                             @foreach($loaiphong as $lp)
                            <div class="item" >    
                                <div class="accomd-modations-room_1">
                                    <div class="img">
                                        <a href="/loai-phong/12/deluxe-room" title="TeamplateHotel.Models.ShowObject.<Title></Title>">
                                          
                                            @if($lp->Anh)
                                        <img src="{{ asset('storage/'.$lp->Anh ) }}" >
                                        @else
                                            {{'Chưa có ảnh'}}
                                        @endif
                                        </a>
                                    </div>

                                    <div class="text">
                                    <h2><a href="/loai-phong/12/deluxe-room">{{$lp->TenLoaiPhong}}</a></h2>
                                        <div class="wrap-price">
                                            <p class="price">
                                                <span class="amout-price">From:</span>
                                                <span class="amout"> {{$lp->Gia}}&nbspUSD </span>
                                            </p>
                                            <a href="{{route('loaiphong.show',$lp->id)}}" class="awe-btn awe-btn-default">
                                            VIEW DETAIL
                                        </a>
                                        </div>
                                    </div>
                                </div>
                              
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>