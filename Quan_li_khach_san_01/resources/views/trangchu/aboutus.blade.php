<div class="col-md-3 col-md-pull-9">
        <div class="submit-wrapper">
                <a href="{{route('trangchu.datphong')}}">ĐẶT PHÒNG</a>
            </div>
   <div class="">
       <div class="widget widget_categories">
           
           <div class="panel panel-info">
               <div class="panel-heading">CÁC LOẠI PHÒNG</div>
               @foreach($loaiphong as $lp)
               <div class="panel-body" style="">
                   <h2 style="line-height:35px;font-size:15px;margin-left:10px"><a href="{{route('loaiphong.show',$lp->id)}}">{{$lp->TenLoaiPhong}}</a></h2>
               </div>
               @endforeach
           </div>
               
               <div class="">
                   <div class="panel panel-info">
                       <div class="panel-heading">KHUYẾN MÃI</div>
                       <div id="myCarousel" class="carousel slide " data-ride="carousel">
                           <ol class="carousel-indicators">
                               <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                               <li data-target="#myCarousel" data-slide-to="1"></li>
                               <li data-target="#myCarousel" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                               <div class="item active">
                                   <img src="./plugin/files/images/slide/7.jpg" style="height:250px"
                                       alt="Los Angeles">
                               </div>
                               <div class="item">
                                   <img src="/plugin/files/images/slide/8.jpg" style="height:250px" alt="Chicago">
                               </div>
                               <div class="item">
                                   <img src="/plugin/files/images/slide/9.jpg" style="height:250px" alt="New York">
                               </div>
                           </div>
                           <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                               <span class="fa fa-chevron-left"></span>
                               <span class="sr-only">Previous</span>
                           </a>
                           <a class="right carousel-control" href="#myCarousel" data-slide="next">
                               <span class="fa fa-chevron-right"></span>
                               <span class="sr-only">Next</span>
                           </a>
                       </div>
                   </div>
               </div>
       </div>
   </div>
</div>