@extends('index')
@section('content')
<div class="container">
    <div class="col-md-9 col-md-push-3">
        Loại phòng: <h3 class="title-sub">{{ $loai->TenLoaiPhong }}</h3>
        <section class="section-white">


            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" style="height:200px;">
                    <div class="item active">
                        <img src="/plugin/files/images/slide/3.jpg" alt="..." width="100%">
                    </div>
                    <div class="item">
                        <img src="/plugin/files/images/slide/5.jpg" alt="..."  width="100%">

                    </div>
                    <div class="item">
                        <img src="/plugin/files/images/slide/8.jpg" alt="..."  width="100%">
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="fa fa-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="fa fa-chevron-right"></span>
                </a>
            </div>
        </section>
        <div class="room-detail_tab">
            <p>{{$loai->Mota}}</p>
            <strong>Tiện &iacute;ch trong ph&ograve;ng:</strong>
            <table border="0" cellpadding="8" cellspacing="0" style="width:100%">
                <tbody>
                    <tr>
                        <td style="width:50%">
                            <ul>
                                <li>Ph&ograve;ng kh&aacute;ch với salon bọc da</li>
                                <li>Giường ngủ kingsize với chăn, ga, gối l&ocirc;ng vũ 100% cotton</li>
                                <li>Ti vi LCD SONY 40 inch</li>
                                <li>Wifi v&agrave; internet miễn ph&iacute;</li>
                                <li>B&agrave;n l&agrave;m việc</li>
                                <li>Điều h&ograve;a 2 chiều với c&ocirc;ng suất lớn</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>B&agraven tr&agrave; với tr&agrave; v&agrave; caf&eacute; miễn ph&iacute;</li>
                                <li>Minibar v&agrave; c&aacute;c loại đồ uống</li>
                                <li>Dịch vụ ph&ograve;ng</li>
                                <li>Điện thoại gọi trực tiếp trong nước v&agrave; quốc tế</li>
                                <li>Bồn tắm nằm với v&ograve;i hoa sen v&agrave; hệ thống nước n&oacute;ng lạnh</li>
                                <li>Miễn ph&iacute; chỗ để xe tại kh&aacute;ch sạn</li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <button type="button" class="btn btn-primary"><a href="{{route('trangchu.datphong')}}">ĐẶT PHÒNG</a> </button>

    </div>
    @include('trangchu.aboutus')
    @endsection