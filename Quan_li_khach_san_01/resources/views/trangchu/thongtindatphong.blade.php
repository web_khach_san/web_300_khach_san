@extends('index')
@section('content')
<div class="container styleroom">
    <div class="col-md-9 col-md-push-3">
        <div class="home-about">
            <div class="text">
                <h1 class="heading" style="text-transform: uppercase">
                    ĐẶT PHÒNG
                </h1>
                @foreach($thongtindatphong as $thongtin)
                <div class="container-full">
                    <p style="margin-left:auto; margin-right:auto; ">
                        {{$thongtin->content}}
                    </p>
                    <p>
                        Giờ check-in:{{$thongtin->checkIn}}
                    </p>
                    <p>Giờ check-out:{{$thongtin->checkOut}}</p>
                    <p><strong>{{$thongtin->note}}</strong></p>
                </div>
                @endforeach
            </div>
            <h4><strong>Các loại phòng khách sạn</strong></h4>
            <div class="accomd-modations-content_1">
                <div class="accomd-modations-slide_1 owl-carousel owl-theme" id="owl1"
                    style="display: block; opacity: 1;">

                    @foreach($loaiphong as $lp)
                    <div class="item">
                        <div class="accomd-modations-room_1">
                            <div class="img">
                                <a href="{{route('loaiphong.show',$lp->id)}}"
                                    title="TeamplateHotel.Models.ShowObject.<Title></Title>">
                                    @if($lp->Anh)
                                    <img src="upload/imageStyleRoom/{{ $lp->Anh }}" >
                                    @else
                                        {{'Chưa có ảnh'}}
                                    @endif
                                </a>
                            </div>

                            <div class="text">
                                <h2><a href="{{route('loaiphong.show',$lp->id)}}">{{$lp->TenLoaiPhong}}</a></h2>
                                <div class="wrap-price">
                                    <p class="price">
                                        <span class="amout-price">From:</span>
                                        <span class="amout"> {{$lp->Gia}}&nbspUSD </span>
                                    </p>
                                    <a href="{{route('loaiphong.show',$lp->id)}}" class="awe-btn awe-btn-default">
                                        VIEW DETAIL
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @include('trangchu.aboutus')
    @endsection