@extends('index')
@section('content')
<section class="section-room bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <div class="clearfix">
                    <small class="title-sub">TOURS</small>

                </div>

                <div class="room-wrap-1">
                    <div class="row">
                        @foreach($tour as $tours)
                        <div class="col-md-4 col-sm-6 col-xs-12 mg-bottom-30">
                            <div class="room_item-5" style="background-image: url({{$tours->Anh}});">
                                <div class="img">
                                    
                                    <a href="/TOURS INFO/44/​phu-quoc-tour-south"><img src="{{ asset('storage/'.$tours->Anh ) }}"
                                            alt="​Phu Quoc tour south"></a>

                                </div>
                                <div class="room_item-forward">
                                    <h2><a href="/TOURS INFO/44/​phu-quoc-tour-south">{{$tours->nameTour}}</a></h2>
                                </div>
                                <div class="room_item-back">
                                    <h3>​{{$tours->nameTour}}</h3>
                                    <a>{{$tours->content}}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @include('trangchu.aboutus')
                <div class="sidebar">
                    <div class="widget widget_categories">
                        <div class="panel panel-info">
                            <div class="panel-heading">CÁC LOẠI PHÒNG</div>
                            @foreach($loaiphong as $lp)
                            <div class="panel-body">
                                <h2 style="font-size:18px"><a
                                        href="{{route('loaiphong.show',$lp->id)}}">{{$lp->TenLoaiPhong}}</a></h2>
                            </div>
                            @endforeach
                        </div>
                        <div class="row ">
                            <div class="submit-wrapper">
                                <a href="{{route('trangchu.datphong')}}">ĐẶT PHÒNG</a>
                            </div>
                            <div class="reser">
                                <div class="panel panel-info">
                                    <div class="panel-heading">KHUYẾN MÃI</div>
                                    <div id="myCarousel" class="carousel slide " data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel" data-slide-to="1"></li>
                                            <li data-target="#myCarousel" data-slide-to="2"></li>
                                        </ol>
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <img src="./plugin/files/images/slide/7.jpg" style="height:250px"
                                                    alt="Los Angeles">
                                            </div>
                                            <div class="item">
                                                <img src="/plugin/files/images/slide/8.jpg" style="height:250px"
                                                    alt="Chicago">
                                            </div>
                                            <div class="item">
                                                <img src="/plugin/files/images/slide/9.jpg" style="height:250px"
                                                    alt="New York">
                                            </div>
                                        </div>
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</section>

@endsection