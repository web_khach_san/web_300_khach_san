@extends('index')
@section('content')
<section class="section-room-detail bg-white">
    <div class="container">
        <div class="room-detail">
            <div class="row">
                <div class="col-lg-3">
                    <div class="room-detail_tab">
                        <h3 class="title-sub">{{$tour->nameTour}}</h3>
                        <p>{{$tour->content}}</p>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="room-detail_img owl-carousel owl-theme">
                        <div class="item">
                            <div class="room_img-item">
                                    
                                <img src="{{ asset('storage/'.$tour->Anh) }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="room-detail_compare">
                    <h2 class="room-compare_title">OTHER Tours</h2>
                    <div class="room-compare_content">
                        <div class="row">
                            <div class="owl-carousel owl-theme slideshow owl-other" style="display: block; opacity: 1;">
                                <div class="item">
                                    <div class="room-compare_item">
                                        <div class="img">
                                            <a href="#">
                                                <img src="{{ asset('storage/'.$tour->Anh) }}" alt="Superior room">
                                            </a>
                                        </div>
                                        <div class="text">
                                            <h2><a href="#">{{ $tour->nameTour}}</a></h2>
                                            <div>
                                                <div style="text-align: justify;">{{$tour->content}}</div>
                                            </div>
                                            <a href="{{route('tours.chitiet',$tour->id)}}"
                                                class="awe-btn awe-btn-default">VIEW DETAILS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection