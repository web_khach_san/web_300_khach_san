@extends('index')
@section('title')
Trang chủ
@endsection
@section('content')
    @include('trangchu.home.gioithieu')
    @include('trangchu.home.loaiphong')  

    @include('trangchu.home.googlemap_lienhe')
    @include('trangchu.home.chat')
        
@endsection