@extends('index')
@section('title')
Đặt phòng
@endsection
@section('content')
<div class="container n-ding">
    <div class="row">
        <div class=" Rocks">
            <div class="send-email  wow fadeInDown animated animated"
                style="visibility: visible; animation-name: fadeInDown;">
                <div id="main-contact-form" class="contact-form">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                        <strong>{{$err}}</strong><br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('error'))
                    <div class="alert alert-danger">
                        <strong>{{session('error')}}</strong>
                    </div>
                    @endif
                    @if(session('message'))
                    <div class="alert alert-success" id="hide">
                        <strong>{{session('message')}}</strong>
                    </div>
                    @endif
                    @if(session('danger'))
                    <div class="alert alert-danger" id="hide">
                        <strong>{{session('danger')}}</strong>
                    </div>
                    @endif
                    <script>
                        setTimeout(function(){
                            $('#hide').fadeOut('fast');
                         },5000);
                    </script>
                    <form action="{{ route('trangchu.postDatPhong') }}" id="book-tour" method="post"
                        novalidate="novalidate">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-info ">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Step 1: Đặt Phòng</h4>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="CheckIn">Ngày đến<span class="required"
                                                        aria-required="true">*</span></label>
                                                <input class="form-control hasDatepicker valid" type="date"
                                                    name="ngayden" id="from" aria-invalid="false">

                                            </div>
                                            @if($errors->has('ngayden'))
                                            <div class="alert alert-danger">
                                                <strong>{{ $errors->first('ngayden')}}</strong>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="CheckOut">Ngày đi<span class="required"
                                                        aria-required="true">*</span></label>
                                                <input class="form-control hasDatepicker valid" type="date"
                                                    name="ngaydi" id="to" value="date" aria-invalid="false">
                                            </div>
                                            @if($errors->has('ngaydi'))
                                            <div class="alert alert-danger">
                                                <strong>{{ $errors->first('ngaydi')}}</strong>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="CheckOut">Số người<span class="required"
                                                        aria-required="true">*</span></label>
                                                <input class="form-control hasDatepicker valid" type="text"
                                                    name="soNguoi" id="CheckOut" aria-invalid="false">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Loại phòng </label><span class="required"
                                                    aria-required="true">*</span><br>

                                                <select class="form-control" name="loaiphong">
                                                    @foreach($loaiphong as $cate)
                                                    <option value="{{ $cate->TenLoaiPhong }}">{{ $cate->TenLoaiPhong }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info ">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Step 2: Thông Tin Khách Hàng</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Họ tên</label> <span class="required"
                                                        aria-required="true">*</span><br>
                                                    <input type="text" name="hoten" size="40" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Số điện thoại</label><br>
                                                    <input type="text" name="dienthoai" size="40" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Email </label><span class="required"
                                                        aria-required="true">*</span><br>
                                                    <input type="email" name="email" value="" size="40"
                                                        class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 agree">
                                        <input type="checkbox" name="agree" id="agree"> <span>Tôi đã đọc và đồng ý với
                                            các điều khoản và điều kiện của khách sạn Fibo Holiday Hotel</span>
                                    </div>

                                    <div class="clear-fix"></div>
                                    <br>
                                    <div class="col-md-12" style="margin-top: 20px;">
                                        <input type="submit" value="Gửi" class="btn btn-primary">
                                    </div>

                                </div>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>
</div>

@endsection
