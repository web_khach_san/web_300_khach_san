@extends('index')
@section('content')
<div class="container">
    <div class="col-md-9 col-md-push-3">
        <div class="home-about">
            <div class="text">
                <h1 class="heading" style="text-transform: uppercase">
                    Liên Hệ
                </h1>
                <div class="content-full">
                    <p>
                        <strong>Fibo Holiday Hotel</strong>
                        <br>
                        <abbr title="Address"><strong>Address:</strong></abbr>
                        Tầng 4, 147 Mai Dịch, Cầu Giấy, Hà Nội<br>
                        <abbr title="Phone Number"><strong>Phone:</strong></abbr>
                        (024) 6259 8807<br>
                        <abbr title="Hotline"><strong>Hotline:</strong></abbr>
                        (024) 6259 8807<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr>
                        <a href="mailto:info@webhotel.vn" title="info@webhotel.vn">info@webhotel.vn</a><br>
                        <abbr title="Website"><strong>Website:</strong></abbr>
                        <a href="http://fiboholiday.webhotel.vn"
                            title="http://fiboholiday.webhotel.vn">http://fiboholiday.webhotel.vn</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    @include('trangchu.aboutus')

            @endsection