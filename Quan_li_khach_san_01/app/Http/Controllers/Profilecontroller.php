<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;

class Profilecontroller extends Controller
{
    public function getProfile()
    {
    	$profile = Admin::find();
    	return view('admin.author.profile',['profile'=>$profile]);
    }
}
