<?php

namespace App\Http\Controllers;

use App\Information;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lisInfor(Request $request)
    {
        $thongtin = Information::orderBy('id','DESC')->paginate(3);
        return view('admin.information.list',compact('thongtin'))->with('i', ($request->input('page', 1) - 1) * 5);
    }


    public function themThongTin()
    {
        
        return view('admin.information.add');
    }

     public function storeDanhSachPhong(Request $request)
    {
        $this->validate($request,
		   [
			'name'=>'required',
            'address' =>'required' ,
            'phone' =>'required|regex:/(01)[0-9]{9}/' ,
            'email' =>'required' ,
            'fax' =>'required' ,
            'website' =>'required' ,
            'content' =>'required' ,
          
		   ],
		   [
               
                'phone.regex'=>'Định dạng điện thoại không hợp lệ',
                'name.required' => 'Bạn chưa nhập tên khách sạn!',
                'address.required' => 'Bạn chưa nhập địa chỉ!',
                'website.required' => 'Bạn chưa nhập website!',
                'email.required' => 'Bạn chưa nhập email!',
                'fax.required' => 'Bạn chưa nhập số fax!',
                'phone.required' => 'Bạn chưa nhập số điện thoại!',
                'content.required' => 'Bạn chưa nhập nội dung!',
				
         ]);

        $thongtin = new Information;
        $thongtin->name = $request->name;
        $thongtin->address = $request->address;
        $thongtin->phone = $request->phone;
        $thongtin->fax = $request->fax;
        $thongtin->email = $request->email;
        $thongtin->website = $request->website;
        $thongtin->content = $request->content;
        $thongtin->save();
        return redirect()->route('admin.themThongTin');
    }

  
}
