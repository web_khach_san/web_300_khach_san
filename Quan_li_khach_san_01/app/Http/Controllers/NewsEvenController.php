<?php

namespace App\Http\Controllers;

use App\News_Even;
use Illuminate\Http\Request;

class NewsEvenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News_Even  $news_Even
     * @return \Illuminate\Http\Response
     */
    public function show(News_Even $news_Even)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News_Even  $news_Even
     * @return \Illuminate\Http\Response
     */
    public function edit(News_Even $news_Even)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News_Even  $news_Even
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News_Even $news_Even)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News_Even  $news_Even
     * @return \Illuminate\Http\Response
     */
    public function destroy(News_Even $news_Even)
    {
        //
    }
}
