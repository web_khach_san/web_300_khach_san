<?php

namespace App\Http\Controllers;

use App\StyleImage;
use Illuminate\Http\Request;

class StyleImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StyleImage  $styleImage
     * @return \Illuminate\Http\Response
     */
    public function show(StyleImage $styleImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StyleImage  $styleImage
     * @return \Illuminate\Http\Response
     */
    public function edit(StyleImage $styleImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StyleImage  $styleImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StyleImage $styleImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StyleImage  $styleImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StyleImage $styleImage)
    {
        //
    }
}
