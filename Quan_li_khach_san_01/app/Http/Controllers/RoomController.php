<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;
use App\StyleRoom;
use Illuminate\Support\Facades\File;

class RoomController extends Controller
{
   
    public function danhSachPhong(Request $request)
    {
        $rooms = Room::orderBy('id','DESC')->paginate(3);
        return view('admin.room.list',compact('rooms'))->with('i', ($request->input('page', 1) - 1) * 5);     
            
    }

    public function SuaDanhSach($id)
    {
        $rooms = Room::findOrFail($id);
        $styleRoom = StyleRoom::all();
        return view('admin.room.edit',compact('rooms','styleRoom'));
    }
 
    public function updateDanhSachPhong(Request $request, $id)
    {
        $this->validate($request,
    		[
    			'Sophong'=>'required',
    			'Mota' => 'required|min:20',    			
    		],
    		[
    			'SoPhong.required'=>'Bạn chưa chọn số phòng!',
    			'Mota.required'=>'Bạn chưa nhập nội dung giới thiệu về dịch vụ!',
    			'Mota.min'=>'Nội dung quá ngắn, yêu cầu tối thiểu gồm 20 ký tự trở lên!',
    		]);
        $rooms = Room::findOrFail($id);
        $rooms ->Sophong = $request->Sophong;
        $rooms ->Mota = $request->Mota;

        if ($request->hasFile('Images')) {
            $image = $request->file('Images');
            $path = $image->store('imagesRoom', 'public');
            $rooms->Anh = $path;
        }

        $rooms->id_loaiPhong = $request->loaiPhong;
        $rooms->save();

    	return redirect('admin/room/danhsach')->with('message','Cập nhập thành công!');
    }

    public function XoaRoom($id){
		$rooms = Room::find($id);
		$image = $rooms->Anh;       
        if ($image) {
            Storage::delete('/public/' . $image);
        }
         $rooms->delete();
    	return redirect('admin/room/danhsach')->with('message','Xóa thành công!');

    }
    public function themPhong()
    {
        $rooms = Room::all();
        $styleRoom = StyleRoom::all();
        return view('admin.room.add',compact('rooms','styleRoom'));
    }
    public function XuLyThemPhong(Request $request)
	
    {   
		$this->validate($request,
    		[
    			'Sophong'=>'required',
    			'Mota' => 'required|min:20',    			
    		],
    		[
    			'SoPhong.required'=>'Bạn chưa nhập số phòng!',
    			'Mota.required'=>'Bạn chưa nhập nội dung giới thiệu về phòng!',
    			'Mota.min'=>'Nội dung quá ngắn, yêu cầu tối thiểu gồm 20 ký tự trở lên!',
    		]);
        $rooms = new Room();
        
        $rooms ->Sophong = $request->Sophong;
        $rooms ->Mota = $request->Mota;

        if ($request->hasFile('imageroom')) {
            $image = $request->file('imageroom');
            $path = $image->store('imagesRoom', 'public');
            $rooms->Anh = $path;
        }
        $rooms->id_loaiPhong = $request->loaiPhong;
        $rooms->save();

        $rooms->id_loaiPhong = $request->loaiPhong;
        $rooms->save();

    	return redirect('admin/room/danhsach')->with('message','Thêm thành công!');
    }

    public function trangthaiphong($id)
    {
        $rooms = Room::find($id);
        $rooms->Trangthai = !$rooms->Trangthai;
        $rooms->save();
        return redirect()->back();
    }
}
