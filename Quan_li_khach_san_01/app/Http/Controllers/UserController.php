<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

use Hash;
class UserController extends Controller
{
   public function themUser(){
	   return view('admin.author.create');
   }
   public function storeUser(Request $request){
      $this->validate($request,
      [
      'name'=>'required',
         'DOB' =>'required' ,
         'phone' =>'required|regex:/(01)[0-9]{9}/' ,
         'email' =>'required' ,
         'password' =>'required|min:6' ,
  
       
      ],
      [
           
             'phone.regex'=>'Định dạng điện thoại không hợp lệ',
             'name.required' => 'Bạn chưa nhập tên!',
             'DOB.required' => 'Bạn chưa nhập ngày sinh!',
             'password.required' => 'Bạn chưa nhập mật khẩu!',
             'password.min' => 'Mật khẩu phải có ít nhất 6 kí tự!',
             'email.required' => 'Bạn chưa nhập email!',       
             'phone.required' => 'Bạn chưa nhập số điện thoại!',
         
      ]);
	   $user = new User;
	   $user->name = $request->name;
	   $user->phone = $request->phone;
	   $user->DOB = $request->DOB;
	   
	   $user->email = $request->email;
	   $user->password = Hash::make($request->password);

	  
	   $user->save();
	   return redirect()->back()->with('thanhcong','Tạo tài khoản thành công');
   }
   public function login(){
		if(Auth::user())
		return redirect()->route('admin.danhsach');
        else
	   return view('admin.author.login');
   }
   public function postLogin(Request $request)
   {
	   $this->validate($request,
		   [
			   'email'=>'required|email',
			   'password' =>'required|min:6' 
		   ],
		   [
				'email.required' => 'Bạn chưa nhập Địa chỉ Email!',
				'email.email' => 'Email không đúng định dạng xxx@xxx.com!',
				'password.required' => 'Bạn chưa nhập Mật khẩu!',
				'password.min' => 'Mật Khẩu gồm tối thiểu 6 ký tự!'
			]);
			$remember = $request->has('remember') ? true : false;
	   if (Auth::attempt(['email'=>$request->email,'password'=>$request->password],$remember))
		{
		  return redirect()->route('admin.danhsach');
		}
	   return redirect()->back()->with('thatbai','tài khoản hoặc mật khẩu sai');
   }

    public function logoutUser(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
    public function listUser(){
       $users = User::all();
       return view('admin.author.list',compact('users'));
    }
    public function updateUser ($id){
      $user = User::find($id);
        $user->role = !$user->role;
        $user->save();
        return redirect()->back();
   }
   public function changepassword($id){ 
      $user = User::find($id);
      return view('admin.author.password',compact('user'));
   }

   public function postpassword(Request $request,$id){
      $this->validate($request,
		   [
			   'password_old'=>'required',
            'password' =>'required|min:6' ,
          
		   ],
		   [
           
            're_password.required' => 'Bạn chưa nhập lại mật khẩu!',
				'password_old.required' => 'Bạn chưa nhập mật khẩu cũ!',
				'password.required' => 'Bạn chưa nhập mật khẩu!',
				'password.min' => 'Mật Khẩu gồm tối thiểu 6 ký tự!'
         ]);
         $users = User::find($id);
         $user = $users->password;
         if(Hash::check($request->password_old,$user)){
            $users->password = Hash::make($request->password);
            $users->save();
            return redirect()->back()->with('message','Thay đổi mật khẩu thành công');
         }else{
            return redirect()->back()->with('danger','Mật khẩu cũ không đúng');
         }
   }

   public function xoaUser($id){
      $user = User::find($id);
      $user->delete();
      return redirect()->back()->with('message','Xóa người dùng thành công');
   }
}
