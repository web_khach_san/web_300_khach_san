<?php

namespace App\Http\Controllers;

use App\Information;
use App\StyleRoom;
use App\Service;
use App\Feedback;
use App\Tour;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ContentReservation;
use App\News_Even;

class HomeController extends Controller
{
    public function home()
    {
        $loaiphong = StyleRoom::paginate(3);
        $gioithieu = Information::all();
        $dichvu = Service::all();
        $events= News_Even::paginate(3);
        $tour = Tour::all();
        return view('template.body',compact('loaiphong','dichvu','events','gioithieu'));
    }
    public function Loaiphong(){
        $loaiphong = StyleRoom::all();
        return view('menu.loaiphong',compact('loaiphong'));
    }
    public function Dichvu(){
        $dichvu = Service::all();
        return view('menu.service',compact('dichvu'));
    }
    public function Lienhe(){
        
        return view('menu.lienhe');
    }
    public function Gioithieu(){
        
        return view('menu.gioithieu');
    }
    

   

  
   
    



    public function chitietDV($id)
    {
        $dichvu = Service::find($id);
        $loaiphong = StyleRoom::all();
        $gioithieu = Information::all();
        $tour = Tour::all();
        return view('trangchu.dichvu.chitietDv', compact('dichvu', 'loaiphong', 'gioithieu', 'tour'));
    }

    public function dienthongtin(Request $request)
    {
        $data = new Feedback;
        $data->subject = $request->subject;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->content = $request->content;
        $data->save();
        Session::flash('success', 'Cám ơn bạn đã liên hệ với chúng tôi');
        return redirect()->back();
    }

    public function danhSachdienthongtin(Request $request)
    {
        $lienhe = Feedback::orderBy('id', 'DESC')->paginate(5);
        return view('admin.contact.listlienhe', compact('lienhe'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function xoalienhe($id)
    {
        $lienhe = Feedback::find($id);
        $lienhe->delete();
        return redirect('feedback/lienhekhachhang')->with('message', 'Xóa thành công!');
    }

    public function Tours()
    {
        $loaiphong = StyleRoom::all();
        $gioithieu = Information::all();
        $dichvu = Service::all();
        $tour = Tour::all();
        return view('trangchu.tours.toursShow', compact('loaiphong', 'dichvu', 'gioithieu', 'tour'));
    }

    public function chitietTours($id)
    {
        $tour = Tour::find($id);
        $loaiphong = StyleRoom::all();
        $gioithieu = Information::all();
        $dichvu = Service::all();
        return view('trangchu.tours.chitietTour', compact('tour', 'loaiphong', 'dichvu', 'gioithieu'));
    }
    public function thongtindatphong(){
        $thongtindatphong = ContentReservation::all();
        $loaiphong = StyleRoom::all();
        $gioithieu = Information::all();
        $dichvu = Service::all();
        return view('trangchu.thongtindatphong',compact('thongtindatphong','loaiphong','gioithieu','dichvu'));
    }

 
}
