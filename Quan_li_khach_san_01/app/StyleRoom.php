<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StyleRoom extends Model
{
    protected $table = 'style_rooms';
    public function reservation() {
		return $this->belongsTo('App\Reservation');
	}
   
}
