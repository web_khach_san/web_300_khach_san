<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function styleRooms(){
        return $this->belongsTo(StyleRoom::class,'id_loaiPhong','id');
    }
}
