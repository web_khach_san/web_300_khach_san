@extends('index')
@section('title')
Chi Tiết Tours
@endsection
@section('banner')
<section class="contact-banner-area" id="gallery">
 <div class="container h-100">
  <div class="contact-banner">
   <div class="text-center">
    <h1>Gallery</h1>
    <nav aria-label="breadcrumb" class="banner-breadcrumb">
     <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Gallery</li>
     </ol>
    </nav>
   </div>
  </div>
 </div>
</section>
@endsection
@section('content')
<section class="section-margin section-margin--small">
 <div class="container">
  <div class="section-intro text-center pb-80px">
   <div class="section-intro__style">
    <img src="img/home/bed-icon.png" alt="">
   </div>
   <h2>Thông tin chi tiết</h2>
  </div>
  <div class="row">
   <div class="col-md-4">
    <img class="card-img" src="{{$tour->images}}" alt="" width="100%">
   </div>
   <div class="col-md-8">
    <h3>{{$tour->nameTour}}</h3>
    <P>{{$tour->content}}</P>
   </div>
  </div>
 </div>
</section>
<section class="section-margin">
 <div class="container">
  <div class="section-intro text-center pb-20px">
   <h2>Tours</h2>
  </div>
  <div class="owl-carousel owl-theme testi-carousel">
   @foreach($tourshow as $tours)
   <div class="testi-carousel__item">
    <div class="media">
     <div class="media-body">
      <img src="{{ ($tours->images) }}" width="200px" height="200px" alt="">
      <div class="testi-carousel__intro">
       <h3>{{ $tours->nameTour }}</h3>
       <a class="card-explore__link" href="#">Chi tiết <i class="ti-arrow-right"></i></a>
      </div>
     </div>
    </div>
   </div>
   @endforeach

  </div>
</section>
@endsection