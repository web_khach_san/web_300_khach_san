@extends('index')
@section('title')
TOUR & DỊCH VỤ
@endsection
@section('banner')
<section class="contact-banner-area" id="gallery">
 <div class="container h-100">
  <div class="contact-banner">
   <div class="text-center">
    <h1>TOUR & DỊCH VỤ</h1>
    <nav aria-label="breadcrumb" class="banner-breadcrumb">
   
    </nav>
   </div>
  </div>
 </div>
</section>
@endsection
@section('content')
<section class="section-margin">
        <div class="container">
         <div class="section-intro text-center pb-20px">
          <h2><span>TOUR</span></h2>
         </div>
         <div class="owl-carousel owl-theme testi-carousel">
          @foreach($tours as $tour)
          <div class="testi-carousel__item">
           <div class="media" style="height:300px;">
            <div class="media-body">
             <img src="{{ ($tour->images) }}" width="200px" height="200px" alt="">
             <p>{{ $tour->nameTour }}</p>
             <div class="testi-carousel__intro">
              {{-- <h3>{{ $tour->nameService }}</h3> --}}
              <a class="card-explore__link" href="#">Chi tiết <i class="ti-arrow-right"></i></a>
             </div>
            </div>
           </div>
          </div>
          @endforeach
         </div>
       </section>
       <section class="section-margin">
            <div class="container">
             <div class="section-intro text-center pb-20px">
              <h2><span>CÁC DỊCH VỤ CỦA KHÁCH SẠN</span></h2>
             </div>
             <div class="owl-carousel owl-theme testi-carousel">
              @foreach($dichvu as $dv)
              <div class="testi-carousel__item">
               <div class="media" style="height:300px">
                <div class="media-body">
                 <img src="{{asset('storage/'.$dv->images)}}" width="200px" height="200px" alt="">
                
                 <div class="testi-carousel__intro">
                  <h3>{{ $dv->nameService }}</h3>
                 </div>
                </div>
               </div>
              </div>
              @endforeach
          
             </div>
           </section>
@endsection
