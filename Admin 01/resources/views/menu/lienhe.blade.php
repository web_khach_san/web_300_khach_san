@extends('index')
@section('title')
Liên hệ
@endsection
@section('banner')
<section class="contact-banner-area" id="contact">
  <div class="container h-100">
    <div class="contact-banner">
      <div class="text-center">
        <h1>Contact Us</h1>
        <nav aria-label="breadcrumb" class="banner-breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>
@endsection
@section('content')
<section class="section-margin">
  <div class="container">
    <!-- google map start -->
    <div class="d-none d-sm-block mb-5 pb-4">
      <div id="map" style="height: 420px;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d239.13094802782985!2d107.5959892!3d16.4707181!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a122c154a049%3A0xeedbda04b598e644!2zNjUgVsO1IFRo4buLIFPDoXUsIFBow7ogSOG7mWksIFRow6BuaCBwaOG7kSBIdeG6vywgVGjhu6thIFRoacOqbiBIdeG6vw!5e0!3m2!1svi!2s!4v1562209961716!5m2!1svi!2s" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

      </div>

    </div>
    
  </div>
</section>
@endsection