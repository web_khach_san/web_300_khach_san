@extends('index')
@section('title')
Quy tắc
@endsection
@section('banner')
<section class="contact-banner-area" id="contact">
		<div class="container h-100">
			<div class="contact-banner">
				<div class="text-center">
					<h1>LOẠI PHÒNG</h1>
				</div>
			</div>
    </div>
	</section>
@endsection
@section('content')
<section>
        <h2 style="text-align: center; margin-bottom:50px;"><span> Quy tắc chung</span></h2>
 <div class="">
  <div class="">
   
   <div class="card-body">
    <table class="table-bordered">
     <thead>
      <tr>
       <th>Nhận phòng</th>
       <th>Trả phòng</th>
       <th>Hủy đặt phòng/ trả trước</th>
       <th>Trẻ em và giường</th>
       <th>Vật nuôi</th>
       <th>Chỉ thanh toán bằng tiền mặt</th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td>thời gian từ 14h tới 22h</td>
       <td>thời gian từ 10h tới 12h</td>
       <td>Các chính sách hủy và thanh toán trước có khác biệt dựa trên loại chỗ nghỉ. Vui
        lòng</td>
       <td>Chỗ nghỉ chào đón trẻ em
        <p>Không có nôi (cũi)</p>
        <p>Không có giường phụ còn trống</p>
       </td>
       <td>Vật nuôi không được phép</td>
       <td>Chỗ nghỉ này chỉ chấp nhận thanh toán bằng tiền mặt</td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
 </div>
</section>
@endsection