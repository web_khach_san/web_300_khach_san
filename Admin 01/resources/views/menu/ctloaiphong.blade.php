<div id="modelId{{ $lp->id }}" class="modal fade" tabindex="-1" style="margin-top:70px" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="card-header">
              <div>
                  <strong class="text-center">Chi tiết loại phòng</strong>
              </div>
            </div>
            <div class="card-body">
              <div>
                  <img class="card-img" src="{{ asset('storage/'.$lp->Anh) }}" height="300px">
              </div>
              <div>
                <strong>Loại phòng : </strong> <span>{{ $lp->TenLoaiPhong }}</span>
              </div>
              <div>
                <strong>Giá : </strong> <span>{{ $lp->Gia }}</span>
              </div>
              <div>
                <strong>Mô tả : </strong> <span>{!! $lp->Mota !!}</span>
              </div>
            </div>
          </div>
        </div>
      </div>