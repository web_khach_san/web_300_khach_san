@extends('index')
@section('title')
Tin tức $ Sự kiện
@endsection
@section('banner')
<section class="blog-banner-area" id="about">
 <div class="container h-100">
  <div class="blog-banner">
   <div class="text-center">
    {{-- <h1>Tin tức & Sự kiện</h1> --}}
    <nav aria-label="breadcrumb" class="banner-breadcrumb"></nav>
   </div>
  </div>
 </div>
</section>
@endsection
@section('content')
<section class="section-margin section-margin--small">
 <div class="container">
  <div class='row'>
   @foreach ($events as $event)
   <div class="col-md-4 ">
    <div class="card" style="width: 20rem;" id="styleevent">
     <img class="card-img-top" src="{{$event->Anh}}" alt="Card image cap">
     <div class="card-body">
      <h5 class="card-title">{{$event->title}}</h5>
      <p class="card-text">{{ str_limit($event->content, $limit = 50, $end = '...') }}</p>
      <a href="#" class="btn btn-primary" data-target="#my-modal{{$event->id}}" data-toggle="modal">Xem chi tiết</a>
     </div>
    </div>
   </div>
   <div id="my-modal{{$event->id}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
     <div class="modal-content">
      <div class="modal-body">
       <p>{{$event->title}}</p>
       <p>{{$event->content}}</p>
      </div>
     </div>
    </div>
   </div>
   @endforeach
  </div>
 </div>
</section>
@endsection