@extends('admin.layout.layout')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                                {!! $statistiday->html() !!}
                        </div>
                        <div class="col-lg-6">
                                {!! $statistimonth->html() !!}
                            </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
    {!! Charts::scripts() !!}
    {!! $statistiday->script() !!}
    {!! $statistimonth->script() !!}
    @endsection