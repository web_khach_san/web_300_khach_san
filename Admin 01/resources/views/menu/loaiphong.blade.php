@extends('index')
@section('title')
Loại phòng
@endsection
@section('banner')
<section class="contact-banner-area" id="contact">
		<div class="container h-100">
			<div class="contact-banner">
				<div class="text-center">
					<h1>LOẠI PHÒNG</h1>
				</div>
			</div>
    </div>
  </section>
  
@endsection
@section('content')
<section class="section-margin section-margin--small">
    <div class="container">
      
      <div class="row pb-4">
        @foreach($loaiphong as $lp)
        <div class="col-md-6 col-xl-4 mb-5">
          <div class="card card-explore" style="height:450px;">
            <div class="card-explore__img">
            <a href="#" data-toggle="modal" data-target="#modelId{{ $lp->id }}"><img class="card-img" src="{{ asset('storage/'.$lp->Anh) }}" height="250px;"></a>  
            </div>
            <div class="card-body">
              <h3 class="card-explore__price">{{ $lp->Gia }} <sub>/ Per Night</sub></h3>
              <h4 class="card-explore__title" ><a href="#" data-toggle="modal" data-target="#modelId{{ $lp->id }}">{{ $lp->TenLoaiPhong }}</a></h4>
              {{-- <p>{{ $lp->Mota }}</p> --}}
              <a class="card-explore__link" href="#" data-toggle="modal" data-target="#modelId{{ $lp->id }}">Chi tiết <i class="ti-arrow-right"></i></a>
            </div>
          </div>
        </div>
        @include('menu.ctloaiphong')
        @endforeach
      </div>
    </div>
  </section>
  @endsection