@extends('index')
@section('title')
Đặt phòng
@endsection
@section('content')
<div class="container n-ding">
    <div class="row">
        <div class=" Rocks">
            <div class="send-email  wow fadeInDown animated animated"
                style="visibility: visible; animation-name: fadeInDown;">
                <div id="main-contact-form" class="contact-form">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                        <strong>{{$err}}</strong><br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('error'))
                    <div class="alert alert-danger">
                        <strong>{{session('error')}}</strong>
                    </div>
                    @endif
                    @if(session('message'))
                    <div class="alert alert-success" id="hide">
                        <strong>{{session('message')}}</strong>
                    </div>
                    @endif
                    @if(session('danger'))
                    <div class="alert alert-danger" id="hide">
                        <strong>{{session('danger')}}</strong>
                    </div>
                    @endif
                    <script>
                        setTimeout(function(){
                            $('#hide').fadeOut('fast');
                         },5000);
                    </script>
                    <form action="{{ route('trangchu.postDatPhong') }}" id="book-tour" method="post"
                        novalidate="novalidate">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-info ">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Step 1: Đặt Phòng</h4>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="CheckIn">Ngày đến<span class="required"
                                                        aria-required="true">*</span></label>
                                                <input class="form-control hasDatepicker valid" type="date"
                                                    name="ngayden" id="from" aria-invalid="false">

                                            </div>
                                            @if($errors->has('ngayden'))
                                            <div class="alert alert-danger">
                                                <strong>{{ $errors->first('ngayden')}}</strong>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="CheckOut">Ngày đi<span class="required"
                                                        aria-required="true">*</span></label>
                                                <input class="form-control hasDatepicker valid" type="date"
                                                    name="ngaydi" id="to" value="date" aria-invalid="false">
                                            </div>
                                            @if($errors->has('ngaydi'))
                                            <div class="alert alert-danger">
                                                <strong>{{ $errors->first('ngaydi')}}</strong>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="CheckOut">Người lớn<span class="required"
                                                        aria-required="true">*</span></label>
                                                <input class="form-control hasDatepicker valid" type="text"
                                                    name="nguoilon" id="CheckOut" aria-invalid="false">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="CheckOut">Trẻ em<span class="required"
                                                        aria-required="true">*</span></label>
                                                <input class="form-control hasDatepicker valid" type="text" name="treem"
                                                    id="CheckOut" aria-invalid="false">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Loại phòng </label><span class="required"
                                                    aria-required="true">*</span><br>

                                                <select class="form-control" name="loaiphong">
                                                    @foreach($loaiphong as $cate)
                                                    <option value="{{ $cate->TenLoaiPhong }}">{{ $cate->TenLoaiPhong }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info ">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Step 2: Thông Tin Khách Hàng</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Họ tên</label> <span class="required"
                                                        aria-required="true">*</span><br>
                                                    <input type="text" name="hoten" size="40" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Số điện thoại</label><br>
                                                    <input type="text" name="dienthoai" size="40" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Email </label><span class="required"
                                                        aria-required="true">*</span><br>
                                                    <input type="email" name="email" value="" size="40"
                                                        class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 agree">
                                        <input type="checkbox" name="agree" id="agree"> <span>Tôi đã đọc và đồng ý với
                                            các điều khoản và điều kiện của khách sạn Fibo Holiday Hotel</span>
                                    </div>

                                    <div class="clear-fix"></div>
                                    <br>
                                    <div class="col-md-12" style="margin-top: 20px;">
                                        <input type="submit" value="Gửi" class="btn btn-primary">
                                    </div>

                                </div>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>
</div>
<h4>Quý khách vui lòng điền các thông tin sau</h4>
      <div class="container">
   <form>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationServer013">Ngày đến</label>
      <input type="date" class="form-control is-valid" id="validationServer013" 
        value="Mark" required>
     
    </div>
    <div class="col-md-4 mb-3">
      <label >Ngày đi</label>
      <input type="date" class="form-control is-valid" 
        value="Ngày đi" required>
     
    </div>
    <div class="col-md-4 mb-3">
      <label >Email</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" >@</span>
        </div>
        <input type="text" class="form-control is-invalid" id="validationServerUsername33" placeholder="Nhập email"
          aria-describedby="inputGroupPrepend33" required>
        <div class="invalid-feedback">
          Please choose a username.
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="validationServer033">City</label>
      <input type="text" class="form-control is-invalid" id="validationServer033" placeholder="City"
        required>
      <div class="invalid-feedback">
        Please provide a valid city.
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationServer043">State</label>
      <input type="text" class="form-control is-invalid" id="validationServer043" placeholder="State"
        required>
      <div class="invalid-feedback">
        Please provide a valid state.
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationServer053">Zip</label>
      <input type="text" class="form-control is-invalid" id="validationServer053" placeholder="Zip"
        required>
      <div class="invalid-feedback">
        Please provide a valid zip.
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" class="custom-control-input is-invalid" id="invalidCheck33" required>
      <label class="custom-control-label" for="invalidCheck33">Agree to terms and conditions</label>
      <div class="invalid-feedback">
        You must agree before submitting.
      </div>
    </div>
    <div class="invalid-feedback">
      You must agree before submitting.
    </div>
  </div>
  <button class="btn btn-primary" type="submit">Submit form</button>
</form></div>
@endsection
