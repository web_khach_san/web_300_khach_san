@extends('index')
@section('title')
SHAPPHIRE HOTEL
@endsection
@section('banner')
<section class="home-banner-area" id="home">
  
      
                 <img src="https://i.imgur.com/Rn1VG0V.jpg" width="100%" height="100%" alt="">
          
   
</section>

@endsection
@section('content')
@include('template.body.introduction')
@include('template.body.room')
@include('template.body.service')

@endsection