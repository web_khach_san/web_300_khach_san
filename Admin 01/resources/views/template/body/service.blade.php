<section class="section-margin">
    <div class="container">
     <div class="section-intro text-center pb-20px">
      <h2> <span>TOUR</span> </h2>
     </div>
     <div class="owl-carousel owl-theme testi-carousel">
      @foreach($tours as $tour)
      <div class="testi-carousel__item">
       <div class="media" style="height:300px">
        <div class="media-body">
         <img src="{{ ($tour->images) }}" width="200px" height="200px" alt="">
         <p>{{ $tour->nameTour }}</p>
         <div class="testi-carousel__intro">
          {{-- <h3>{{ $tour->nameService }}</h3> --}}
          <a class="card-explore__link" href="#">Chi tiết <i class="ti-arrow-right"></i></a>
         </div>
        </div>
       </div>
      </div>
      @endforeach
  
     </div>
   </section>