<section class="welcome">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 mb-4 mb-lg-0" style="width:100%">
            <div class="col-lg-12" style="width:100%">
                <div class="carousel" id="slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#slide" data-slide-to="0" class="active"></li>
                    <li data-target="#slide" data-slide-to="1"></li>
                    <li data-target="#slide" data-slide-to="2"></li>
                    <li data-target="#slide" data-slide-to="3"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="./plugin/img/IMGgioithieu/DSC05361.jpg" class="img-fluid">
                    </div>
                    <div class="carousel-item">
                      <img src="./plugin/img/IMGgioithieu/DSC05337.jpg" class="img-fluid">
                    </div>
                    <div class="carousel-item">
                      <img src="./plugin/img/IMGgioithieu/DSC05293.jpg" class="img-fluid">
                    </div>
                     <div class="carousel-item">
                        <img src="./plugin/img/IMGgioithieu/DSC05330.jpg" class="img-fluid">
                    </div>
                  </div>
                  <a href="#slide" class="carousel-control-prev" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                   <a href="#slide" class="carousel-control-next" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>
                </div>
            </div>
          <div class="row no-gutters welcome-images col-md-12 mt-1" >

            <div class="col-md-5 d-lg-block d-none" style="width:100%">
              <div class="card">
                <img class="" src="./plugin/img/IMGgioithieu/DSC05696.jpg"  height="290px" alt="Card image cap">
              </div>
            </div>
            <div class="col-md-7 d-lg-block d-none " style="width:100%">
              <div class="card">
                <img class="" src="./plugin/img/IMGgioithieu/DSC05707.jpg"  height="290px" alt="Card image cap">
              </div>
            </div>

          </div>
        </div>

        <div class="col-lg-6">
          <div class="">
            <h3 class="mb-4">Welcome To &nbsp{{ $gioithieu->name }}</h3>
            <p style="text-align: justify; font-size:20px">{{$gioithieu->content}}</p>
          </div>
        </div>
      </div>
     </div>
    </div>
   </div>
  
  </div>
 </div>
</section>