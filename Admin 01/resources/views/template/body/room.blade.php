<section class="section-margin">
  <div class="container">
    <div class="section-intro text-center pb-80px">
      <h2> <span>CÁC LOẠI PHÒNG</span> </h2>
    </div>
    <div class="row ">
      @foreach($loaiphong as $lp)
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0 ">
        <div class="card card-explore" style="height:450px;">
          <div class="card-explore__img">
            <a  href="#" data-toggle="modal" data-target="#modelId{{ $lp->id }}"><img class="card-img" src="{{ asset('storage/'.$lp->Anh) }}" alt=""></a> 
          </div>
          <div class="card-body">
            <h3 class="card-explore__price">{{ $lp->Gia }} <sub>/ Per Night</sub></h3>
            <h4 class="card-explore__title"><a href="#" data-toggle="modal" data-target="#modelId{{ $lp->id }}">{{ $lp->TenLoaiPhong }}</a></h4>

            <a class="card-explore__link" href="#" data-toggle="modal" data-target="#modelId{{ $lp->id }}">Chi tiết <i
                class="ti-arrow-right"></i></a>
          </div>
        </div>
      </div>
      @include('menu.ctloaiphong')
      @endforeach
    </div>

  </div>
  <div style="text-align:center; padding-top:30px">
   <a href="{{ route('Loaiphong') }}"> <button class="btn btn-info"> xem thêm</button></a> 
  </div>
  <!-- Button trigger modal -->


  <script>
    $('#exampleModal').on('show.bs.modal', event => {
        var button = $(event.relatedTarget);
        var modal = $(this);
        // Use above variables to manipulate the DOM
        
      });
  </script>
</section>