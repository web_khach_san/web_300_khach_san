<div class="main_menu">
  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <!-- <a class="navbar-brand logo_h" href="index.html"><img src="plugin/img/logo.png" alt=""></a> -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
        <ul class="nav navbar-nav menu_nav">
          <li class="nav-item active"><a class="nav-link" href="{{ route('Trangchu') }}">TRANG CHỦ</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('Loaiphong') }}">LOẠI PHÒNG</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('Dichvu') }}">TOUR & DỊCH VỤ</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('tinTucSuKien') }}">TIN TỨC & SỰ KIỆN</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('Lienhe') }}">LIÊN HỆ</a></li>
          
          <li class="nav-item active"><a style='padding-top:15px'class="nav-link " href="#formDangKy"><button class='btn btn-warning'>BOOK NOW</button> </a></li>
        </ul>
      </div>
      

      <ul class="social-icons ml-auto">
        {{--  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
          <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
          <li><a href="#"><i class="fab fa-twitter"></i></a></li>
          <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
          <li><a href="#"><i class="fas fa-rss"></i></a></li>  --}}

        <li>
        <iframe
          src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fsapphirehotelhue%2F&width=80&layout=button_count&action=like&size=small&show_faces=true&share=false&height=21&appId"
          width="80" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
          allowTransparency="true" allow="encrypted-media" class="mt-3"></iframe>
        <iframe
          src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.facebook.com%2Fsapphirehotelhue%2F&layout=button_count&size=small&width=90&height=20&appId"
          width="90" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
          allowTransparency="true" allow="encrypted-media" class="mt-3"></iframe>
        </li>
      </ul>
    </div>
  </nav>

  {{--  <div class="search_input" id="search_input_box">
      <div class="container">
        <form class="d-flex justify-content-between">
          <input type="text" class="form-control" id="search_input" placeholder="Search Here">
          <button type="submit" class="btn"></button>
          <span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
        </form>
      </div>
    </div>  --}}
</div>