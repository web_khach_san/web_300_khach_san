<div class="header-top ">
  <div class="container  " id="quancao">
    <div class="row">


      <div class=" col-lg-3 d-flex align-items-center">
        <div id="logo">
          <a href="index.html"><img src="plugin/img/logo/logo.png" width='100px' height="100px" alt="" title="" /></a>
        </div>
      </div>
      <div class=" col-lg-9 flexslider ">
        <ul class="slides">
            @foreach($allAds as $ad)
            <li>
                <div class="col-lg-12 p-0">
                <a href=""><img src="{{asset('storage/'.$ad->image)}}" width="100%" height="93px;"></a>
            </div>

            <div class="col-lg-12 p-0">
                <span class="float-right" style="margin-left:30px;">
                    <strong>
                      <i class="ti-email"></i>
                      <span>{{ $ad->email }}</span>
                    </strong>
                  </span>
                <span class="float-right" >
                    <strong>
                        <i class="fa fa-phone-volume"></i>
                        <span >{{ $ad->phone }}</span>
                    </strong>
              </span>
            </div>
          </li>
          @endforeach
      </div>
  </div>


</div>
<style>
  .flexslider {


    margin: 0 auto;
    box-shadow: none;
  }

  .flexslider li {
    position: relative;
  }

</style>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.0/jquery.flexslider-min.js"></script>
<script>
  $('.flexslider').flexslider({

    controlNav: false
  });
</script>
