<div class="container">
  <div class="row" style="height:300px;">
    <div class="col-xl-4 col-sm-6 mb-4 mb-xl-0 single-footer-widget" >

      <h4 class="text-center" style="margin-top:30px;">LIÊN HỆ</h4>

      <ul>
        {{-- <li><a href="#">Managed Website</a></li> --}}
        <li><p>Địa chỉ:{{$gioithieu->address}} </p></li>
        <li><p>Số điện thoại: &nbsp;{{$gioithieu->phone}} </p></li>
        <li><p>Email: &nbsp;{{$gioithieu->email}}</p></li>
      </ul>


    </div>

    <div class="col-xl-4 col-sm-6 mb-4 mb-xl-0 single-footer-widget" >
      <iframe
        src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsapphirehotelhue%2F&tabs=timeline&width=340&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
        width="340px" height="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
        allowTransparency="true" allow="encrypted-media"></iframe>
    </div>


    <div class="col-xl-4 col-md-8 mb-4 mb-xl-0 single-footer-widget">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d239.13094802782985!2d107.5959892!3d16.4707181!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a122c154a049%3A0xeedbda04b598e644!2zNjUgVsO1IFRo4buLIFPDoXUsIFBow7ogSOG7mWksIFRow6BuaCBwaOG7kSBIdeG6vywgVGjhu6thIFRoacOqbiBIdeG6vw!5e0!3m2!1svi!2s!4v1562209961716!5m2!1svi!2s" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    
  </div>
  {{--  <div class="footer-bottom row align-items-center text-center text-lg-left">
    <p class="footer-text m-0 col-lg-8 col-md-12">

    </p>
    <div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
      <a href="#"><i class="fab fa-facebook-f"></i></a>
      <a href="#"><i class="fab fa-twitter"></i></a>
      <a href="#"><i class="fab fa-dribbble"></i></a>
      <a href="#"><i class="fab fa-behance"></i></a>
    </div>
  </div>  --}}
 
</div>