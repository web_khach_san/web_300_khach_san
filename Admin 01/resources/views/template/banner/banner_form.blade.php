<form class="form-search form-search-position" action="{{ route('trangchu.postDatPhong') }}" id="book-tour"
  method="post" novalidate="novalidate">
  @csrf
  {{--  @if(count($errors) > 0)
 <div class="alert alert-danger">
    @foreach($errors->all() as $err)
    <strong>{{$err}}</strong><br>
  @endforeach
  </div>
  @endif

  @if(session('error'))
  <div class="alert alert-danger">
    <strong>{{session('error')}}</strong>
  </div>
  @endif --}}
  @if(session('message'))
  <div class="alert alert-success" id="hide">
    <strong>{{session('message')}}</strong>
  </div>
  @endif
  @if(session('danger'))
  <div class="alert alert-danger" id="hide">
    <strong>{{session('danger')}}</strong>
  </div>
  @endif
  <script>
    setTimeout(function(){
                $('#hide').fadeOut('fast');
             },5000);
  </script>
  <div class="container" id="formDangKy">
    <div class="row">
      <div class="col-lg-4 gutters-19">
        <div class="form-group">
          @if($errors->has('hoten'))
          <div>
            <span style="color:red">{{ $errors->first('hoten')}}</span>
          </div>
          @endif
          <input class="form-control" type="text" id="hoten" name="hoten" placeholder="Họ và tên">
        </div>
        @if($errors->has('hoten'))
        <div class="">
          <strong>{{ $errors->first('hoten')}}</strong>
        </div>
        @endif
      </div>
      <div class="col-lg-8 gutters-19">
        <div class="row">
          <div class="col-sm">
            <div class="form-group ">
              <div class="form-select-custom ">
                @if($errors->has('ngayden'))
                <div>
                  <span style="color:red">{{ $errors->first('ngayden')}}</span>
                </div>
                @endif
                <input class="form-control " type="date" name="ngayden" placeholder="Ngày đến">
              </div>
            </div>
          </div>
          <div class="col-sm gutters-19">
            <div class="form-group">
              <div class="form-select-custom">
                @if($errors->has('ngaydi'))
                <div>
                  <span style="color:red">{{ $errors->first('ngaydi')}}</span>
                </div>
                @endif
                <input class="form-control" type="date" name="ngaydi" placeholder="Ngày đi">
              </div>
            </div>
          </div>
          <div class="col-sm gutters-19">
            <div class="form-group">
              <div class="form-select-custom">
                @if($errors->has('soluong'))
                <div>
                  <span style="color:red">{{ $errors->first('soluong')}}</span>
                </div>
                @endif
                <input class="form-control" type="text" name="soluong" placeholder="Số lượng">
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12 gutters-19">
    <div class="row">
      <div class="col-sm gutters-19">
        <div class="form-group">
          <div class="form-select-custom">
            @if($errors->has('loaiphong'))
            <div>
              <span style="color:red">{{ $errors->first('loaiphong')}}</span>
            </div>
            @endif
                
            
            <select name="loaiphong" id="">
              <option value="" disabled selected>Vui lòng loại phòng</option>
              @foreach ($loaiphong as $lp)
              <option value="{{ $lp->TenLoaiPhong }}">{{ $lp->TenLoaiPhong }}</option>
              @endforeach
            </select>

          </div>
        </div>
      </div>
      <div class="col-sm gutters-19">
        <div class="form-group">
          <div class="form-select-custom">
            @if($errors->has('dienthoai'))
            <div>
              <span style="color:red">{{ $errors->first('dienthoai')}}</span>
            </div>
            @endif
            <input class="form-control" type="text" name="dienthoai" placeholder="Điện thoại">
          </div>
        </div>
      </div>
      <div class="col-sm pr-1 gutters-19">
        <div class="form-group">
          <div class="form-select-custom">
            @if($errors->has('email'))
            <div>
              <span style="color:red">{{ $errors->first('email')}}</span>
            </div>
            @endif
            <input class="form-control" type="text" name="email" placeholder="Email">
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  <div class="row">

    <div class="col-lg-8 gutters-19 ml-4 mr-4 mt-3">
      <div class="form-group">
        <input type="checkbox" id="checkbox"><span> Tôi đã đọc và đồng ý với các <a href="{{route('noiquy')}}">Điều khoản và quy tắc chung</a> của khách san </span>
      </div>
    </div>
    <div class="col-lg-3 gutters-19">
      <div class="form-group">
        <button class="button button-form" type="submit">Đặt phòng</button>
      </div>
    </div>
  </div>
  </div>

</form>
<script>
    $(document).ready(function() {
      $(':button[type="submit"]').prop('disabled', true);
      $('input[type="checkbox"]').click(function() {
         if($(this).val() != '') {
            $(':input[type="submit"]').prop('disabled', false);
         }
      });
  });
  </script>