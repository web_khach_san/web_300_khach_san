@extends('admin.layout.layout')
@section('content')

<div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Danh sách đặt phòng</h4>
              <p >Có:  {{ count($datphong)}} khách đặt phòng</p>
              </div>

              @if(session('message'))
              <div class="alert alert-success">
                  <strong>{{session('message')}}</strong>
              </div>
          @endif
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table" style="width:1165px;">
                    <thead class=" text-primary">
                            <th class="text-center">Tên</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Điện thoại</th>
                            <th class="text-center">Loại phòng</th>
                            <th class="text-center">Số lượng</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Ngày đến</th>
                            <th class="text-center">Ngày đi</th>
                            <th class="text-center">Xóa</th>
                    </thead>
                    <tbody class="text-center">

                            @foreach($datphong as $dp)
                            <tr>
                                <td style="">{{ $dp->hoten }}</td>
                                <td>
                                    {{ $dp->email }}
                                </td>
                                <td>
                                    {{ $dp->dienthoai}}
                                </td>
                                <td>
                                    {{ $dp->loaiphong}}

                                </td>
                                <td>
                                {{ $dp->soluong}}
                                </td>
                                <td>
                                <form method='POST' action="{{ route('admin.capnhapDatphong', $dp->id)}}" }}>
                                    @csrf
                                      @if ($dp->trangthai == false)
                                           <input type='submit' value="Chưa liên hệ">
                                      @else
                                          <input type='submit' value="Đã liên hệ"></form>
                                      @endif 
                                </form>
                                </td>
                                <td>
                                {{ $dp->ngayden}}
                                </td>
                                <td>
                                {{ $dp->ngaydi}}
                                </td>
                                <td>
                                <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{ route('admin.xoaDatPhong',$dp->id) }}"><i class="fa fa-trash"></i></a>

                                </td>
                            </tr>
                            @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>
{!! $datphong->render() !!}

@endsection
