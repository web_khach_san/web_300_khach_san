@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

    <div class="col-lg-12">
        <h3>
            Thêm phòng <strong></strong>
        </h3>
    </div>


    @if(session('message'))
    <div class="alert alert-success">
        <strong>{{session('message')}}</strong>
    </div>
    @endif
    <form action="{{route('admin.themPhong')}}" method="Post" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="form-group">
            <label>Số phòng</label>
            <input type="text" class="form-control" name="Sophong" placeholder="Nhập số phòng">
            @if($errors->has('Sophong'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('Sophong')}}</strong>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Giới thiệu về phòng</label>
            <textarea type="text" class="form-control ckeditor" name="Mota" row="10"
                placeholder="Giới thiệu về phòng"></textarea>
            @if($errors->has('Mota'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('Mota')}}</strong>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Ảnh</label>
            <input type="file" class="form-control" name="imageroom">
            <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" /> 
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Loại phòng</label>
            <select class="form-control" name="loaiPhong">
                @foreach($styleRoom as $style)
                <option value="{{ $style->id }}">{{ $style->TenLoaiPhong }}</option>
                @endforeach
                </option>
            </select>
            @if($errors->has('loaiPhong'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('loaiPhong')}}</strong>
            </div>
            @endif
        </div>
        <button type="submit" class="btn btn-primary">Thêm phòng</button>
    </form>
</div>

@endsection