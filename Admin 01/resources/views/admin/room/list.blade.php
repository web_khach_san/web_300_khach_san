@extends('admin.layout.layout')
@section('content')


<div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Danh sách phòng</h4>
              <p>Tìm thấy: {{count($rooms)}} đặt phòng</p>
              <a href="{{route('admin.themPhong')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Thêm phòng
              </a>
              </div>

              @if(session('message'))
              <div class="alert alert-success">
                  <strong>{{session('message')}}</strong>
              </div>
          @endif
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table" style="width:1165px;">
                    <thead class=" text-primary text-center">
                            {{--  <th>Stt</th>  --}}
                            <th>Số phòng</th>
                            <th>
                                Loại Phòng
                            </th>
                            {{--  <th>Ảnh</th>  --}}
                            <th>Trạng thái</th>
                            <th colspan="2">Hành Động</th>
                    </thead>
                    <tbody class="text-center">

                            @foreach($rooms as $room)
                            <tr>
                                {{--  <td>{{ $room->id }}</td>  --}}
                                <td>
                                    {{ $room->Sophong }}
                                </td>

                                <td>
                                        {{$room->styleRooms->TenLoaiPhong}}

                                </td>
                                {{--  <td>
                                        @if($room->Anh)
                                        <img src="upload/imagesRoom/{{ $room->Anh }}" alt="" style="width: 300px; height: 200px">
                                        @else
                                            {{'Chưa có ảnh'}}
                                        @endif
                                    </td>  --}}
                                    <td>
                                      <form method='POST' action="{{ route('admin.capnhap', $room->id)}}" }}>
                                            @csrf
                                      @if ($room->Trangthai == false)
                                           <input type='submit' value="Phòng trống">
                                      @else
                                          <input type='submit' value="Phòng có khách"></form>
                                      @endif 
                                      </form>
                                    </td>
                                    <td>

                                            <a href="{{route('admin.suaDanhSach',$room->id)}}" class="btn btn-info">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </a>
                                    </td>
                                    <td>

                                          <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{route('admin.xoa',$room->id)}}"><i class="fa fa-trash"></i></a>
                                        </td>
                            </tr>
                            @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>

{!! $rooms->render() !!}

@endsection
