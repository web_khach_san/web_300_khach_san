@extends('admin.layout.layout')
@section('content')

<div class="content">
 <div class="row">
  <div class="col-md-12">
   <div class="card">
    <div class="card-header">
     <h4 class="card-title"> Danh sách Tours</h4>
     <a href="{{route('admin.themTour')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"
       aria-hidden="true"></i> Thêm Tour
     </a>
    </div>
    @if(session('message'))
    <div class="alert alert-success">
     <strong>{{session('message')}}</strong>
    </div>
    @endif
    <div class="card-body">
     <div class="table-responsive">
      <table class="table" style="width:1165px;">
       <thead class=" text-primary text-center">

        <th>Tên Tour</th>
        <th>Giới thiệu về Tour</th>
        <th>Ảnh</th>
        <th colspan="2">Hành Động</th>
       </thead>
       <tbody>
        @foreach($tour as $tours)
        <tr>

         <td>
          {{ $tours->nameTour }}
         </td>
         <td style="width:450px; height:100px;overflow-y: scroll;">
          {{ $tours->content}}
         </td>
         <td>
          @if($tours->images)
          <img src="{{ asset('storage/'.$tours->images)}}" alt="" style="width: 200px; height: 150px">
          @else
          {{'Chưa có ảnh'}}
          @endif
         </td>

         <td>
          <a href="{{route('admin.suaTour',$tours->id)}}" class="btn btn-info ">
           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
          </a>
         </td>
         <td>
          <a class="btn btn-danger" onclick="return confirm('Are you sure?')"
           href="{{route('admin.xoaTour',$tours->id)}}"><i class="fa fa-trash"></i></a>
         </td>
        </tr>
        @endforeach
       </tbody>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
@endsection