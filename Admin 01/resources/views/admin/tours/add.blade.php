@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

 <div class="col-lg-12">
  <h3>
   Thêm loại Tours <strong></strong>
  </h3>
 </div>

 @if(session('message'))
 <div class="alert alert-success">
  <strong>{{session('message')}}</strong>
 </div>
 @endif
 <form action="{{route('admin.themTour')}}" method="Post" enctype="multipart/form-data">

  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <div class="form-group">
   <label>Tên dịch vụ</label>
   <input type="text" class="form-control" name="nameTour" placeholder="Nhập tên Tour">
   @if($errors->has('nameTour'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('nameTour')}}</strong>
   </div>
   @endif
  </div>
  <div class="form-group">
   <label>Giới thiệu về Tour</label>
   <textarea type="text" class="form-control" name="content" row="10" placeholder="Giới thiệu về Tour"></textarea>
   @if($errors->has('content'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('content')}}</strong>
   </div>
   @endif
  </div>
  <div class="form-group">
   <label>Ảnh</label>
   <input type="file" class="form-control" name="imageTour">
   <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />


  </div>

  <button type="submit" class="btn btn-primary">Thêm Tour</button>
 </form>
</div>

@endsection