@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

 <div class="col-lg-12">
  <h3>
   Sửa thông tin Tour <strong>{{$tour->nameTour}}</strong>
  </h3>

 </div>
 <div>
 </div>
 @if(count($errors) > 0)
 <div class="alert alert-danger">
  @foreach($errors->all() as $err)
  <strong>{{$err}}</strong><br>
  @endforeach
 </div>
 @endif

 @if(session('error'))
 <div class="alert alert-danger">
  <strong>{{session('error')}}</strong>
 </div>
 @endif

 @if(session('message'))
 <div class="alert alert-success">
  <strong>{{session('message')}}</strong>
 </div>
 @endif
</div>
<form action="{{route('admin.suaTour',$tour->id)}}" method="Post" enctype="multipart/form-data">

 <input type="hidden" name="_token" value="{{ csrf_token() }}" />
 <div class="form-group">
  <label>Tên Tour</label>
  <input type="text" class="form-control" name="nameTour" value="{{$tour->nameTour}}">
 </div>
 <div class="form-group">
  <label>Giới thiệu về Tour</label>
  <textarea type="text" class="form-control" name="content" rows="100">{{$tour->content}}</textarea>
 </div>
 <div class="form-group">
  <label>Ảnh</label>
  <p>
   <img width="400px" src="{{asset('storage/'.$tour->images)}}">
   <input type="file" class="form-control" name="Images">
   <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
  </p>
 </div>
 <button type="submit" class="btn btn-primary">Cập nhập</button>
</form>
</div>

@endsection