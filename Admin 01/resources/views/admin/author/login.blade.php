<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <base href="{{ asset('') }}">
    </base>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../assets/css/login.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>

<body>
    <section>
        <div class="login">
            <div class="form-login">
                <form id="login-form" action="{{ route('admin.postLogin') }}" method="POST" role="form">
                    @csrf
                    <input type="text" placeholder="Tên Đăng Nhập" id="userName" name="email" class="textInput">
                    @if($errors->has('email'))
                    <div class="alert alert-danger">
                        <strong>{{ $errors->first('email')}}</strong>
                    </div>
                    @endif
                    <input type="password" placeholder="Mật Khẩu" id="passWord" name="password" class="textInput">
                    @if($errors->has('password'))
                    <div class="alert alert-danger">
                        <strong>{{ $errors->first('password')}}</strong>
                    </div>
                    @endif
                    <input type="checkbox" name="remember"><span>Ghi nhớ tài khoản</span>
                    <button type="submit">Login</button>
                    <p class="message">Not Registered? <a href="#"> Register </a></p>
                    @if(Session::has('thatbai'))
                    <div class="alert alert-danger">
                        {{Session::get('thatbai')}}
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
    </script>
</body>

</html>