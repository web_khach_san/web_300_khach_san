@extends('admin.layout.layout')
@section('content')
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Danh sách người dùng</h4>
          <p>Tìm thấy: {{count($users)}} người dùng</p>
          <a href="{{route('admin.themUser')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"
              aria-hidden="true"></i> Thêm người dùng
          </a>
        </div>
        @if(session('message'))
        <div class="alert alert-success">
          <strong>{{session('message')}}</strong>
        </div>
        @endif
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" style="width:1165px;">
              <thead class=" text-primary text-center">
                <th>Tên người dùng</th>
                <th>Email</th>
                <th>Ngày sinh</th>
                <th> Điện thoại</th>
                <th>Quyền</th>
                <th>Xóa</th>
              </thead>
              <tbody class="text-center" >
                @foreach($users as $user)
                <tr>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->DOB }}</td>
                  <td>{{$user->phone}}</td>
                  <td>
                    @if(Auth::user()->role == true && Auth::user()->id != $user->id)
                    <form method='POST' action="{{ route('admin.updateUser', $user->id)}}" }}>
                      @csrf 
                      @if ($user->role == false)
                      <input type='submit' value="admin"> 
                      @else
                      <input type='submit' value="user"> 
                      @endif
                    </form>
                    @else 
                    @endif
                  </td>
                  <td>
                    @if(Auth::user()->id == $user->id) 
                      @else 
                        @if(Auth::user()->role == true)
                    <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{ route('admin.xoaUser',$user->id) }}"><i
                        class="fa fa-trash"></i></a> 
                        @else 
                        @endif 
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection