@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">
    
        <div class="col-lg-12">
                <h3>
                    Sửa thông tin dịch vụ <strong>{{$services->nameService}}</strong> 
                </h3>
           
        </div>
        <div>
        </div>
                  @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            <strong>{{$err}}</strong><br>
                        @endforeach
                    </div>
                @endif
                
                @if(session('error'))
                    <div class="alert alert-danger">
                        <strong>{{session('error')}}</strong>
                    </div>
                @endif

                @if(session('message'))
                    <div class="alert alert-success">
                        <strong>{{session('message')}}</strong>
                    </div>
               @endif
        </div>
        <form action="{{route('admin.SuaDichVu',$services->id)}}" method="Post" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group">
                  <label >Tên dịch vụ</label>
                <input type="text" class="form-control" name="nameService" value="{{$services->nameService}}">
                </div>
                <div class="form-group">
                  <label >Giới thiệu về dịch vụ</label>
                  <textarea type="text" class="form-control" name="content"  rows="100" >{{$services->content}}</textarea>
                </div>
                <div class="form-group">
                        <label>Ảnh</label>
                        <p>
                        <img width="400px" src="{{asset('storage/'.$services->images)}}">
                <input type="file" class="form-control" name="Images">
                <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" /> 

                        </p>                
                        
                    
                </div>
               
                <button type="submit" class="btn btn-primary">Cập nhập dịch vụ</button>
              </form>
            </div>
        
@endsection