@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

    <div class="col-lg-12">
        <h3>
            Thêm loại dịch vụ <strong></strong>
        </h3>
    </div>
    

    @if(session('message'))
    <div class="alert alert-success">
        <strong>{{session('message')}}</strong>
    </div>
    @endif
    <form action="{{route('admin.themDichVu')}}" method="Post" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="form-group">
            <label>Tên dịch vụ</label>
            <input type="text" class="form-control" name="nameService" placeholder="Nhập tên dịch vụ">
            @if($errors->has('nameService'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('nameService')}}</strong>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Giới thiệu về dịch vụ</label>
            <textarea type="text" class="form-control" name="content" row="10"
                placeholder="Giới thiệu về dịch vụ"></textarea>
                @if($errors->has('content'))
                <div class="alert alert-danger">
                    <strong>{{ $errors->first('content')}}</strong>
                </div>
                @endif
        </div>
        <div class="form-group">
            <label>Ảnh</label>
            <input type="file" class="form-control" name="imageSevice">
            <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" /> 


        </div>

        <button type="submit" class="btn btn-primary">Thêm dịch vụ</button>
    </form>
</div>

@endsection