@extends('admin.layout.layout')
@section('content')

<div class="content">
 <div class="row">
  <div class="col-md-12">
   <div class="card">
    <div class="card-header">
     <h4 class="card-title"> Danh sách tin tức sự kiện</h4>
     <a href="{{route('admin.themTTSK')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"
       aria-hidden="true"></i> Thêm tin tức sự kiện
     </a>
    </div>
    @if(session('message'))
    <div class="alert alert-success">
     <strong>{{session('message')}}</strong>
    </div>
    @endif
    <div class="card-body">
     <div class="table-responsive">
      <table class="table">
       <thead class=" text-primary text-center">
        <th>Tên Tin Tức Sự Kiện</th>
        <th>Nội Dung</th>
        <th>Ảnh</th>
        <th>Ngày tổ chức</th>
        <th colspan="2">Hành Động</th>
       </thead>
       <tbody>
        @foreach($neweven as $newevens)
        <tr>
         <td>{{ $newevens->title }}</td>
         <td style="width:450px; height:100px;overflow-y: scroll;">{{ $newevens->content}}</td>
         <td>@if($newevens->Anh)
          <img src="{{ asset('storage/'.$newevens->Anh)}}" alt="" style="width: 200px; height: 150px">
          @else
          {{'Chưa có ảnh'}}
          @endif
         </td>
         <td>{{$newevens->created_at}}</td>
         <td>
          <a href="{{route('admin.suaTTSK',$newevens->id)}}" class="btn btn-info ">
           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
          </a>
         </td>
         <td>
          <a class="btn btn-danger" onclick="return confirm('Are you sure?')"
           href="{{route('admin.xoaTTSK',$newevens->id)}}"><i class="fa fa-trash"></i></a>
         </td>
        </tr>
        @endforeach
       </tbody>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
@endsection