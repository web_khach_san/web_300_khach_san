@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

 <div class="col-lg-12">
  <h3>
   Sửa thông tin tin tức sự kiện <strong>{{$neweven->title}}</strong>
  </h3>

 </div>
 <div>
 </div>
 @if(count($errors) > 0)
 <div class="alert alert-danger">
  @foreach($errors->all() as $err)
  <strong>{{$err}}</strong><br>
  @endforeach
 </div>
 @endif
 @if(session('error'))
 <div class="alert alert-danger">
  <strong>{{session('error')}}</strong>
 </div>
 @endif
 @if(session('message'))
 <div class="alert alert-success">
  <strong>{{session('message')}}</strong>
 </div>
 @endif
</div>
<form action="{{route('admin.suaTTSK',$neweven->id)}}" method="Post" enctype="multipart/form-data">
 <input type="hidden" name="_token" value="{{ csrf_token() }}" />
 <div class="form-group">
  <label>Tên tin tức sự kiện</label>
  <input type="text" class="form-control" name="title" value="{{$neweven->title}}">
 </div>
 <div class="form-group">
  <label>Nội Dung</label>
  <textarea type="text" class="form-control" name="content" rows="100">{{$neweven->content}}</textarea>
 </div>
 <div class="form-group">
  <label>Ảnh</label>
  <p>
   <img width="400px" src="{{asset('storage/'.$neweven->Anh)}}">
   <input type="file" class="form-control" name="Anh">
   <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
  </p>
 </div>
 <button type="submit" class="btn btn-primary">Cập nhập</button>
</form>
</div>

@endsection