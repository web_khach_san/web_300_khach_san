@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

 <div class="col-lg-12">
  <h3>
   Thêm loại tin tức sự kiện <strong></strong>
  </h3>
 </div>


 @if(session('message'))
 <div class="alert alert-success">
  <strong>{{session('message')}}</strong>
 </div>
 @endif
 <form action="{{route('admin.themTTSK')}}" method="Post" enctype="multipart/form-data">

  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <div class="form-group">
   <label>Tên tin tức sự kiện</label>
   <input type="text" class="form-control" name="title" placeholder="Nhập tên tin tức sự kiện">
   @if($errors->has('title'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('title')}}</strong>
   </div>
   @endif
  </div>
  <div class="form-group">
   <label>Nội Dung</label>
   <textarea type="text" class="form-control" name="content" row="10" placeholder="Nội dung"></textarea>
   @if($errors->has('content'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('content')}}</strong>
   </div>
   @endif
  </div>
  <div class="form-group">
   <label>Ảnh</label>
   <input type="file" class="form-control" name="imageTTSK">
   <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
  </div>
  <button type="submit" class="btn btn-primary">Thêm</button>
 </form>
</div>
@endsection