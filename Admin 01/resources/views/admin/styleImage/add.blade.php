@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

 <div class="col-lg-12">
  <h3>
   Thêm loại ảnh <strong></strong>
  </h3>
 </div>

 @if(session('message'))
 <div class="alert alert-success">
  <strong>{{session('message')}}</strong>
 </div>
 @endif
 <form action="{{route('admin.themStyleImage')}}" method="Post" enctype="multipart/form-data">

  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <div class="form-group">
   <label>Tên loại ảnh</label>
   <textarea type="text" class="form-control " name="nameStyle" row="10" placeholder="Nhập tên loại ảnh"></textarea>
   @if($errors->has('nameStyle'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('nameStyle')}}</strong>
   </div>
   @endif
  </div>
  <div class="form-group">
   <label>Mô tả về loại ảnh</label>
   <textarea type="text" class="form-control ckeditor" name="description" row="10"
    placeholder="Mô tả  về loại ảnh"></textarea>
   @if($errors->has('description'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('description')}}</strong>
   </div>
   @endif
  </div>
  <div class="form-group">
   <label>Ảnh</label>
   <input type="file" class="form-control" value="file" name="imageStyleImage">
   <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
   @if($errors->has('imageStyleImage'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('imageStyleImage')}}</strong>
   </div>
   @endif
  </div>
  <button type="submit" class="btn btn-primary">Thêm loại ảnh</button>
 </form>
</div>

@endsection