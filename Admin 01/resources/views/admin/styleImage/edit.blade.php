@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

 <div class="col-lg-12">
  <h3>
   Sửa thông tin về loại ảnh
  </h3>
 </div>
 @if(count($errors) > 0)
 <div class="alert alert-danger">
  @foreach($errors->all() as $err)
  <strong>{{$err}}</strong><br>
  @endforeach
 </div>
 @endif

 @if(session('error'))
 <div class="alert alert-danger">
  <strong>{{session('error')}}</strong>
 </div>
 @endif

 @if(session('message'))
 <div class="alert alert-success">
  <strong>{{session('message')}}</strong>
 </div>
 @endif
 <form action="{{route('admin.suaStyleImage',$styleImage->id)}}" method="Post" enctype="multipart/form-data">

  <input type="hidden" name="_token" value="{{ csrf_token() }}" />

  <div class="form-group">
   <label>Tên loại ảnh</label>
   <textarea type="text" class="form-control" name="nameStyle" row="10">{{$styleImage->nameStyle}}</textarea>
  </div>
  <div class="form-group">
   <label>Mô tả về loại ảnh</label>
   <textarea type="text" class="form-control ckeditor" name="description"
    row="10">{{$styleImage->description}}</textarea>
  </div>
  <div class="form-group">
   <label>Ảnh</label>
   <p>
    <img width="400px" src="{{asset('storage/'.$styleImage->image )}}">
    <input type="file" class="form-control" name="Image">
    <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
   </p>
  </div>
  <button type="submit" class="btn btn-primary">Sửa loại ảnh</button>
 </form>
</div>
@endsection