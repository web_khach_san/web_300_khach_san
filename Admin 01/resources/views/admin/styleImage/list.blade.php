@extends('admin.layout.layout')
@section('content')
<div class="content">
 <div class="row">
  <div class="col-md-12">
   <div class="card">
    <div class="card-header">
     <h4 class="card-title"> Danh sách loại ảnh</h4>

     <a href="{{route('admin.themStyleImage')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"
       aria-hidden="true"></i> Thêm loại ảnh
     </a>
    </div>

    @if(session('message'))
    <div class="alert alert-success">
     <strong>{{session('message')}}</strong>
    </div>
    @endif
    <div class="card-body">
     <div class="table-responsive">
      <table class="table" style="width:1165px;">
       <thead class=" text-primary text-center">
        <th>Tên Ảnh</th>
        <th>Mô tả</th>
        <th>Ảnh </th>
        <th colspan="2">Hành Động</th>
       </thead>
       <tbody class="text-center">

        @foreach($styleImage as $styleImages)
        <tr>
         <td style="width:150px;">
          {{ $styleImages->nameStyle}}
         </td>
         <td style="width:450px; height:100px;overflow-y: scroll;">
          {{ $styleImages->description}}
         </td>
         <td style="width:150px;">
          @if($styleImages->image)
          <img src="{{ asset('storage/'.$styleImages->image) }}" alt="" style="width: 150px; height: 100px">
          @else
          {{'Chưa có ảnh'}}
          @endif
         </td>
         <td style="width:30px;">

          <a href="{{route('admin.suaStyleImage',$styleImages->id)}}" class="btn btn-info ">
           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
          </a>
         </td>
         <td style="width:30px;">
          <a class="btn btn-danger" onclick="return confirm('Bạn muốn xóa?')"
           href="{{route('admin.xoaStyleImage',$styleImages->id)}}"><i class="fa fa-trash"></i></a>
         </td>
        </tr>
        @endforeach
       </tbody>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
@endsection