@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

  <div class="col-lg-12">
    <h3>
      Thêm loại phòng <strong></strong>
    </h3>
  </div>


  @if(session('message'))
  <div class="alert alert-success">
    <strong>{{session('message')}}</strong>
  </div>
  @endif
  <form action="{{route('admin.themLoaiPhong')}}" method="Post" enctype="multipart/form-data">

    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <div class="form-group">
      <label>Mã số loại phòng</label>
      <input type="text" class="form-control" name="maSo" placeholder="Nhập mã số loại phòng">
      @if($errors->has('maSo'))
      <div class="alert alert-danger">
        <strong>{{ $errors->first('maSo')}}</strong>
      </div>
      @endif
    </div>
    <div class="form-group">
      <label>Tên loại phòng</label>
      <textarea type="text" class="form-control " name="nameStyle" row="10" placeholder="Nhập tên loại phòng"></textarea>
      @if($errors->has('nameStyle'))
      <div class="alert alert-danger">
        <strong>{{ $errors->first('nameStyle')}}</strong>
      </div>
      @endif
    </div>
    <div class="form-group">
      <label>Mô tả về loại phòng</label>
      <textarea type="text" class="form-control ckeditor" name="Mota" row="10" placeholder="Mô tả  về loại phòng"></textarea>
      @if($errors->has('Mota'))
      <div class="alert alert-danger">
        <strong>{{ $errors->first('Mota')}}</strong>
      </div>
      @endif
    </div>
    <div class="form-group">
      <label>Ảnh</label>
      <input type="file" class="form-control" value="file" name="imageStyleroom">
      <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" /> 
      @if($errors->has('imageStyleroom'))
      <div class="alert alert-danger">
        <strong>{{ $errors->first('imageStyleroom')}}</strong>
      </div>
      @endif
    </div>
    <div class="form-group">
      <label>Giá loại phòng</label>
      <input type="number" class="form-control" name="Gia" placeholder="Nhập giá loại phòng">
      @if($errors->has('Gia'))
      <div class="alert alert-danger">
        <strong>{{ $errors->first('Gia')}}</strong>
      </div>
      @endif
    </div>
    <button type="submit" class="btn btn-primary">Thêm loại phòng</button>
  </form>
</div>

@endsection