@extends('admin.layout.layout')
@section('content')

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Danh sách loại phòng</h4>
          <p>Tìm thấy: {{ count($styleRooms)}} loại phòng</p>
          <a href="{{route('admin.themLoaiPhong')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"
              aria-hidden="true"></i> Thêm loại phòng
          </a>
        </div>

        @if(session('message'))
        <div class="alert alert-success">
          <strong>{{session('message')}}</strong>
        </div>
        @endif
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" style="width:1165px;">
              <thead class=" text-primary text-center">
                <th>Mã</th>
                <th>Tên</th>
                <th>Giá</th>
                <th>Mô tả</th>
                <th>Ảnh </th>
                <th colspan="2">Hành Động</th>
              </thead>
              <tbody class="text-center" >

                @foreach($styleRooms as $styleRoom)
                <tr>
                  <td style="width:30px;">
                    {{ $styleRoom->MaLoaiPhong }}
                  </td>
                  <td style="width:150px;">
                    {{ $styleRoom->TenLoaiPhong}}
                  </td>
                  <td>
                    {{ $styleRoom->Gia}}

                  </td>

                  <td style="width:450px; height:100px;overflow-y: scroll;">
                    {!! $styleRoom->Mota !!}
                  </td>
                  <td style="width:150px;">
                    @if($styleRoom->Anh)
                    <img src="{{ asset('storage/'.$styleRoom->Anh) }}" alt="" style="width: 150px; height: 100px">
                    @else
                    {{'Chưa có ảnh'}}
                    @endif
                  </td>
                  <td style="width:30px;">

                    <a href="{{route('admin.SuaLoaiPhong',$styleRoom->id)}}" class="btn btn-info ">
                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                  </td>
                  <td style="width:30px;">
                    <a class="btn btn-danger" onclick="return confirm('Are you sure?')"
                      href="{{route('admin.xoaLoaiPhong',$styleRoom->id)}}"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{!! $styleRooms->render() !!}
@endsection
