@extends('admin.layout.layout')
@section('content')
<div class="card">
 <div class="card-header">
  <legend>Thêm quảng cáo</legend>
 </div>
 <div class="card-body">
  <form action="{{route('admin.storeAds')}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Tên công ty</label>
    <input type="text" class="form-control" name="name" placeholder="Nhập tên công ty">
   </div>

   <div class="form-group">
    <label for="">Email</label>
    <input type="text" class="form-control" name="email" placeholder="Nhập Email">
   </div>

   <div class="form-group">
    <label for="">Số điện thoại</label>
    <input type="number" class="form-control" name="phone" placeholder="Nhập Số điện thoại">
   </div>

   <div class="form-group">
    <label>Ảnh</label>
    <input type="file" class="form-control" name="imageAds">
    <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
   </div>

   <div class="form-group">
    <label for="">Website</label>
    <input type="text" class="form-control" name="website" placeholder="Nhập tên website">
   </div>

   <button type="submit" class="btn btn-primary">Thêm</button>
  </form>
 </div>
</div>
@endsection