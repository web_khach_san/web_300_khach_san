@extends('admin.layout.layout')
@section('content')
<div class="card">
  <div class="card-header">
    <legend>Quản lý quảng cáo</legend>
    <a href="{{route('admin.createAds')}}"><button class="btn btn-primary" Route>Thêm quảng cáo</button></a>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table" style="width:1165px;">
        <thead class="text-primary">
          <th class="text-center">Tên công ty</th>
          <th class="text-center">Email</th>
          <th class="text-center">Số điện thoại</th>
          <th class="text-center">Ảnh</th>
          <th class="text-center">Website</th>
          <th colspan="2" class="text-center">Chức năng</th>
        </thead>
        <tbody class="text-center">
          @foreach($allAds as $ad)
          <tr>
            <td>{{$ad->name}}</td>
            <td>{{$ad->email}}</td>
            <td>{{$ad->phone}}</td>
            <td><img src="{{asset('storage/'.$ad->image)}}" alt="" style="width: 150px; height: 100px"></td>
            <td>{{$ad->website}}</td>
            <td>
              <a class="btn btn-info" href="{{route('admin.editAds',$ad->id)}}">
                <i class="fa fa-edit"></i></a>
            </td>
            <td>
              <a class="btn btn-danger" onclick="return confirm('Are you sure?')"
                href="{{route('admin.deleteAds',$ad->id)}}">
                <i class="fa fa-trash"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection