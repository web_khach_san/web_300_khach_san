@extends('admin.layout.layout')
@section('content')
<div class="card">
 <div class="card-header">
  <legend>Sửa quảng cáo</legend>
 </div>
 <div class="card-body">
  <form action="{{route('admin.suaEditAds',$ad->id)}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Tên công ty</label>
    <input type="text" class="form-control" name="name" value="{{$ad->name}}">
   </div>

   <div class="form-group">
    <label for="">Email</label>
    <input type="text" class="form-control" name="email" value="{{$ad->email}}">
   </div>

   <div class="form-group">
    <label for="">Số điện thoại</label>
    <input type="number" class="form-control" name="phone" value="{{$ad->phone}}">
   </div>

   <div class="form-group">
    <label>Ảnh</label>
    <img width="400px" src="{{asset('storage/'.$ad->imageAds)}}">
    <input type="file" class="form-control" name="imageAds">
    <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
   </div>

   <div class="form-group">
    <label for="">Website</label>
    <input type="text" class="form-control" name="website" value="{{$ad->website}}">
   </div>

   <button type="submit" class="btn btn-primary">Sửa</button>
  </form>
 </div>
</div>
@endsection