@extends('admin.layout.layout')
@section('content')

<div class="content">
 <div class="row">
  <div class="col-md-12">
   <div class="card">
    <div class="card-header">
     <h4 class="card-title"> Danh sách ảnh</h4>
     <a href="{{route('admin.themImage')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"
       aria-hidden="true"></i> Thêm Ảnh
     </a>
    </div>

    @if(session('message'))
    <div class="alert alert-success">
     <strong>{{session('message')}}</strong>
    </div>
    @endif
    <div class="card-body">
     <div class="table-responsive">
      <table class="table">
       <thead class=" text-primary text-center">
        <th>Tên Ảnh</th>
        <th>Loại Ảnh</th>
        <th>Ảnh</th>
        <th colspan="2">Hành Động</th>
       </thead>
       <tbody class="text-center">

        @foreach($images as $image)
        <tr>
         <td>{{ $image->name }}</td>
         <td>{{$image->styleImage->nameStyle}}</td>

         <td>@if($image->image)
          <img src="{{ asset('storage/'.$image->image) }}" alt="" style="width: 300px;
         height: 200px">
          @else
          {{'Chưa có ảnh'}}
          @endif
         </td>
         <td>
          <a href="{{route('admin.suaImage',$image->id)}}" class="btn btn-info">
           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
          </a>
         </td>
         <td>
          <a class="btn btn-danger" onclick="return confirm('Bạn muốn xóa?')"
           href="{{route('admin.xoaImage',$image->id)}}"><i class="fa fa-trash"></i></a>
         </td>
        </tr>
        @endforeach
       </tbody>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
@endsection