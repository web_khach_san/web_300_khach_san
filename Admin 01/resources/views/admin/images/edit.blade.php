@extends('admin.layout.layout')

@section('content')
<div class="container-fluid">

 <div class="col-lg-12">
  <h3>
   Sửa thông tin ảnh {{$images->name}}
  </h3>
 </div>
 <div>
 </div>
 @if(count($errors) > 0)
 <div class="alert alert-danger">
  @foreach($errors->all() as $err)
  <strong>{{$err}}</strong><br>
  @endforeach
 </div>
 @endif

 @if(session('error'))
 <div class="alert alert-danger">
  <strong>{{session('error')}}</strong>
 </div>
 @endif

 @if(session('message'))
 <div class="alert alert-success">
  <strong>{{session('message')}}</strong>
 </div>
 @endif
</div>
<form action="{{route('admin.suaImage',$images->id)}}" method="Post" enctype="multipart/form-data">

 <input type="hidden" name="_token" value="{{ csrf_token() }}" />
 <div class="form-group">
  <label>Tên Ảnh</label>
  <input type="text" class="form-control" name="name" value="{{$images->name}}">
 </div>
 <div class="form-group">
  <label>Ảnh</label>
  <p>
   <img width="400px" src="{{asset('storage/'.$images->image)}}">
   <input type="file" class="form-control" name="Images">
   <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
  </p>
 </div>
 <div class="form-group">
  <label for="exampleInputPassword1">Loại Ảnh</label>
  <select class="form-control" name="loaiAnh">
   @foreach($styleImage as $style)
   <option @if($images->id_styleImage == $style->id)
    {{ 'selected' }}
    @endif
    value="{{ $style->id }}">{{ $style->nameStyle }}</option>
   @endforeach
   </option>
  </select>
 </div>
 <button type="submit" class="btn btn-primary">Sửa</button>
</form>
</div>

@endsection