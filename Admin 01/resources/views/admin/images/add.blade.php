@extends('admin.layout.layout')

@section('content')
<!-- Page Content -->
<div class="container-fluid">

 <div class="col-lg-12">
  <h3>
   Thêm ảnh
  </h3>
 </div>


 @if(session('message'))
 <div class="alert alert-success">
  <strong>{{session('message')}}</strong>
 </div>
 @endif
 <form action="{{route('admin.themImage')}}" method="Post" enctype="multipart/form-data">

  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <div class="form-group">
   <label>Tên Ảnh</label>
   <input type="text" class="form-control" name="name" placeholder="Nhập tên ảnh">
   @if($errors->has('name'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('name')}}</strong>
   </div>
   @endif
  </div>
  <div class="form-group">
   <label>Ảnh</label>
   <input type="file" class="form-control" name="imageImages">
   <input type="button" value="Choose File" onclick="document.getElementById('selectedFile').click();" />
  </div>
  <div class="form-group">
   <label for="exampleInputPassword1">Loại Ảnh</label>
   <select class="form-control" name="loaiAnh">
    @foreach($styleImage as $style)
    <option value="{{ $style->id }}">{{ $style->nameStyle }}</option>
    @endforeach
    </option>
   </select>
   @if($errors->has('loaiAnh'))
   <div class="alert alert-danger">
    <strong>{{ $errors->first('loaiAnh')}}</strong>
   </div>
   @endif
  </div>
  <button type="submit" class="btn btn-primary">Thêm </button>
 </form>
</div>

@endsection