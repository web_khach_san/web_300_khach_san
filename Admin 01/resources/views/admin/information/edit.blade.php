@extends('admin.layout.layout')
@section('content')     
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Sửa thông tin</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.updateInfor',$thongtin->id)}}" method="Post">
                    @csrf
                        <div class="form-group">
                            <label for="my-input">name</label>
                            <input  class="form-control" value="{{ $thongtin->name }}" type="text" name="name">
                            @if($errors->has('name'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('name')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">email</label>
                            <input  value="{{ $thongtin->email }}" class="form-control" type="text" name="email">
                            @if($errors->has('email'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('email')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">address</label>
                            <input  class="form-control" value="{{ $thongtin->address }}" type="text" name="address">
                            @if($errors->has('address'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('address')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">phone</label>
                            <input class="form-control" value="{{ $thongtin->phone }}" type="text" name="phone">
                            @if($errors->has('phone'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('phone')}}</strong>
                            </div>
                            @endif
                           
                            <label for="my-input">website</label>
                            <input  class="form-control" value="{{ $thongtin->website }}" type="text" name="website">
                            @if($errors->has('website'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('website')}}</strong>
                            </div>
                            @endif
                            <label for="my-input">content</label>
                            <textarea id="summernote" name="content"  class="form-control ckeditor"> value="{{ $thongtin->content }}"</textarea>
                            @if($errors->has('content'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('content')}}</strong>
                            </div>
                            @endif
                            <input type="submit" class="btn btn-success" value="Sửa thông tin khách sạn">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
 
</div>

@endsection
