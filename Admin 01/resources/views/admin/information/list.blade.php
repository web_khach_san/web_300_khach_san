@extends('admin.layout.layout')
@section('content')


<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Thông tin khách sạn</h4>

          <a href="{{route('admin.themThongTin')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"
              aria-hidden="true"></i> Thêm thông tin
          </a>
        </div>

        @if(session('message'))
        <div class="alert alert-success">
          <strong>{{session('message')}}</strong>
        </div>
        @endif
        <style>
          .card-body label {
            font-size: 17px;
            font-weight: bold;
          }

        </style>
        @foreach($thongtins as $thongtin)
        <div class="card-body">
          <div class="form-group">
            <label for="">Tên khách sạn : </label>
            <span>{{ $thongtin->name }}</span>
          </div>
          <div class="form-group">
            <label for="">Email : </label>
            <span>{{ $thongtin->email }}</span>
          </div>
          <div class="form-group">
            <label for="">Địa chỉ : </label>
            <span>{{ $thongtin->address }}</span>
          </div>
          <div class="form-group">
            <label for="">Điện thoại : </label>
            <span>{{ $thongtin->phone }}</span>
          </div>
          <div class="form-group">
            <label for="">Fax: </label>
            <span>{{ $thongtin->fax }}</span>
          </div>
          <div class="form-group">
            <label for="">Website : </label>
            <span>{{ $thongtin->website }}</span>
          </div>
          <div class="form-group">
            <label for="">Nội dung : </label>
            <span>{!! $thongtin->content !!}</span>
          </div>
        </div>
       <a href="{{ route('admin.editInfor',$thongtin->id) }}">Sửa</a> 
        @endforeach
      </div>
    </div>
  </div>
</div>



@endsection