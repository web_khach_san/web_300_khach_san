<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <base href="{{ asset('') }}"></base>
  @include('template.link.link')
</head>
<body>
  <header class="header_area">
    @include('template.header.header_top')
    @include('template.header.header_bottom')
  </header>
  <main class="site-main">
    @include('template.banner')
    @yield('content')
  </main>
  @include('template.chat')
  <footer class="footer-area section-gap">
   @include('template.footer.footer')
  </footer>
  @include('template.link.script')
</body>

</html>