CKEDITOR.editorConfig = function(config) {
    config.enterMode = CKEDITOR.ENTER_BR;
    config.enterMode = CKEDITOR.ENTER_P;
    config.enterMode = 2;
    config.autoParagraph = false
    config.enterMode = CKEDITOR.ENTER_DIV;
    CKEDITOR.dtd.$removeEmpty['i'] = false;
    // Define changes to default configuration here. For example:
    config.language = 'en';

    // config.uiColor = '#AADC6E';

    config.toolbar_Full = [
        ['Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
        ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
        ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
        '/', ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['BidiLtr', 'BidiRtl'],
        ['Link', 'Unlink', 'Anchor'],
        ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'],
        '/', ['Styles', 'Format', 'Font', 'FontSize'],
        ['TextColor', 'BGColor'],
        ['Maximize', 'ShowBlocks', '-', 'About']
    ];

    config.uiColor = '# 949AAA';
    config.entities = false;
    //config.entities_latin = false;

    config.filebrowserBrowseUrl = 'admin_asset/ckfinder/ckfinder.html';

    config.filebrowserImageBrowseUrl = 'admin_asset/ckfinder/ckfinder.html?type=Images';

    config.filebrowserFlashBrowseUrl = 'admin_asset/ckfinder/ckfinder.html?type=Flash';

    config.filebrowserUploadUrl = 'admin_asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';

    config.filebrowserImageUploadUrl = 'admin_asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

    config.filebrowserFlashUploadUrl = 'admin_asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';


};