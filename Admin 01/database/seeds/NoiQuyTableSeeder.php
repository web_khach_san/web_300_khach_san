<?php

use Illuminate\Database\Seeder;

class NoiQuyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('noiquy')->insert([
                'name' => 'Nội Quy Khách Sạn',
                'content' => "Khách sạn SAPPHIRE nằm ngay trung tâm khu phố Tây nhộn nhịp nơi những căn phòng được thiết kế với gam màu xanh thanh lịch và tinh tế kèm theo những ô cửa sổ hay ban công nhỏ, nơi quý khách có thể thoải mái thư giãn nhâm nhi tách trà hay cà phê, hít thở và ngắm nhìn một phần hồn xứ Huế đang chầm chậm trôi trên phố. Khách sạn này được quản lý bởi một giáo viên dạy Yoga vì thế bạn có thể có được 1h học yoga miễn phí cùng cô ấy tất cả sẽ trở nên tuyệt vời cho kỳ nghỉ của bạn.",
            ]);
    }
}