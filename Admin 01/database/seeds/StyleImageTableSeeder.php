<?php

use Illuminate\Database\Seeder;

class StyleImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('style_images')->insert([
            [
                'nameStyle' => 'Ảnh quảng cáo',
                'description' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín',
                'image' => 'https://bitly.vn/5j9k',
            ],
            [
                'nameStyle' => 'Ảnh tin tức sự kiện',
                'description' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín',
                'image' => 'https://bitly.vn/5j9k',
            ],
            [
                'nameStyle' => 'Ảnh Dịch Vụ',
                'description' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín',
                'image' => 'https://bitly.vn/5j9k',
            ],
            [
                'nameStyle' => 'Ảnh Tour',
                'description' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín',
                'image' => 'https://bitly.vn/5j9k',
            ],
            [
                'nameStyle' => 'Ảnh Các Loại Phòng',
                'description' => 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín',
                'image' => 'https://bitly.vn/5j9k',
            ],
        ]);
    }
}