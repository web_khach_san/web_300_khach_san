<?php

use Illuminate\Database\Seeder;

class ImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            [
                'name' => 'Ảnh phòng ngủ',
                'image' => 'http://fiboholiday.webhotel.vn/files/images/Room/suite/1.jpg',
                'id_styleImage' => '1'
            ],
            [
                'name' => 'Ảnh phòng ngủ',
                'image' => 'http://fiboholiday.webhotel.vn/files/images/Room/suite/1.jpg',
                'id_styleImage' => '2'
            ],
            [
                'name' => 'Ảnh phòng ngủ',
                'image' => 'http://fiboholiday.webhotel.vn/files/images/Room/suite/1.jpg',
                'id_styleImage' => '3'
            ],
        ]);
    }
}