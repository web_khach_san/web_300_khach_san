<?php

use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
                'nameService' => 'Dịch vụ nhà hàng và Cafe',
                'content' => 'Quán cà phê trong khuôn viên, Bữa ăn tự chọn phù hợp với trẻ em, Bữa sáng tại phòng',
                'images' => "plugin/img/IMGdichvu/dv3.jpg",
            ],
            [
                'nameService' => 'Dịch Vụ Giặt Là',
                'content' => 'Dọn phòng hàng ngày, dịch vụ là (ủi), giặt khô giặt ủi',
                'images' => "plugin/img/IMGdichvu/giatla.jpg",
            ],
            [
                'nameService' => 'Dịch vụ thuê phương tiện đi lại',
                'content' => 'Cho thuê xe đạp, xe máy, xe hơi',
                'images' => "plugin/img/IMGdichvu/thuexe.jpg",
            ],
            [
                'nameService' => 'Dịch vụ lữ hành và đặt vé máy bay',
                'content' => 'Đưa khách ra sân bay, Đón khách tại sân bay, Đặt vé máy bay ',
                'images' => "plugin/img/IMGdichvu/sanbay.jpg",
            ],
        ]);
    }
}