<?php

use Illuminate\Database\Seeder;

class ContentReservationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content_reservations')->insert
        ([
            'content'=>"
            Khách sạn Mường Thanh Holiday Huế có tổng cộng 108 phòng nghỉ với không gian sang trọng, nội thất ấm cúng, đầy đủ các trang thiết bị tiện nghi, hiện đại, đạt tiêu chuẩn quốc tế. Wi-Fi miễn phí trong tất cả các phòng và khu vực công cộng, quầy lễ tân và dịch vụ phòng phục vụ 24/24 giờ, bãi đỗ xe thuận tiện chỉ là một vài trong số những thiết bị được khách sạn cung cấp cho quý khách.",
            'checkIn'=>"2PM",
            'checkOut'=>"12PM",
            'note'=>
            "Nếu Quý khách có bất cứ câu hỏi gì về việc đặt phòng, vui lòng liên hệ với chúng tôi qua số hotline: 1900 1833.",
        ]);
    }
}
