<?php

use Illuminate\Database\Seeder;

class StyleRoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('style_rooms')->insert([
            [
                'MaLoaiPhong' => '01',
                'TenLoaiPhong' => 'Deluxe Double City View',
                'Anh' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQR99ndHs3ZeBt2tbIxN24alpdb7ob1k2wKHDPVKzvsCFF9oDOK',
                'Gia' => '450',
                'Mota' => 'Với 1 phòng nghỉ (diện tích từ 20 đến 22 m2) đều được thiết kế tao nhã và trang bị đầy đủ tiện nghi.',

            ],
            [
                'MaLoaiPhong' => '02',
                'TenLoaiPhong' => 'Deluxe King Room City View',
                'Anh' => 'http://hue.muongthanh.com/FileUpload/Images/thumb/_1380636.jpg',
                'Gia' => '500',
                'Mota' => 'Với 6 phòng nghỉ (diện tích từ 20 đến 22 m2) đều được thiết kế tao nhã và trang bị đầy đủ tiện nghi.',
            ],
            [
                'MaLoaiPhong' => '03',
                'TenLoaiPhong' => 'Deluxe Family City View',
                'Anh' => 'https://bitly.vn/5peh',
                'Gia' => '550',
                'Mota' => 'Với 2 phòng nghỉ (diện tích từ 20 đến 22 m2) đều được thiết kế tao nhã và trang bị đầy đủ tiện nghi.',
            ],
            [
                'MaLoaiPhong' => '04',
                'TenLoaiPhong' => 'Deluxe Family With Balcony',
                'Anh' => 'https://bitly.vn/5peh',
                'Gia' => ' 600',
                'Mota' => 'Với 2 phòng nghỉ (diện tích từ 20 đến 22 m2) đều được thiết kế tao nhã và trang bị đầy đủ tiện nghi.',
            ],
            [
                'MaLoaiPhong' => '05',
                'TenLoaiPhong' => 'Superior Double',
                'Anh' => 'https://bitly.vn/5peh',
                'Gia' => '400',
                'Mota' => 'Với 4 phòng nghỉ (diện tích từ 20 đến 22 m2) đều được thiết kế tao nhã và trang bị đầy đủ tiện nghi.',
            ],
        ]);
    }
}