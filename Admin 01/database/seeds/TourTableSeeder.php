<?php

use Illuminate\Database\Seeder;

class TourTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tours')->insert([
            [
                'nameTour' => 'Huế City Tour - 1 Ngày ở Huế ',
                'content' => 'Tham quan Đại Nội, Chùa Thiên Mụ, Lăng Minh Mạng, Lăng Khải Định, Thuyền rồng trên sông Hương',
                'images' => "plugin/img/IMGdichvu/huetour.jpg",
            ],
            [
                'nameTour' => 'Tour Khám phá ẩm thực xứ Huế',
                'content' => 'Thời gian: 18h00 - 22h00, Di chuyển bằng xe',
                'images' => "plugin/img/IMGdichvu/amthuchue.png",
            ],
            [
                'nameTour' => 'Tour Hoàng hôn phá Tam Giang - Khám phá đầm phá lớn nhất',
                'content' => 'Thời gian: 1 đến 2 ngày.',
                'images' => "plugin/img/IMGdichvu/phatamgiang.jpg",
            ],
            [
                'nameTour' => 'Tour Khám phá Bạch Mã Huế',
                'content' => 'Thời gian: 1 Ngày',
                'images' => "plugin/img/IMGdichvu/bachma.jpg",
            ],
        ]);
    }
}