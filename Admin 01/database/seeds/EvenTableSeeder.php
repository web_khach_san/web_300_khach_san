<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EvenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news__evens')->insert([
            [
                'title' => 'Bánh Nguyễn Triều - Phẩm vị tri ân
                “Giữ trọn tinh hoa văn hóa ẩm thực"',
                'content' => 'Huế - mảnh đất cố đô không chỉ nổi tiếng bởi sự nguy nga tráng lệ của các lăng tẩm, nét dịu dàng thơ mộng của thiên nhiên non nước hài hòa. Huế còn là ‘kinh đô ẩm thực’ làm khách du lịch say lòng. 
                Ẩm thực Huế luôn được đánh giá rất cao, đặc biệt ẩm thực cung đình Huế nổi tiếng không chỉ ở cách chế biến mà còn ở cách trình bày, trang trí món ăn, làm nên nét đặc sắc mà không phải nơi nào cũng có.
                Tuy nhiên theo thời gian, những món ăn cung đình xưa phần nào đã bị thất truyền.
                Với lòng tri ân tổ tiên và vùng đất cố đô, những hậu duệ của hoàng tộc nhà Nguyễn – cũng là người khởi xướng thương hiệu Bánh cung đình Nguyễn Triều đã dành nhiều tâm huyết phục hiện những món bánh xưa trong cung đình.
                Các món bánh này được làm ra nhờ những kinh nghiệm và bí quyết làm bánh của nghệ nhân Hồ Thị Hoàng Anh – hậu duệ của Đội trưởng Đội Thượng Thiện Hồ Văn Tá triều vua Khải Định và Bảo Đại thực hiện, để những chiếc bánh ấy, không chỉ dừng lại là 1 thức bánh thông thường, phổ dụng mà còn gửi gắm vào đó là cả câu chuyện văn hóa ẩm thực của một thời đại lịch sử.
                Bánh Nguyễn Triều đã cho ra mắt thị trường được 7 dòng bánh với những hương vị đặc trưng của từng loại trái cây đặc sản khác nhau của vùng đất cố đô:
🍃 Bánh Sen
🍃 Bánh Thanh Trà
🍃 Bánh Gừng
🍃 Bánh Chuối Tiêu
🍃 Bánh Xoài
🍃 Bánh Dứa
🍃 Bánh Bưởi

Mỗi chiếc bánh là nơi đúc kết tâm tình, là tấm lòng tri ân, là câu chuyện kể về một loại cây trái đặc thù, là sản vật nổi tiếng của vùng đất Cố đô: sen hồ Tịnh, thanh trà Thủy Biều, quýt Hương Cần, hay quả vả riêng có ở xứ Huế v.v. Đó không chỉ là món ăn mà còn là văn hóa, là truyền thống, là di sản ẩm thực cung đình Huế được những người trẻ tuổi hôm nay kế thừa và tiếp bước cha ông. 

Với hương vị thơm ngon, tinh tế cùng nhiều giá trị văn hóa, lịch sử đằng sau, những sản phẩm bánh Nguyễn Triều hoàn toàn có thể trở thành MÓN QUÀ TRI  N ý nghĩa dành tặng những người mà bạn trân trọng!

Đến với Nguyễn Triều bạn có thể được tận hưởng dịch vụ chăm sóc khách hàng của chúng tôi một cách toàn diện nhất. Với tiêu chí tạo nên những giá trị tuyệt vời cho khách hàng bánh Nguyễn Triều hông chỉ chú trọng về chất lượng sản phẩm mà còn chú trọng về mặt thẩm mỹ. Từng công đoạn đều được kiểm định rõ ràng, khâu chuẩn bị nguyên liệu đảm bảo được chất lượng an toàn tuyệt đối. Nguồn trứng, bột, sữa phải được bảo quản trong môi trường đúng chuẩn quy định. Ngoài ra, đội ngũ Đầu Bếp Bánh chuyên nghiệp với phong cách làm việc sáng tạo, nhiệt huyết, tỉ mỉ trong từng thao tác như một nghệ sĩ chuyên nghiệp làm bạn hài lòng.
Bên cạnh đó, việc tạo nên những sản phẩm đạt chất lượng tốt, tiệm bánh Nguyễn Triều còn mở ra với không gian tinh tế, ấn tượng với phong cách cung đình, cổ kính, nét xưa. Nơi đây rất thích hợp để bạn lựa chọn làm nơi để sáng tạo với nguồn cảm hứng bất tận, hay lựa chọn làm nơi lắng đọng giữa những bộn bề cuộc sống để cùng thưởng thức những hương vị đẳng cấp của các loại bánh cung đình.

Hãy cùng Giữ trọn tinh hoa văn hóa ẩm thực cùng Nguyễn Triều, bạn nhé!


                ',
                'Anh' => 'plugin/img/IMGdichvu/banhnguyentrieu.jpg',
                'created_at' => Carbon::create('2019-07-01'),
            ],
        ]);
    }
}