<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StyleRoomTableSeeder::class);
        $this->call(StyleImageTableSeeder::class);
        $this->call(RoomTableSeeder::class);
        $this->call(TourTableSeeder::class);
        $this->call(ImageTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
        $this->call(AdvertisementTableSeeder::class);
        $this->call(EvenTableSeeder::class);
        $this->call(InfomationTableSeeder::class);
    }
}