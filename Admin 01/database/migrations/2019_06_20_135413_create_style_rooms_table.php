<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStyleRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('style_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('MaLoaiPhong',191);
            $table->string('TenLoaiPhong',191);
            $table->string('Gia',191);
            $table->text('Mota');
            $table->string('Anh',191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('style_rooms');
    }
}
