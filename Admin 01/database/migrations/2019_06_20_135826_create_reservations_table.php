<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
             $table->increments('id');
            $table->date('ngayden');
            $table->date('ngaydi');
            $table->string('soluong',191);
            $table->boolean('trangthai')->default(false);
            $table->text('loaiphong');
            $table->string('hoten', 255);
            $table->text('dienthoai');
            $table->string('email', 250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
