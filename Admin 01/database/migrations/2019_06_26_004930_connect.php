<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Connect extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("rooms", function (Blueprint $table) { 
            $table->foreign('id_loaiPhong')->references('id')->on('style_rooms')->onDelete('cascade');
            
        });
        
    }

  
    public function down()
    {
        
    }
}
