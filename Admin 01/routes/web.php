<?php

Route::get('/', 'HomeController@home')->name('Trangchu');
Route::get('GioiThieu', 'HomeController@Gioithieu')->name('Gioithieu');
Route::get('LoaiPhong', 'HomeController@Loaiphong')->name('Loaiphong');
Route::get('DichVu', 'HomeController@Dichvu')->name('Dichvu');
Route::get('tintucsukien', 'HomeController@TinTucSuKien')->name('tinTucSuKien');
Route::get('chitietSukien/{id}', 'HomeController@chitietSuKien')->name('chitietSukien');
Route::get('LienHe', 'HomeController@Lienhe')->name('Lienhe');
Route::get('datphong', 'HomeController@datphong')->name('trangchu.datphong');
Route::post('datphong', 'ReservationController@postDatPhong')->name('trangchu.postDatPhong');
Route::post('dienthongtin', 'FeedbackController@dienthongtin')->name('trangchu.postlienhe');
Route::get('LoaiPhong/{id}', 'HomeController@ShowLoaiPhong')->name('ShowLoaiPhong');
Route::get('ctdichvu/{id}', 'HomeController@chitietDV')->name('ctdichvu');
Route::get('ctTour/{id}', 'HomeController@ctTour')->name('ctTour');
Route::get('noiquy', 'HomeController@noiquy')->name('noiquy');




Route::group(['prefix' => 'auth'], function () {
    Route::get('login', 'UserController@login')->name('admin.login');
    Route::post('login', 'UserController@postLogin')->name('admin.postLogin');
    Route::get('logout', 'UserController@logoutUser')->name('admin.Logout');
});


Route::group(['prefix' => 'reservation'], function () {
    Route::get('danhsach', 'ReservationController@danhsachDatPhong')->name('admin.danhsachDatPhong');
    Route::get('xoa/{id}', 'ReservationController@xoaDatPhong')->name('admin.xoaDatPhong');
    Route::post('capnhattrangthai/{id}', 'ReservationController@trangthaidatphong')->name('admin.capnhapDatphong');
});



Route::group(['prefix' => 'admin'], function () {

    Route::get('', 'StatisticalController@index')->name('admin.Thongke');

    Route::group(['prefix' => 'account'], function () {
        Route::get('list', 'UserController@listUser')->name('admin.listUser');
        Route::post('update/{id}', 'UserController@updateUser')->name('admin.updateUser');
        Route::get('changepassword/{id}', 'UserController@changepassword')->name('admin.changepassword');
        Route::post('changepassword/{id}', 'UserController@postpassword')->name('admin.postpassword');
        Route::get('register', 'UserController@themUser')->name('admin.themUser');
        Route::post('register', 'UserController@storeUser')->name('admin.storeUser');
        Route::get('xoa/{id}', 'UserController@xoaUser')->name('admin.xoaUser');
    });

    Route::group(['prefix' => 'ads'], function () {
        Route::get('listAds', 'adsController@index')->name('admin.adsList');
        Route::get('addAds', 'adsController@createAds')->name('admin.createAds');
        Route::post('storeAds', 'adsController@storeAds')->name('admin.storeAds');
        Route::get('editAds/{id}', 'adsController@editAds')->name('admin.editAds');
        Route::post('editAds/{id}', 'adsController@suaEditAds')->name('admin.suaEditAds');
        Route::get('deleteAds/{id}', 'adsController@deleteAds')->name('admin.deleteAds');
    });

    Route::group(['prefix' => 'room'], function () {
        Route::get('danhsach', 'RoomController@danhSachPhong')->name('admin.danhsach');
        Route::get('sua/{id}', 'RoomController@SuaDanhSach')->name('admin.suaDanhSach');
        Route::post('sua/{id}', 'RoomController@updateDanhSachPhong');
        Route::get('xoa/{id}', 'RoomController@XoaRoom')->name('admin.xoa');
        Route::get('themPhong', 'RoomController@themPhong')->name('admin.themPhong');
        Route::post('themPhong', 'RoomController@XuLyThemPhong');
        Route::get('xoa/{id}', 'RoomController@XoaRoom')->name('admin.xoa');
        Route::post('capnhattrangthai/{id}', 'RoomController@trangthaiphong')->name('admin.capnhap');
    });

    Route::group(['prefix' => 'service'], function () {
        Route::get('danhsachdichvu', 'ServiceController@danhSachDichVu')->name('admin.danhsachDichVu');
        Route::get('Suadichvu/{id}', 'ServiceController@suaDichVu')->name('admin.SuaDichVu');
        Route::post('Suadichvu/{id}', 'ServiceController@capNhapDichVu');
        Route::get('themDichVu', 'ServiceController@themDichVu')->name('admin.themDichVu');
        Route::post('themDichVu', 'ServiceController@XuLyThemDichVu');
        Route::get('xoaDichVu/{id}', 'ServiceController@xoaDichVu')->name('admin.xoaDichVu');
    });

    Route::group(['prefix' => 'styleRoom'], function () {
        Route::get('danhsachloaiphong', 'StyleRoomController@danhSachLoaiPhong')->name('admin.danhsachloaiphong');
        Route::get('SuaLoaiPhong/{id}', 'StyleRoomController@SuaLoaiPhong')->name('admin.SuaLoaiPhong');
        Route::post('SuaLoaiPhong/{id}', 'StyleRoomController@capnhapLoaiPhong');
        Route::get('themloaiphong', 'StyleRoomController@themLoaiPhong')->name('admin.themLoaiPhong');
        Route::post('themloaiphong', 'StyleRoomController@XuLyThemLoaiPhong');
        Route::get('xoaLoaiPhong/{id}', 'StyleRoomController@xoaLoaiPhong')->name('admin.xoaLoaiPhong');
    });

    Route::group(['prefix' => 'information'], function () {
        Route::get('list', 'ContactController@lisInfor')->name('admin.listInfor');
        Route::get('add', 'ContactController@themThongTin')->name('admin.themThongTin');
        Route::post('add', 'ContactController@storeDanhSachPhong')->name('admin.storeThongTin');
        Route::get('edit/{id}', 'ContactController@editInfor')->name('admin.editInfor');
        Route::post('edit/{id}', 'ContactController@updateInfor')->name('admin.updateInfor');
    });

    Route::group(['prefix' => 'feedback'], function () {
        Route::get('lienhekhachhang', 'FeedbackController@danhSachdienthongtin')->name('admin.feedback');
        Route::get('lienhekhachhang/{id}', 'FeedbackController@xoalienhe')->name('admin.xoalienhe');
    });

    Route::group(['prefix' => 'news_evens'], function () {
        Route::get('danhsachTTSK', 'News_EvensController@danhsachTTSK')->name('admin.danhsachTTSK');
        Route::get('themTTSK', 'News_EvensController@themTTSK')->name('admin.themTTSK');
        Route::post('themTTSK', 'News_EvensController@xulyThemTTSK');
        Route::get('suaTTSK/{id}', 'News_EvensController@suaTTSK')->name('admin.suaTTSK');
        Route::post('suaTTSK/{id}', 'News_EvensController@capnhatTTSK');
        Route::get('xoaTTSK/{id}', 'News_EvensController@xoaTTSK')->name('admin.xoaTTSK');
    });

    Route::group(['prefix' => 'tours'], function () {
        Route::get('danhsachTour', 'ToursController@danhsachTour')->name('admin.danhsachTour');
        Route::get('themTour', 'ToursController@themTour')->name('admin.themTour');
        Route::post('themTour', 'ToursController@xylythemTour');
        Route::get('suaTour/{id}', 'ToursController@suaTour')->name('admin.suaTour');
        Route::post('suaTour/{id}', 'ToursController@capnhatTour');
        Route::get('xoaTour/{id}', 'ToursController@xoaTour')->name('admin.xoaTour');
    });

    Route::group(['prefix' => 'styleImage'], function () {
        Route::get('danhsachStyleImage', 'StyleImageController@danhsachStyleImage')->name('admin.danhsachStyleImage');
        Route::get('suaStyleImage/{id}', 'StyleImageController@suaStyleImage')->name('admin.suaStyleImage');
        Route::post('suaStyleImage/{id}', 'StyleImageController@xulysuaStyleImage');
        Route::get('themStyleImage', 'StyleImageController@themStyleImage')->name('admin.themStyleImage');
        Route::post('themStyleImage', 'StyleImageController@xulythemStyleImage');
        Route::get('xoaStyleImage/{id}', 'StyleImageController@xoaStyleImage')->name('admin.xoaStyleImage');
    });

    Route::group(['prefix' => 'images'], function () {
        Route::get('danhsachImage', 'ImageController@danhsachImage')->name('admin.danhsachImage');
        Route::get('suaImage/{id}', 'ImageController@suaImage')->name('admin.suaImage');
        Route::post('suaImage/{id}', 'ImageController@xulysuaImage');
        Route::get('themImage', 'ImageController@themImage')->name('admin.themImage');
        Route::post('themImage', 'ImageController@xulythemImage');
        Route::get('xoaImage/{id}', 'ImageController@xoaImage')->name('admin.xoaImage');
    });
});