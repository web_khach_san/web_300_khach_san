<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function styleImage()
    {
        return $this->belongsTo('App\StyleImage', 'id_styleImage');
    }
}