<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{

    public function danhSachDichVu(Request $request)
    {
        $services = Service::orderBy('id', 'DESC')->paginate(3);
        return view('admin.Services.list', compact('services'));
    }

    public function themDichVu()
    {
        $services = Service::all();
        return view('admin.Services.add', compact('services'));
    }


    public function XuLyThemDichVu(Request $request)

    {
        $this->validate(
            $request,
            [
                'nameService' => 'required|min:10',
                // 'content' => 'required|min:20',    			
            ],
            [
                'nameService.required' => 'Bạn chưa nhập tên dịch vụ!',
                'nameService.min' => 'Nội dung quá ngắn, yêu cầu tối thiểu gồm 6 ký tự trở lên!',
                // 'content.required'=>'Bạn chưa nhập nội dung giới thiệu về dịch vụ!',
                // 'content.min'=>'Nội dung quá ngắn, yêu cầu tối thiểu gồm 20 ký tự trở lên!',
            ]
        );
        $services = new Service();
        $services->nameService = $request->nameService;
        $services->content = $request->content;

        if ($request->hasFile('imageSevice')) {
            $image = $request->file('imageSevice');
            $path = $image->store('imageService', 'public');
            $services->images = $path;
        }
        $services->save();



        return redirect('admin/service/danhsachdichvu')->with('message', 'Thêm thành công!');
    }


    public function suaDichVu($id)
    {
        $services = Service::findOrFail($id);
        return view('admin.Services.edit', compact('services'));
    }


    public function capNhapDichVu(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'nameService' => 'required',
                // 'content' => 'required|min:20',    			
            ],
            [
                'nameService.required' => 'Bạn chưa nhập tên dịch vụ!',
                // 'content.required'=>'Bạn chưa nhập nội dung giới thiệu về dịch vụ!',
                // 'content.min'=>'Nội dung quá ngắn, yêu cầu tối thiểu gồm 20 ký tự trở lên!',
            ]
        );
        $services = Service::findOrFail($id);
        $services->nameService = $request->nameService;
        $services->content = $request->content;

        if ($request->hasFile('Images')) {
            $image = $request->file('Images');
            $path = $image->store('imageService', 'public');
            $services->images = $path;
        }

        $services->save();

        return redirect('admin/service/danhsachdichvu')->with('message', 'Sửa thành công!');
    }
    public function xoaDichVu($id)
    {
        $services = Service::findOrFail($id);
        $image = $services->images;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $services->delete();
        return redirect('admin/service/danhsachdichvu')->with('message', 'Xóa thành công!');
    }
}