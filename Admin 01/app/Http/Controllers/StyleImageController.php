<?php

namespace App\Http\Controllers;

use App\StyleImage;
use Illuminate\Http\Request;

class StyleImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function danhsachStyleImage(Request $request)
    {
        $styleImage = StyleImage::orderBy('id', 'DESC')->paginate(3);;
        return view('admin.styleImage.list', compact('styleImage'));
    }

    public function themStyleImage()
    {
        return view('admin.styleImage.add');
    }

    public function xulythemStyleImage(Request $request)
    {
        $this->validate(
            $request,
            [
                'nameStyle' => 'required|min:5',
                'description' => 'required|min:20',
            ],
            [
                'nameStyle.required' => 'Bạn chưa nhập tên loại ảnh!',
                'description.required' => 'Bạn chưa nhập nội dung mô tả về loại ảnh!',
            ]
        );
        $styleImage = new StyleImage();
        $styleImage->nameStyle = $request->nameStyle;

        if ($request->hasFile('imageStyleImage')) {
            $image = $request->file('imageStyleImage');
            $path = $image->store('imageStyleImage', 'public');
            $styleImage->image = $path;
        }

        $styleImage->description = $request->description;
        $styleImage->save();

        return redirect('admin/styleImage/danhsachStyleImage')->with('message', 'Thêm thành công!');
    }

    public function suaStyleImage($id)
    {
        $styleImage = StyleImage::find($id);
        return view('admin/styleImage/edit', compact('styleImage'));
    }

    public function xulysuaStyleImage(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'nameStyle' => 'required|min:5',
                'description' => 'required|min:20',
            ],
            [
                'nameStyle.required' => 'Bạn chưa nhập tên loại ảnh!',
                'description.required' => 'Bạn chưa nhập nội dung mô tả về loại ảnh!',
            ]
        );
        $styleImage = StyleImage::find($id);
        $styleImage->nameStyle = $request->nameStyle;

        if ($request->hasFile('Image')) {
            $image = $request->file('Image');
            $path = $image->store('imageStyleImage', 'public');
            $styleImage->image = $path;
        }

        $styleImage->description = $request->description;
        $styleImage->save();

        return redirect('admin/styleImage/danhsachStyleImage')->with('message', 'Sửa thành công!');
    }

    public function xoaStyleImage($id)
    {
        $styleImage = StyleImage::find($id);
        $image = $styleImage->Anh;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $styleImage->delete();
        return redirect('admin/styleImage/danhsachStyleImage')->with('message', 'Xóa thành công!');
    }
}