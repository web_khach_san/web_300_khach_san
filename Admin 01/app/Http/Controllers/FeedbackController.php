<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;

class FeedbackController extends Controller
{
    public function dienthongtin(Request $request)
    {
        $this->validate($request,
        [
            'subject'=>'required',
               'name' =>'required' ,
               'email' =>'required' ,
               'content' =>'required|min:6' ,


            ],
            [
                   'name.required' => 'Bạn chưa nhập tên!',
                   'subject.required' => 'Bạn chưa nhập chủ đề!',
                   'content.min' => 'Nội dung phải có ít nhất 6 kí tự!',
                   'email.required' => 'Bạn chưa nhập email!',
                   'content.required' => 'Bạn chưa nhập nội dung!',

            ]);
        $data = new Feedback;
        $data->subject = $request->subject;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->content = $request->content;
        $data->save();
        Session::flash('success', 'Cám ơn bạn đã liên hệ với chúng tôi');
        return redirect()->back();
    }

    public function danhSachdienthongtin(Request $request)
    {
        $lienhe = Feedback::orderBy('id', 'DESC')->paginate(5);
        return view('admin.contact.listlienhe', compact('lienhe'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function xoalienhe($id)
    {
        $lienhe = Feedback::find($id);
        $lienhe->delete();
        return redirect('feedback/lienhekhachhang')->with('message', 'Xóa thành công!');
    }
}
