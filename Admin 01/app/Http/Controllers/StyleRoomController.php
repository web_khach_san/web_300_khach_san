<?php

namespace App\Http\Controllers;

use App\StyleRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StyleRoomController extends Controller
{
    public function danhSachLoaiPhong(Request $request)
    {
        $styleRooms = StyleRoom::orderBy('id', 'DESC')->paginate(3);
        return view('admin.styleRoom.list', compact('styleRooms'));
    }

    public function themLoaiPhong()
    {
        return view('admin.styleRoom.add');
    }

    public function XuLyThemLoaiPhong(Request $request)
    {
        $this->validate($request,
            [
                'maSo' => 'required',
                'nameStyle' => 'required|min:5',
                'Gia' => 'required',
                'Mota' => 'required|min:20',
            ],
            [
                'maSo.required' => 'Bạn chưa nhập số phòng!',
                'nameStyle.required' => 'Bạn chưa nhập tên loại phòng!',
                'nameStyle.min' => 'Tên quá ngắn, yêu cầu tối thiểu gồm 5 ký tự trở lên!',
                'Gia.required' => 'Bạn chưa nhập giá loại phòng!',
                'Mota.required' => 'Bạn chưa nhập nội dung giới thiệu về loại phòng!',
                'Mota.min' => 'Nội dung quá ngắn, yêu cầu tối thiểu gồm 20 ký tự trở lên!',
            ]);
        $styleRooms = new StyleRoom();

        $styleRooms->MaLoaiPhong = $request->maSo;
        $styleRooms->TenLoaiPhong = $request->nameStyle;

        if ($request->hasFile('imageStyleroom')) {
            $image = $request->file('imageStyleroom');
            $path = $image->store('imageStyleRoom', 'public');
            $styleRooms->Anh = $path;
        }

        $styleRooms->Mota = $request->Mota;
        $styleRooms->Gia = $request->Gia;
        $styleRooms->save();

        return redirect('admin/styleRoom/danhsachloaiphong')->with('message', 'Thêm thành công!');
    }

    public function SuaLoaiPhong($id)
    {
        $styleRooms = StyleRoom::find($id);
        return view('admin/styleRoom/edit', compact('styleRooms'));
    }

    public function capnhapLoaiPhong(Request $request, $id)
    {
        $this->validate($request,
            [
                'maSo' => 'required',
                'nameStyle' => 'required|min:5',
                'Gia' => 'required',
                'Mota' => 'required|min:20',
            ],
            [
                'maSo.required' => 'Bạn chưa nhập số phòng!',
                'nameStyle.required' => 'Bạn chưa nhập tên loại phòng!',
                'nameStyle.min' => 'Tên quá ngắn, yêu cầu tối thiểu gồm 5 ký tự trở lên!',
                'Gia.required' => 'Bạn chưa nhập giá loại phòng!',
                'Mota.required' => 'Bạn chưa nhập nội dung giới thiệu về loại phòng!',
                'Mota.min' => 'Nội dung quá ngắn, yêu cầu tối thiểu gồm 20 ký tự trở lên!',
            ]);
        $styleRooms = StyleRoom::find($id);

        $styleRooms->MaLoaiPhong = $request->maSo;
        $styleRooms->TenLoaiPhong = $request->nameStyle;

        if ($request->hasFile('Images')) {
            $image = $request->file('Images');
            $path = $image->store('imageStyleRoom', 'public');
            $styleRooms->Anh = $path;
        }

        $styleRooms->Mota = $request->Mota;
        $styleRooms->Gia = $request->Gia;
        $styleRooms->save();

        return redirect('admin/styleRoom/danhsachloaiphong')->with('message', 'Cập nhập thành công!');
    }

    public function xoaLoaiPhong($id)
    {
        $styleRooms = StyleRoom::find($id);
        $image = $styleRooms->Anh;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $styleRooms->delete();
        return redirect('admin/styleRoom/danhsachloaiphong')->with('message', 'Xóa thành công!');

    }

}
