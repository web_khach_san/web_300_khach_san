<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisement;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class adsController extends Controller
{
    public function index()
    {
        $allAds = Advertisement::all();

        return view('admin.advertisements.listAds', compact('allAds'));
    }

    public function createAds()
    {
        return view('admin.advertisements.createAds');
    }

    public function storeAds(Request $request)
    {
        $ad = new Advertisement();

        $ad->name = $request->name;
        $ad->email = $request->email;
        $ad->phone = $request->phone;

        if ($request->hasFile('imageAds')) {
            $image = $request->file('imageAds');
            $path = $image->store('adsImage', 'public');
            $ad->image = $path;
        }
        $ad->website = $request->website;

        $ad->save();

        return redirect('admin/ads/listAds')->with('message', 'Thêm thành công!');
    }

    public function editAds($id)
    {
        $ad = Advertisement::findOrFail($id);
        return view('admin.advertisements.editAds', compact('ad'));
    }

    public function suaEditAds(Request $request, $id)
    {
        $ad = Advertisement::findOrFail($id);

        $ad->name = $request->name;
        $ad->email = $request->email;
        $ad->phone = $request->phone;

        if ($request->hasFile('imageAds')) {
            $image = $request->file('imageAds');
            $path = $image->store('adsImage', 'public');
            $ad->image = $path;
        }
        $ad->website = $request->website;

        $ad->save();

        return redirect('admin/ads/listAds')->with('message', 'Sửa thành công!');
    }

    public function deleteAds($id)
    {

        $ads = Advertisement::find($id);
        $image = $ads->image;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $ads->delete();
        return redirect()->back()->with('message', 'Xóa thành công!');
    }
}