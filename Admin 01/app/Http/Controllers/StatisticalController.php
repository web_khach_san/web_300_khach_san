<?php

namespace App\Http\Controllers;

use App\Reservation;
use Charts;
use DB;

class StatisticalController extends Controller
{
    public function index()
    {
        $products = Reservation::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), date('Y'))->get();
        $statistiday = Charts::database($products, 'bar', 'highcharts')
            ->title("Thống kê đặt phòng ngày")
            ->elementLabel("Lượt đặt phòng trong ngày")
            ->dimensions(1000, 500)
            ->responsive(true)
            ->groupByDay();
        $statistimonth = Charts::database($products, 'bar', 'highcharts')
            ->title("Thống kê đặt phòng tháng")
            ->elementLabel("Lượt đặt phòng trong tháng")
            ->dimensions(1000, 500)
            ->responsive(true)
            ->groupByMonth(date('Y'), false);
         
        return view('menu.test', compact('statistiday','statistimonth'));
    }
}
