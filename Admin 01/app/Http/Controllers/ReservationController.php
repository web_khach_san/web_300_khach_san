<?php

namespace App\Http\Controllers;

use App\Reservation;

use Mail;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function postDatPhong(Request $request) 
    {
        $this->validate($request,
		   [
			'ngayden'=>'required|after:today',
            'ngaydi' =>'required|after:ngayden' ,
            'hoten' =>'required' ,
            'soluong' =>'required' ,
            'loaiphong' =>'required' ,
            'dienthoai' =>'required' ,
            'email' =>'required|email' ,
          
		   ],
		   [
            'ngayden.required' => 'Bạn chưa nhập ngày đến!',
                'ngaydi.required' => 'Bạn chưa nhập ngày đi!',
                'ngayden.after'=> 'Xin vui lòng chọn lại ngày đến!',
                'ngaydi.after'=> 'Xin vui lòng chọn lại ngày đi!',
                'hoten.required' => 'Bạn chưa nhập họ và tên!',
                'soluong.required' => 'Bạn chưa nhập số lượng người!',
                'loaiphong.required' => 'Bạn chưa nhập loại phòng!',
                'dienthoai.required' => 'Bạn chưa nhập số điện thoại!',
                'email.required' => 'Bạn chưa nhập email!',
                'email.email' => 'Bạn nhập không đúng định dạng!',
				
         ]);
        $mail = new Reservation;
        $mail->ngayden =$request->ngayden;
        $mail->ngaydi = $request->ngaydi;
        $mail->hoten = $request->hoten;
        $mail->soluong = $request->soluong;
        $mail->loaiphong = $request->loaiphong;
        $mail->dienthoai = $request->dienthoai;
        $mail->email =$request->email;
        $mail->save();
        $data=  ['ngayden'=>$request->ngayden,
                'ngaydi'=>$request->ngaydi,
                'hoten'=>$request->hoten,
                'soluong'=>$request->soluong,
                'loaiphong'=>$request->loaiphong,
                'dienthoai'=>$request->dienthoai,
                'email'=>$request->email
                ];

        // $data->save();        
        Mail::send('admin.reservation.sendmail',$data,function($message){
            $message->from('quangthanhle2018@gmail.com','admin');
            $message->to('long.hoangngocphuc@gmail.com','long')->subject('Đặt phòng');
        });
        return redirect()->back()->with('message','Cảm ơn bạn đã đặt phòng, chúng tôi sẽ liên hệ bạn trong thời gian sớm nhất');
    }
    public function danhsachDatPhong(Request $request)
    {
        $datphong = Reservation::orderBy('id','DESC')->paginate(3);
        return view('admin.reservation.danhsach',compact('datphong'))->with('i', ($request->input('page', 1) - 1) * 5);;
    }
    public function trangthaidatphong($id)
    {
        $datphong = Reservation::find($id);
        $datphong->trangthai = !$datphong->trangthai;
        $datphong->save();
        return redirect()->back();
    }
    public function xoaDatPhong($id){
        $datphong = Reservation::find($id);
        $datphong->delete();
        return redirect()->back()->with('message','Bạn đã xóa thành công');
    }
}
