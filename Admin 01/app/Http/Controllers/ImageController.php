<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\StyleImage;

class ImageController extends Controller
{
    public function danhsachImage(Request $request)
    {
        $images = Image::orderBy('id', 'DESC')->paginate(3);
        return view('admin.images.list', compact('images'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function themImage()
    {
        $images = Image::all();
        $styleImage = StyleImage::all();
        return view('admin.images.add', compact('images', 'styleImage'));
    }

    public function xulythemImage(Request $request)

    {
        $this->validate(
            $request,
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập số phòng!',
            ]
        );
        $images = new Image();
        $images->name = $request->name;

        if ($request->hasFile('imageImages')) {
            $image = $request->file('imageImages');
            $path = $image->store('imageImages', 'public');
            $images->image = $path;
        }
        $images->id_styleImage = $request->loaiAnh;
        $images->save();
        return redirect('admin/images/danhsachImage')->with('message', 'Thêm thành công!');
    }

    public function suaImage($id)
    {
        $images = Image::findOrFail($id);
        $styleImage = StyleImage::all();
        return view('admin.images.edit', compact('images', 'styleImage'));
    }

    public function xulysuaImage(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập số phòng!',
            ]
        );
        $images = Image::findOrFail($id);
        $images->name = $request->name;

        if ($request->hasFile('Images')) {
            $image = $request->file('Images');
            $path = $image->store('imageImages', 'public');
            $images->image = $path;
        }

        $images->id_styleImage = $request->loaiAnh;
        $images->save();

        return redirect('admin/images/danhsachImage')->with('message', 'Sửa thành công!');
    }

    public function xoaImage($id)
    {
        $images = Image::find($id);
        $image = $images->Anh;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $images->delete();
        return redirect('admin/images/danhsachImage')->with('message', 'Xóa thành công!');
    }
}