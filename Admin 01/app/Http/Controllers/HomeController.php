<?php

namespace App\Http\Controllers;

use App\Information;
use App\StyleRoom;
use App\Service;
use App\Feedback;
use App\Tour;
use App\Advertisement;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ContentReservation;
use App\News_Even;

class HomeController extends Controller
{
    public function home()
    {
        $loaiphong = StyleRoom::paginate(3);
        $gioithieu = Information::all()->first();
        $dichvu = Service::all();
        $events= News_Even::all();
        $tours = Tour::all();
        $allAds = Advertisement::all();
        return view('template.body', compact('loaiphong', 'dichvu', 'gioithieu','allAds','tours'));
    }
    public function Loaiphong()
    {
        $loaiphong = StyleRoom::all();
        $gioithieu = Information::all()->first();
        $allAds = Advertisement::all();
        return view('menu.loaiphong',compact('loaiphong','gioithieu','allAds'));
    }
    // public function ChiTietLoaiphong($id){
    //     $chitietloaiphong = StyleRoom::find($id);
    //     return view('menu.loaiphong',compact('chitietloaiphong'));
    // }

    public function datphong()
    {
        $gioithieu = Information::all()->first();
        $loaiphong = StyleRoom::all();
        $dichvu = Service::all();
        $tours = Tour::all();
        return view('menu.datphong', compact('gioithieu', 'loaiphong', 'dichvu', 'tours'));
    }
    public function Dichvu(){
        $gioithieu = Information::all()->first();
        $loaiphong = StyleRoom::all();
        $dichvu = Service::all();
        $tours = Tour::all();
        $allAds = Advertisement::all();
        return view('menu.service',compact('dichvu','gioithieu','tours','allAds','loaiphong'));
    }
    public function Lienhe(){
        $gioithieu = Information::all()->first();
        $allAds = Advertisement::all();
        $loaiphong = StyleRoom::all();
        return view('menu.lienhe',compact('gioithieu','allAds','loaiphong'));
    }
    public function Gioithieu()
    {
        return view('menu.gioithieu');
    }
    public function noiquy()
    {
        $gioithieu = Information::all()->first();
        $allAds = Advertisement::all();
        return view('menu.noiquy',compact('allAds','gioithieu'));
    }

    public function ShowLoaiPhong($id)
    {
        $loaiphong = StyleRoom::find($id);
        return view('template.showroom', compact('loaiphong'));
    }
    public function TinTucSuKien(){
        $gioithieu = Information::all()->first();
        $events= News_Even::all();
        $loaiphong = StyleRoom::all();
        $gioithieu = Information::all()->first();
        $allAds = Advertisement::all();
        return view('menu.tinTucSuKien',compact('events','gioithieu','allAds','loaiphong'));
    }
    public function chitietSuKien($id){
        $gioithieu = Information::all()->first();
        $allAds = Advertisement::all();
        $chitietSukien = News_Even::find($id);
        return view('menu.chitietSukien',compact('chitietSukien','allAds','gioithieu'));
    }

    public function chitietDV($id)
    {
        $dichvu = Service::find($id);
        $dichvus = Service::all();
        return view('menu.ctdichvu', compact('dichvu','dichvus'));
    }

    public function ctTour($id)
    {        
        $gioithieu = Information::all()->first();
        $tour = Tour::find($id);
        $tourshow = Tour::all();
        return view('menu.ctTour', compact('tour', 'tourshow','gioithieu'));
    }

    
    public function thongtindatphong()
    {
        $thongtindatphong = ContentReservation::all();
        $loaiphong = StyleRoom::all();
        $gioithieu = Information::all();
        $dichvu = Service::all();
        $allAds = Advertisement::all();
        return view('trangchu.thongtindatphong', compact('thongtindatphong', 'loaiphong', 'gioithieu', 'dichvu','$allAds'));
    }
}
