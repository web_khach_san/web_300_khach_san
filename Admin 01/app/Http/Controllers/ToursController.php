<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Tour;

class ToursController extends Controller
{

    public function danhsachTour(Request $request)
    {
        $tour = Tour::orderBy('id', 'DESC')->paginate(3);
        return view('admin.tours.list', compact('tour'));
    }

    public function themTour()
    {
        $tour = Tour::all();
        return view('admin.tours.add', compact('tour'));
    }


    public function xylythemTour(Request $request)

    {
        $this->validate(
            $request,
            [
                'nameTour' => 'required|min:10',
            ],
            [
                'nameTour.required' => 'Bạn chưa nhập tên Tour!',
                'nameTour.min' => 'Nội dung quá ngắn, yêu cầu tối thiểu gồm 6 ký tự trở lên!',
            ]
        );
        $tour = new Tour();
        $tour->nameTour = $request->nameTour;
        $tour->content = $request->content;

        if ($request->hasFile('imageTour')) {
            $image = $request->file('imageTour');
            $path = $image->store('imageTour', 'public');
            $tour->images = $path;
        }
        $tour->save();



        return redirect('admin/tours/danhsachTour')->with('message', 'Thêm thành công!');
    }


    public function suaTour($id)
    {
        $tour = Tour::findOrFail($id);
        return view('admin.tours.edit', compact('tour'));
    }


    public function capnhatTour(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'nameTour' => 'required',
            ],
            [
                'nameTour.required' => 'Bạn chưa nhập tên Tour!',
            ]
        );
        $tour = Tour::findOrFail($id);
        $tour->nameTour = $request->nameTour;
        $tour->content = $request->content;

        if ($request->hasFile('Images')) {
            $image = $request->file('Images');
            $path = $image->store('imageTour', 'public');
            $tour->images = $path;
        }

        $tour->save();

        return redirect('admin/tours/danhsachTour')->with('message', 'Sửa thành công!');
    }
    public function xoaTour($id)
    {
        $tour = Tour::findOrFail($id);
        $image = $tour->images;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $tour->delete();
        return redirect('admin/tours/danhsachTour')->with('message', 'Xóa thành công!');
    }
}