<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\News_Even;
use Carbon\Carbon;

class News_EvensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function danhsachTTSK()
    {
        $neweven = News_Even::orderBy('id', 'DESC')->paginate(3);
        return view('admin.news_events.list', compact('neweven'));
    }

    public function themTTSK()
    {
        $neweven = News_Even::all();
        return view('admin.news_events.add', compact('neweven'));
    }

    public function xulyThemTTSK(Request $request)

    {
        $this->validate(
            $request,
            [
                'title' => 'required|min:10',
            ],
            [
                'title.required' => 'Bạn chưa nhập tên tin tức sự kiện!',
                'title.min' => 'Nội dung quá ngắn, yêu cầu tối thiểu gồm 6 ký tự trở lên!',
            ]
        );
        $neweven = new News_Even();
        $neweven->title = $request->title;
        $neweven->content = $request->content;
        $neweven->created_at = Carbon::now();

        if ($request->hasFile('imageTTSK')) {
            $image = $request->file('imageTTSK');
            $path = $image->store('imageTTSK', 'public');
            $neweven->Anh = $path;
        }
        $neweven->save();
        return redirect('admin/news_evens/danhsachTTSK')->with('message', 'Thêm thành công!');
    }

    public function suaTTSK($id)
    {
        $neweven = News_Even::find($id);
        return view('admin.news_events.edit', compact('neweven'));
    }


    public function capnhatTTSK(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'title' => 'required',
            ],
            [
                'title.required' => 'Bạn chưa nhập tên tin tức sự kiện!',
            ]
        );
        $neweven = News_Even::findOrFail($id);
        $neweven->title = $request->title;
        $neweven->content = $request->content;
        $neweven->created_at = Carbon::now();


        if ($request->hasFile('Anh')) {
            $image = $request->file('Anh');
            $path = $image->store('imageTTSK', 'public');
            $neweven->Anh = $path;
        }

        $neweven->save();

        return redirect('admin/news_evens/danhsachTTSK')->with('message', 'Sửa thành công!');
    }
    public function xoaTTSK($id)
    {
        $neweven = News_Even::findOrFail($id);
        $image = $neweven->Anh;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $neweven->delete();
        return redirect('admin/news_evens/danhsachTTSK')->with('message', 'Xóa thành công!');
    }
}