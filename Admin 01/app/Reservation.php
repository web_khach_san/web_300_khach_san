<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservations';
	public function styke_room() {
		return $this->hasMany('App\StyleRoom', 'id_datPhong');
	}
}
